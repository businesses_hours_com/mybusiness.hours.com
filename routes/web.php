<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Order;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Laravel\Cashier\Coupon\RedeemedCoupon;
use Laravel\Cashier\Credit\Credit;
use Laravel\Cashier\Order\OrderItem;

Auth::routes(['verify' => true]);

Route::get('/', function () {
    return redirect()->intended(route('admin.locations.index'));
});

// Cache bust route
Route::get('cache-bust', function (Request $request) {
    if ($userId = $request->input('deleted_user_id')) {
        Subscription::query()->where('owner_type', User::class)->where('owner_id', $userId)->delete();
        Order::query()->where('owner_type', User::class)->where('owner_id', $userId)->delete();
        OrderItem::query()->where('owner_type', User::class)->where('owner_id', $userId)->delete();
        RedeemedCoupon::query()->where('owner_type', User::class)->where('owner_id', $userId)->delete();
        Credit::query()->where('owner_type', User::class)->where('owner_id', $userId)->delete();
    }

    Artisan::call('cache:clear');
});

// Mollie webhooks
Route::prefix('webhooks')->group(function () {
    Route::prefix('mollie')->group(function () {
        Route::get('/', 'MollieController@index');
        Route::get('first-payment', 'MollieController@index');
    });
});

