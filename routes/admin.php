<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'DashboardController@index')->name('dashboard');

Route::prefix('contact')->name('contact.')->group(function () {
    Route::get('/', 'ContactController@index')->name('index');
    Route::post('/', 'ContactController@send')->name('send');
});

Route::match([  'get', 'post'], 'promotions', 'PromotionsPageController@index')->name('promotions');

Route::resource('users', 'UserController')->only('edit', 'update');

Route::resource('businesses', 'BusinessController')->only('index', 'edit', 'update');
Route::resource('locations', 'LocationController')->only('index', 'edit', 'update');

Route::prefix('locations')->name('locations.')->group(function () {
    Route::get('payment/{payment_id?}', 'LocationController@index');
    Route::get('{hash}/address', 'LocationController@editAddress')->name('address');
    Route::put('{hash}/address', 'LocationController@updateAddress')->name('update-address');
    Route::get('{hash}/hours', 'LocationController@editHours')->name('hours');
    Route::put('{hash}/hours', 'LocationController@updateHours')->name('update-hours');
    Route::get('{hash}/images', 'LocationController@editImages')->name('images');
    Route::put('{hash}/images', 'LocationController@updateImages')->name('update-images');
    Route::get('{hash}/texts', 'LocationController@editTexts')->name('texts');
    Route::put('{hash}/texts', 'LocationController@updateTexts')->name('update-texts');
    Route::delete('{hash}/images/{file_id}/destroy', 'LocationController@destroyImage')->name('destroy-image');
    Route::delete('{hash}/destroy', 'LocationController@destroy')->name('destroy');
});

Route::view('invoices', 'admin.invoices.index')->name('invoices.index');
Route::get('invoices/download/{order_id}', function ($orderId) {
    return request()->user()->downloadInvoice($orderId);
})->name('invoices.download');

Route::prefix('reviews')->name('reviews.')->group(function () {
    Route::get('{location?}', 'ReviewController@index')->name('index');
    Route::delete('{review}', 'ReviewController@destroy')->name('destroy');
    Route::post('{review}/report', 'ReviewController@report')->name('report');
    Route::get('{review}/responses/create', 'ReviewController@createResponse')->name('responses.create');
    Route::get('{review}/responses/{response}', 'ReviewController@editResponse')->name('responses.edit');
    Route::put('{review}/responses/{response}', 'ReviewController@updateResponse')->name('responses.update');
    Route::post('{review}/responses', 'ReviewController@storeResponse')->name('responses.store');
});

Route::prefix('questions')->name('questions.')->group(function () {
    Route::get('{location?}', 'QuestionController@index')->name('index');
    Route::delete('{question}', 'QuestionController@destroy')->name('destroy');
    Route::post('verified', 'QuestionController@verifiedFAQ')->name('verified-faq');
    Route::post('{question}/report', 'QuestionController@report')->name('report');
    Route::get('{question}/answers', 'AnswerController@index')->name('answers.index');
    Route::post('{question}/answers', 'AnswerController@store')->name('answers.store');
    Route::get('{question}/answers/create', 'AnswerController@create')->name('answers.create');
    Route::put('{question}/answers/{answer}', 'AnswerController@update')->name('answers.update');
    Route::delete('{question}/answers/{answer}', 'AnswerController@destroy')->name('answers.destroy');
    Route::get('{question}/answers/{answer}/edit', 'AnswerController@edit')->name('answers.edit');
    Route::post('{question}/answers/{answer}/report', 'AnswerController@report')->name('answers.report');
});

Route::prefix('location-wizard')->name('location-wizard.')->group(function () {
    Route::get('/', 'LocationWizardController@index')->name('index');
    Route::post('/', 'LocationWizardController@saveProgress')->name('save-progress');
    Route::post('destroy', 'LocationWizardController@destroyProgress')->name('destroy-progress');
    Route::get('add-location', 'LocationWizardController@addLocation')->name('add-location');
    Route::get('load-businesses', 'LocationWizardController@loadBusinesses')->name('load-businesses');
    Route::get('load-locations', 'LocationWizardController@loadLocations')->name('load-locations');
    Route::get('load-holidays', 'LocationWizardController@loadHolidays')->name('load-holidays');
    Route::get('load-categories', 'LocationWizardController@loadCategories')->name('load-categories');
    Route::get('resolve-address', 'LocationWizardController@resolveAddress')->name('resolve-address');
    Route::post('store', 'LocationWizardController@store')->name('store');
});

// scraper
Route::post('scraper', 'WebsiteScraperController')->name('scraper');

// Subscription
Route::match(['get', 'post'], 'subscribe/{hash?}', 'SubscriptionController@subscribe')->name('subscribe');
Route::post('validate-coupon', 'SubscriptionController@validateCoupon')->name('validate-coupon');

// changeLocale
Route::get('country/{code}', 'DashboardController@changeCountry')->name('change-country');
Route::get('locale/{code}', 'DashboardController@changeLocale')->name('change-locale');
