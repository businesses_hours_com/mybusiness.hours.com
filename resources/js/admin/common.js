import axios from 'axios';
import languageBundle from '@kirschbaum-development/laravel-translations-loader!@kirschbaum-development/laravel-translations-loader';

window.trans                                             = languageBundle;
window.axios                                             = axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

const datepickerTranslations = window.trans[window.locale].wizard.step3.datepicker || {};
window.datepickerLocaleObject = {
    format: 'DD-MM-YYYY',
    separator: ' / ',
    applyLabel: datepickerTranslations.apply,
    cancelLabel: datepickerTranslations.cancel,
    fromLabel: datepickerTranslations.from,
    toLabel: datepickerTranslations.to,
    customRangeLabel: datepickerTranslations.custom,
    weekLabel: datepickerTranslations.weekLabel,
    daysOfWeek: [
        datepickerTranslations.sundayLabel,
        datepickerTranslations.mondayLabel,
        datepickerTranslations.tuesdayLabel,
        datepickerTranslations.wednesdayLabel,
        datepickerTranslations.thursdayLabel,
        datepickerTranslations.fridayLabel,
        datepickerTranslations.saturdayLabel
    ],
    monthNames: [
        datepickerTranslations.january,
        datepickerTranslations.february,
        datepickerTranslations.march,
        datepickerTranslations.april,
        datepickerTranslations.may,
        datepickerTranslations.june,
        datepickerTranslations.july,
        datepickerTranslations.august,
        datepickerTranslations.september,
        datepickerTranslations.october,
        datepickerTranslations.november,
        datepickerTranslations.december
    ],
}

$(document).ready(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /**
     * Init select2 elements
     */
    $('select.select2').each(function(index, el){
        let isTaggable  = $(el).hasClass('tags');
        let placeholder = $(el).attr('data-placeholder');
        $(el).select2({
            placeholder: placeholder || 'Select',
            width: '100%',
            tags: isTaggable,
            minimumResultsForSearch: 10
        });
    });

    $('.date-range').each(function(){
        $(this).daterangepicker({
            locale: window.datepickerLocaleObject
        }).val($(this).prop('value') || '').attr('placeholder', 'Choose');
    });

    /**
     * Init dropzone
     */
    $('.dropzone').dropzone({
        url: '/', // Set the url for your upload script location
        autoProcessQueue: false,
        thumbnailMethod: 'contain',
        thumbnailWidth: 500,
        paramName: 'file', // The name that will be used to transfer the file
        maxFiles: 1,
        maxFilesize: 4, // MB
        addRemoveLinks: true,
        acceptedFiles: $(this).attr('data-accepted'),
        accept: function(file, done){
            done();
        },
        init: function(){
            this.addCustomFile = function(file, thumbnail_url, response){
                // Push file to collection
                this.files.push(file);
                // Emulate event to create interface
                this.emit('addedfile', file);
                // Add thumbnail url
                this.emit('thumbnail', file, thumbnail_url);
                // Add status processing to file
                this.emit('processing', file);
                // Add status success to file AND RUN EVENT success from response
                this.emit('success', file, response, false);
                // Add status complete to file
                this.emit('complete', file);
            };
            this.on('thumbnail', function(file, dataUrl){
                const dropzoneEl = $(this.element);
                let type         = dropzoneEl.hasClass('dropzone-favicon') ? 'favicon' : 'logo';
                //set image name
                $('.dropzone-' + type + '-name').val(file.name);
                // set dataUrl
                let hiddenInput = $('input[type="hidden"][name="' + dropzoneEl.attr('data-name') + '"]');
                if (hiddenInput.length) {
                    hiddenInput.val(dataUrl);
                }
                else {
                    dropzoneEl.before('<input class="dropzone-'+type+'-file" type="hidden" name="' + dropzoneEl.attr('data-name') + '"' + ' value="' + dataUrl + '">');
                }
            });
            this.on('removedfile', function(file, dataUrl){
                let type = $(this.element).hasClass('dropzone-favicon') ? 'favicon' : 'logo';
                $('.dropzone-' + type + '-url').val('');
                $('.dropzone-' + type + '-name').val('');
                $('.dropzone-' + type + '-type').val('');
                $('.dropzone-' + type + '-size').val('');
                $('.dropzone-' + type + '-file').val('');
            });
        }
    }).each(function(){
        if ($(this).attr('data-url')) {
            let dropzoneClass = $(this).hasClass('dropzone-favicon') ? '.dropzone-favicon' : '.dropzone-logo';
            let url           = $(dropzoneClass + '-url').val();
            let name          = $(dropzoneClass + '-name').val();
            let type          = $(dropzoneClass + '-type').val();
            let size          = $(dropzoneClass + '-size').val();

            locationWizard.step1.addFileToDropzone(dropzoneClass, {url, name, type, size, file: url});
        }
    });

    /**
     * Autoload object on load of page
     */
    $('form select.objects-select').each(function(index, obj){
        let object = $(obj);

        if (object.length) {
            loadObjects(object);
        }
    });


    /**
     * a.ajax-delete confirmation
     */
    $('a.ajax-delete').click(function(e){
        e.preventDefault();
        let href = $(this).attr('href');
        let title = $(this).attr('data-modal-title') || 'Are you sure you want to delete this item?';
        let cancelBtnText = $(this).attr('data-modal-cancel') || 'Cancel';
        let yesBtnText = $(this).attr('data-modal-yes') || 'Yes';

        swal.fire({
            title: title,
            type: 'error',
            showCancelButton: true,
            confirmButtonClass: 'btn-danger',
            cancelButtonText: cancelBtnText,
            confirmButtonText: yesBtnText
        }).then(function(result){
            if (result.value) {
                $('<form action="' + href + '" method="POST">' +
                    '<input type="hidden" name="_token" value="' + $('meta[name="csrf-token"]').attr('content') + '">' +
                    '<input type="hidden" name="_method" value="DELETE">' +
                    '</form>')
                    .appendTo($('body'))
                    .submit();
                return;
            }
        });
        return false;
    });

    /**
     * a.ajax-report choosing and submitting
     */
    $('a.ajax-report').click(function(e){
        e.preventDefault();
        let href          = $(this).attr('href');
        let isAppropriate = parseInt($(this).attr('data-is-appropriate'));
        if (isAppropriate) {
            swal.fire({
                title: 'Can\'t report it because it\'s already set as appropriate',
                confirmButtonText: 'Ok'
            });
        }
        else {
            swal.fire({
                title: 'Please select a reason for reporting',
                showCancelButton: true,
                cancelButtonText: 'Cancel',
                confirmButtonText: 'Report',
                input: 'select',
                inputOptions: JSON.parse($(this).attr('data-reports')),
                inputPlaceholder: 'Select a reason',
                inputValidator: (value) => {
                    return new Promise((resolve) => {
                        if (value) {
                            resolve();
                        }
                        else {
                            resolve('You need to select a reason');
                        }
                    });
                }
            }).then(function(result){
                if (result.value) {
                    $('<form action="' + href + '" method="POST">' +
                        '<input type="hidden" name="_token" value="' + $('meta[name="csrf-token"]').attr('content') + '">' +
                        '<input type="hidden" name="report_id" value="' + result.value + '">' +
                        '<input type="hidden" name="reported_by" value="Retailer">' +
                        '</form>')
                        .appendTo($('body'))
                        .submit();
                    return;
                }
            });
        }
        return false;
    });

    /**
     * a.ajax-delete confirmation
     */
    $('a.ajax-verified-faq').click(function(e){
        e.preventDefault();
        let href = $(this).attr('href');
        swal.fire({
            title: 'Enter new FAQ',
            html:
                'not implemented yet.',
            // '<input type="text" class="modal-question" name="question[text]" placeholder="Question">' +
            // '<textarea class="modal-answer" name="answer[text]" rows="5" placeholder="Answer"></textarea>',
            preConfirm: function(){
                return new Promise(function(resolve){
                    resolve([
                        $('.modal-question').val(),
                        $('.modal-answer').val()
                    ]);
                });
            },
            showCancelButton: true,
            confirmButtonClass: 'btn-success',
            cancelButtonText: 'Cancel',
            confirmButtonText: 'Create'
        }).then(function(result){
            if (result.value) {
                console.log(result);
                // $('<form action="' + href + '" method="POST">' +
                //   '<input type="hidden" name="_token" value="' + $('meta[name="csrf-token"]').attr('content') + '">' +
                //   '<input type="hidden" name="_method" value="DELETE">' +
                //   '</form>')
                //   .appendTo($('body'))
                //   .submit();
                // return;
            }
        });
        return false;
    });
});

/**
 * Search-based auto-load objects based on type
 */
function loadObjects(objectSelect, type){
    objectSelect.select2({
        ajax: {
            url: objectSelect.attr('data-objects-route'),
            dataType: 'json',
            delay: 350,
            data: function(params){
                return {
                    name: params.term
                };
            },
            processResults: function(data){
                data = data.data || data;
                return {
                    results: $.map(data, function(item){
                        return {
                            text: item.name || item.plural,
                            id: item.id
                        };
                    })
                };
            }
        },
        minimumInputLength: 3
    });
}

window.validateUrl = function(value){
    return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(value);
};

window.delayFn = function(fn, ms){
    let timer = 0;
    return function(...args){
        clearTimeout(timer);
        timer = setTimeout(fn.bind(this, ...args), ms || 0);
    };
};




