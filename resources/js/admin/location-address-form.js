import step2 from './location-wizard/step2';

$(document).ready(function () {
  $('.location-address-edit-form .btn[type="submit"]').click(function (e) {
    e.preventDefault();
    let country = $(this).parents('.step2').attr('data-country');

    if(step2.validateAddress(country)){
      $(this).parents('form').submit();
    }
  });
});