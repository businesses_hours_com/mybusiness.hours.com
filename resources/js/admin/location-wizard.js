'use strict';
window.KTWizard    = require('../../metronic/demo1/src/assets/js/global/components/base/wizard');
window.KTUtil      = require('../../metronic/demo1/src/assets/js/global/components/base/util');
window.KTApp       = require('../../metronic/demo1/src/assets/js/global/components/base/app');
window.KTAvatar    = require('../../metronic/demo1/src/assets/js/global/components/base/avatar');
window.KTDialog    = require('../../metronic/demo1/src/assets/js/global/components/base/dialog');
window.KTHeader    = require('../../metronic/demo1/src/assets/js/global/components/base/header');
window.KTMenu      = require('../../metronic/demo1/src/assets/js/global/components/base/menu');
window.KTOffcanvas = require('../../metronic/demo1/src/assets/js/global/components/base/offcanvas');
window.KTPortlet   = require('../../metronic/demo1/src/assets/js/global/components/base/portlet');
window.KTScrolltop = require('../../metronic/demo1/src/assets/js/global/components/base/scrolltop');
window.KTToggle    = require('../../metronic/demo1/src/assets/js/global/components/base/toggle');

import step1 from './location-wizard/step1';
import step2 from './location-wizard/step2';
import step3 from './location-wizard/step3';

window.locationWizard = {
    step1,
    step2,
    step3,
    el: $('#location-wizard'),
    wizard: null,
    invalid: false,
    saveProgress(){
        return new Promise((resolve, reject) => {
            axios.post(locationWizard.el.attr('data-save-progress-route'), {
                current_step: locationWizard.wizard.currentStep,
                user_id: locationWizard.el.attr('data-user-id'),
                data: locationWizard.el.find('form').serialize()
            })
                .then(response => resolve(response.data))
                .catch(err => reject(err.response.data));
        });
    },
    destroyProgress(){
        return new Promise((resolve, reject) => {
            axios.post(locationWizard.el.attr('data-destroy-progress-route'))
                .then(response => resolve(response.data))
                .catch(err => reject(err.response.data));
        });
    },
    submitForm(submitType, btn){
        const url = $('.wizard-form').attr('action');
        $('.tab-pane.active .regular-hours-table input[type="time"]').removeClass('is-invalid');

        axios.post(url, {
            submit_type: submitType
        })
            .then(response => {
                if (response.data.hasOwnProperty('href')) {
                    window.location.href = response.data.href;
                }
            })
            .catch(err => {
                let error = err.response.data.error;
                locationWizard.showError(JSON.parse(error));
            })
            .finally(() => {
                btn.prop('disabled', false);
                btn.find('.fa-spinner').remove();
            });
    },
    showError(err){
        if (err.hasOwnProperty('overlap')) {
            err.overlap.forEach((day) => $('.tab-pane.active .regular-hours-table [data-day-num="' + day + '"] input[type="time"]').addClass('is-invalid'));
            $('html, body').animate({
                scrollTop: $('.step3').offset().top
            }, 400);
        }

        swal.fire({
            type: 'error',
            title: 'Oops...',
            text: err.message.join(', '),
            confirmButtonClass: 'btn btn-secondary'
        });
    },
    showResetPopup(){
        let translations = window.trans[window.locale].wizard.step3 || {};
        swal.fire({
            type: 'error',
            title: translations.reset_current_input,
            text: translations.no_revert,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: translations.reset_btn,
            cancelButtonText: translations.cancel_btn
        }).then(result => {
            if (result.value) {
                locationWizard.destroyProgress().then(() => location.reload());
            }
        });
    },
    renderHtml(step){
        if (step && this.hasOwnProperty('step' + step)) {
            locationWizard.el.attr('data-current-step', step);
            KTUtil.scrollTop();
            this['step' + step].renderHtml();
        }
    },
    validateRequiredFields(){
        return new Promise((resolve, reject) => {
            locationWizard.invalid = false;

            let stepWrapper = $('.step' + locationWizard.wizard.currentStep);

            stepWrapper.find('[required]:not([disabled]):not(:hidden)').each(function(){
                let singleInvalid = $(this).val() === '';

                $(this).toggleClass('is-invalid', singleInvalid);

                if (singleInvalid) {
                    locationWizard.invalid = true;
                    $(this).siblings('.error.invalid-feedback').remove();
                    $(this).after('<div class="error invalid-feedback">This field is required.</div>');
                }
                else {
                    $(this).siblings('.error.invalid-feedback').remove();
                }

                if (locationWizard.wizard.currentStep === 2) {
                    let country = locationWizard.el.attr('data-country');
                    locationWizard.step2.validateAddress(country);
                }
            });

            locationWizard.invalid ? reject() : resolve();
        });
    },
    handleBackendValidationErrors(err){
        if (!err.hasOwnProperty('errors')) {
            return;
        }

        let stepWrapper = $('.step' + this.wizard.currentStep);

        // reset old invalid fields before setting the new
        stepWrapper.find('.error.invalid-feedback').remove();
        stepWrapper.find('.is-invalid').removeClass('is-invalid');

        for (let [key, value] of Object.entries(err.errors)){
            value = Array.isArray(value) ? value.filter(err => err) : value;

            if (!value || !value.length) {
                continue;
            }

            // convert laravel dot notation to square brackets
            let fieldName = key.replace(/\.(.+?)(?=\.|$)/g, (m, s) => `[${ s }]`);

            let field = stepWrapper.find(`[name="${ fieldName }"]`);

            if (field && field.length === 0) {
                continue;
            }

            if (field.hasClass('select2')) {
                field = field.siblings('.select2-container');
            }

            field.toggleClass('is-invalid', true);
            field.siblings('.error.invalid-feedback').remove();

            value.forEach((val) => field.after(`<div class="error invalid-feedback">${ val }</div>`));
        }

        if (this.wizard.currentStep === 1) {
            KTUtil.scrollTo('kt-wizard-v4__wrapper');
        }
    },
    goNext(){
        this.wizard.goNext();
        locationWizard.renderHtml(this.wizard.currentStep);
    },
    goBack(){
        this.wizard.goPrev();
        locationWizard.renderHtml(this.wizard.currentStep);
    },
    init(){
        let currentStep = parseInt(this.el.attr('data-current-step')) || 1;
        this.wizard     = new KTWizard(this.el.attr('id'), {
            startStep: currentStep,
            clickableSteps: true  // allow step clicking
        });

        this.step1.eventHandlers();
        this.step2.eventHandlers();
        this.step3.eventHandlers();
        this.renderHtml(currentStep);

        this.wizard.on('beforeNext', wizardObj => {
            wizardObj.stop();

            locationWizard.validateRequiredFields()
                .then(result => {
                    locationWizard.saveProgress()
                        .then(result => locationWizard.goNext())
                        .catch(err => {
                            locationWizard.handleBackendValidationErrors(err);
                        });
                })
                .catch(() => {
                    if (
                        !locationWizard.step2.email.hasClass('is-invalid') &&
                        !locationWizard.step2.phone.hasClass('is-invalid')
                    ) {
                        KTUtil.scrollTop();
                    }
                });
        });

        this.wizard.on('beforePrev', wizardObj => {
            wizardObj.stop();
            locationWizard.goBack();
        });
    },
    addLoadingWrapper(el){
        if (!el.hasClass('loading')) {
            el.addClass('loading').append('<div class="loader-wrap"><div class="loader">Loading...</div></div>');
        }
    },
    removeLoadingWrapper(el){
        el.removeClass('loading');
        el.find('.loader-wrap').remove();
    }
};

$(document).ready(function(){

    if (locationWizard.el.length) {
        locationWizard.init();

        // prevent "enter" submitting the form
        $(window).keydown(function(event){
            if (event.keyCode === 13) {
                event.preventDefault();
                return false;
            }
        });

        $(document).on('click', '.btn-reset', function(e){
            if (locationWizard.wizard.currentStep === 1) {
                e.preventDefault();
                locationWizard.showResetPopup();
            }
        });

        $(document).on('click', '.wizard-form button[type="submit"]', function(e){
            e.preventDefault();
            $(this).prop('disabled', true);
            $(this).prepend('<i class="fa fa-spinner fa-spin"></i>');
            let btn        = $(this);
            let submitType = $(this).attr('name');
            locationWizard.saveProgress().then(result => {
                locationWizard.submitForm(submitType, btn);
            });
        });

        $(document).on('input', '[required]:not([disabled]):not(:hidden)', delayFn(function(e){
            if ($(this).val().length > 0) {
                $(this).toggleClass('is-invalid', false);
                $(this).siblings('.invalid-feedback').remove();
            }
            else {
                locationWizard.invalid = true;
                $(this).toggleClass('is-invalid', true);
                $(this).siblings('.invalid-feedback').remove();
                $(this).after('<div class="error invalid-feedback">This field is required.</div>');
            }
        }, 500));

        locationWizard.el.find('.kt-wizard-v4__nav-item').click(function(e){
            e.preventDefault();
            let step = parseInt($(this).attr('data-step'));
            if (step > locationWizard.wizard.currentStep) {
                return false;
            }
            locationWizard.wizard.goTo(step, true);
        });
    }

    // reuse functionality for Step1 in Business edit form
    if ($('.business-edit-form').length) {
        locationWizard.step1.eventHandlers();
    }

    // reuse functionality for Step2 in Location Address form
    if ($('.location-address-edit-form').length) {
        locationWizard.step2.resolveAddress();
        locationWizard.step2.eventHandlers();
    }

    // reuse functionality for step3 in Location Hours form
    if ($('.location-hours-edit-form').length) {
        locationWizard.step3.eventHandlers();
        locationWizard.step3.timeTypeValue = locationWizard.step3.timeTypeRadio.filter(':checked').val();
        if (locationWizard.step3.timeTypeValue) {
            locationWizard.step3.renderHolidayTable();
            let timeTypeEl     = locationWizard.step3.timeTypes[locationWizard.step3.timeTypeValue];
            let isTabs         = timeTypeEl.find('.tab-pane.active').length > 0;
            let table          = isTabs ? timeTypeEl.find('.tab-pane.active .week-times .regular-hours-table') : locationWizard.step3.table;
            let exceptionTable = isTabs ? timeTypeEl.find('.tab-pane.active .exception-times .regular-hours-table') : locationWizard.step3.table;
            locationWizard.step3.syncTableData(JSON.parse(table.attr('data-regular')), table);
            locationWizard.step3.syncTableData(JSON.parse(table.attr('data-exceptions')), exceptionTable);
            timeTypeEl.show();
        }
    }
});
