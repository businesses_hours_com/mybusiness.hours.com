$(document).ready(function () {
  $('.business-edit-form .btn[type="submit"]').click(function (e) {
    e.preventDefault();
    let timeForwardedThisYear = parseInt($(this).attr('data-times-forwarded-this-year'));
    let oldName = $(this).attr('data-old-name');
    let newName = $(this).parents('form').find('.new-business-name').val();

    if ( oldName !== newName && timeForwardedThisYear >= 2 ) {
      swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'This business already changed names 2 times this year, you\'re not allowed another one.',
        confirmButtonClass: 'btn btn-secondary'
      });
    }
    else {
      $(this).parents('form').submit();
    }
  });
});