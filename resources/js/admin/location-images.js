const Uppy        = require('@uppy/core');
const Dashboard   = require('@uppy/dashboard');
const Form        = require('@uppy/form');
const XHRUpload   = require('@uppy/xhr-upload');
const GoogleDrive = require('@uppy/google-drive');
const Dropbox     = require('@uppy/dropbox');
const Instagram   = require('@uppy/instagram');
const Facebook    = require('@uppy/facebook');
const OneDrive    = require('@uppy/onedrive');
const Webcam      = require('@uppy/webcam');
const ImageEditor = require('@uppy/image-editor');
const Url         = require('@uppy/url');
const DropTarget  = require('@uppy/drop-target');

require('@uppy/core/dist/style.css');
require('@uppy/dashboard/dist/style.css');

const locales = {
    'nl': require('@uppy/locales/lib/nl_NL'),
    'be': require('@uppy/locales/lib/nl_NL'),
    'fr': require('@uppy/locales/lib/fr_FR'),
    'de': require('@uppy/locales/lib/de_DE'),
    'uk': require('@uppy/locales/lib/en_US'),
    'us': require('@uppy/locales/lib/en_US')
};

document.addEventListener('DOMContentLoaded', function(event){

    if (document.getElementById('uppy') === null) {
        return;
    }

    const uppy = new Uppy({
        debug: true,
        autoProceed: false,
        restrictions: {
            maxFileSize: 1048576,
            maxNumberOfFiles: 6,
            minNumberOfFiles: 1,
            allowedFileTypes: ['image/*']
        },
        // locale: locales['us']
        locale: locales[window.locale]
    })
        .use(Dashboard, {
            inline: true,
            target: '#uppy',
            replaceTargetContent: true,
            showProgressDetails: true,
            height: 470,
            metaFields: [
                {id: 'name', name: 'Name', placeholder: 'file name'}
            ],
            browserBackButtonClose: false
        })
        .use(Form, {
            target: '.images-form',
            resultName: 'uppy_images',
            getMetaFromForm: true,
            formData: false,
            multipleResults: false,
            submitOnSuccess: false,
            triggerUploadOnSubmit: false
        })
        .use(XHRUpload, {
            endpoint: document.getElementById('uppy').getAttribute('data-route'),
            headers: {
                'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
            }
        })
        // .use(GoogleDrive, {target: Dashboard, companionUrl: 'https://companion.uppy.io'})
        // .use(Dropbox, {target: Dashboard, companionUrl: 'https://companion.uppy.io'})
        // .use(Instagram, {target: Dashboard, companionUrl: 'https://companion.uppy.io'})
        // .use(Facebook, {target: Dashboard, companionUrl: 'https://companion.uppy.io'})
        // .use(OneDrive, {target: Dashboard, companionUrl: 'https://companion.uppy.io'})
        // .use(Url, {target: Dashboard, companionUrl: 'https://companion.uppy.io/'})
        .use(Webcam, {
            target: Dashboard,
            modes: [
                'picture'
            ],
            mirror: true,
            videoConstraints: {
                facingMode: 'user',
                width: {min: 720, ideal: 1280, max: 1920},
                height: {min: 480, ideal: 800, max: 1080}
            }
        })
        .use(ImageEditor, {target: Dashboard})
        .use(DropTarget, {target: document.body});

});
