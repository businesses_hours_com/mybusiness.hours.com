let step3 = {
    timeTypes: {
        all: $('.times'),
        regular: $('.step3 .regular-times'),
        departments: $('.step3 .department-times'),
        seasonal: $('.step3 .seasonal-times')
    },
    timeTypeRadio: $('.step3 .times-type-radio'),
    timeTypeValue: 'regular',
    holidaysFromApi: null,
    addTimeBtn: $('.step3 .btn-add-time'),
    tabContent: $('.step3 .tab-content'),
    table: $('.step3 .regular-times .regular-hours-table[data-type="regular"]'),
    backBtn: $('.btn-back'),
    promotionsBtn: $('.btn-promotions'),
    addNewLocationBtn: $('.btn-add-new-location'),
    isDirty: false,

    addNewTimeRow(thisRow){
        let table   = thisRow.parents('.regular-hours-table');
        let newRow  = thisRow.clone();
        let rows    = table.find('tr[data-day-num="' + thisRow.attr('data-day-num') + '"]');
        let lastRow = rows.last();
        newRow.find('.td-day').html('');
        newRow.find('.td-open').html('');
        newRow.find('.td-actions .btn-add-time').remove();
        newRow.find('.td-actions .btn-remove-exception').remove();
        newRow.find('.td-actions .btn-remove-time').show();
        newRow.find('input').prop('checked', false).inputmask();
        newRow.find('input:not([type="checkbox"])').val('');

        let appointmentCheckbox = newRow.find('.appointment-checkbox');
        let appointmentNewName  = appointmentCheckbox.attr('name').replace(/\[(\d+)\]$/g, '[' + rows.length + ']');
        appointmentCheckbox.prop('checked', false).attr('name', appointmentNewName);
        newRow.find('.appointment-hidden-default').attr('name', appointmentNewName).val('off');

        newRow.insertAfter(lastRow);
    },

    handleTimeInputsAvailability(el, isOpen){
        let dayNum = el.parents('tr').attr('data-day-num');
        let table  = el.parents('.regular-hours-table');
        let rows   = table.find('tr[data-day-num="' + dayNum + '"]');
        rows.find('.opening-time').prop('disabled', !isOpen).toggleClass('d-none', !isOpen);
        rows.find('.closing-time').prop('disabled', !isOpen).toggleClass('d-none', !isOpen);
        rows.find('.td-appointment label').toggleClass('d-none', !isOpen);
        rows.find('.btn-add-time').toggleClass('d-none', !isOpen);

        if (isOpen) {
            el.removeClass('btn-outline-danger').addClass('btn-outline-success');
            rows.find('.opening-time').each(function(index, timeInput){
                let val = $(timeInput).attr('value') || '';
                $(timeInput).val(val).inputmask();
            });
            rows.find('.closing-time').each(function(index, timeInput){
                let val = $(timeInput).attr('value') || '';
                $(timeInput).val(val).inputmask();
            });
        }
        else {
            if (!el.val().includes('open_24')) {
                el.removeClass('btn-outline-success').addClass('btn-outline-danger');
            }
            rows.each(function(index, row){
                if (index !== 0) {
                    $(row).remove();
                }
            });
        }
        rows.find('.exception-reason').length ? rows.find('.exception-reason').prop('disabled', isOpen).toggleClass('d-none', isOpen) : '';
    },

    copyToRow(index, el, copyToDay){
        if (copyToDay === '') {
            return;
        }

        $(el).find('select.copy-to').val('').trigger('change');

        let openStatus = $(el).find('.open-status').length ? $(el).find('.open-status').val() : 'closed';

        if ($(el).find('.open-status').length === 0) {
            let currentDay = $(el).attr('data-day-num');
            openStatus     = $(el).siblings('[data-day-num="' + currentDay + '"]').eq(0).find('.open-status').val();
        }

        let isAppointment     = $(el).find('.appointment-checkbox').is(':checked');
        let openingTime       = $(el).find('.opening-time').val();
        let closingTime       = $(el).find('.closing-time').val();
        let holidayOpenStatus = $(el).find('.holiday-open-status').val();
        let copyToRow         = $(el).siblings('tr[data-day-num="' + copyToDay + '"]');
        if (copyToRow.eq(index).length) {
            if (parseInt(index) === 0) {
                copyToRow.eq(index).find('.open-status').prop('disabled', false).val(openStatus).trigger('change');
            }
            copyToRow.eq(index).find('.holiday-open-status').prop('disabled', false).val(holidayOpenStatus).trigger('change.select2');
            copyToRow.eq(index).find('.opening-time').val(openingTime).prop('disabled', openStatus === 'closed');
            copyToRow.eq(index).find('.closing-time').val(closingTime).prop('disabled', openStatus === 'closed');
            copyToRow.eq(index).find('.appointment-checkbox').prop('checked', isAppointment);
        }
        else {
            this.addNewTimeRow(copyToRow.eq(0));
            this.copyToRow(index, el, copyToDay);
        }
    },

    handleSundayAvailability(previous, el){
        let valueArray = el.val();
        if (valueArray.includes('closed')) {
            el.removeClass('btn-outline-success').addClass('btn-outline-danger');
            el.find('option:not([value="closed"])').prop('disabled', true).prop('selected', false);
            el.val(['closed']);
        }
        else {
            el.removeClass('btn-outline-danger').addClass('btn-outline-success');
            if (valueArray.indexOf('closed') > -1) {
                valueArray.splice(valueArray.indexOf('closed'), 1);
            }
            el.val(valueArray);

            if (valueArray.includes('open_all')) {
                el.find('option[value^="open"]:not([value="open_all"]):not([value="open_24"])').prop('disabled', true).prop('selected', false);
            }
            else {
                el.find('option:not([value="closed"])').prop('disabled', false);
            }
        }
        el.trigger('change.select2');
    },

    addNewTab(addBtn){
        let parentWrap   = addBtn.parents('.times');
        let newNum       = parentWrap.find('.nav-tabs').children().length - 1;
        let isDepartment = parentWrap.hasClass('department-times');
        let translations = window.trans[window.locale].wizard.step3 || {};
        let newTabName   = isDepartment ? translations.new_department : translations.new_season_period;
        let newLabel     = isDepartment ? translations.department_name : translations.season_period;
        let newInputText = isDepartment ? translations.enter_your_department_name : translations.enter_your_season_period;
        let tabId        = isDepartment ? 'department_tab_' + newNum : 'seasonal_tab_' + newNum;

        parentWrap.find('.nav-link.active').removeClass('active');
        addBtn.closest('li').before(
            '<li class="nav-item active"><a class="nav-link active" href="#' + tabId + '" data-toggle="tab">' +
            newTabName + '</a>' +
            '<span class="remove-tab">&#10006;</span>' +
            '</li>'
        );

        let firstTabContent = parentWrap.find('.tab-pane').eq(0);

        parentWrap.find('.tab-pane.active').removeClass('active');
        parentWrap.find('.tab-content').append(firstTabContent.clone());

        let newTabContent    = parentWrap.find('.tab-pane').eq(newNum);
        let newNameAttribute = firstTabContent.find('.tab-name').attr('name').replace(/\[(\d+)\]/g, '[' + newNum + ']');
        newTabContent.attr('id', tabId).addClass('active');
        newTabContent.find('[name]').each(function(index, el){
            let nameAttr = $(el).prop('name');
            let count    = 0;
            let newName  = nameAttr.replace(/\[(\d+)\]/g, function(match){
                count++;
                return ( count === 1 ) ? '[' + newNum + ']' : match;
            });
            $(el).attr('name', newName);
        });

        if (isDepartment) {
            let isMainDepartment = firstTabContent.find('.isMainDepartmentCheckbox');
            if (isMainDepartment) {
                let newIsMainNameAttribute = isMainDepartment.attr('name').replace(/\[(\d+)\]/g, '[' + newNum + ']');
                newTabContent.find('.isMainDepartmentCheckbox').attr('name', newIsMainNameAttribute).prop('checked', false);
            }
        }

        newTabContent.find('.tab-name').attr('data-num', newNum).attr('name', newNameAttribute).val('');
        newTabContent.find('.label').text(newLabel);
        newTabContent.find('.form-text.text-muted').text(newInputText);
        newTabContent.find('.appointment-checkbox').prop('checked', false).parents('label').removeClass('d-none').addClass('d-none');
        newTabContent.find('input.opening-time, input.closing-time').attr('disabled', true).val('').removeClass('d-none').addClass('d-none');
        newTabContent.find('.btn-add-time').removeClass('d-none').addClass('d-none');
        newTabContent.find('.select2-container').remove();


        newTabContent.find('.open-status').removeClass('btn-outline-success').addClass('btn-outline-danger').val('closed').trigger('change');
        newTabContent.find('.tab-name[data-num="' + newNum + '"]').val('');
        newTabContent.find('.date-range[data-num="' + newNum + '"]').daterangepicker({
            locale: window.datepickerLocaleObject
        }).val('').attr('placeholder', 'Choose');

        newTabContent.find('.copy-to').select2({
            placeholder: 'Copy to',
            width: '100%',
            minimumResultsForSearch: 10
        });

        newTabContent.find('.sunday-status').select2({
            placeholder: 'Select',
            width: '100%',
            minimumResultsForSearch: 10
        }).toggleClass('btn-outline-success', false).toggleClass('btn-outline-danger', true).val('closed').trigger('change');

        newTabContent.find('table').attr('data-regular', '[]').attr('data-holidays', '[]').attr('data-exceptions', '[]');

        let exceptionsTable = newTabContent.find('table[data-type="exceptions"]');
        exceptionsTable.find('tbody tr:not(:first-child)').remove();
        exceptionsTable.find('tbody tr:first-child').css('display', 'none');
    },

    updateTabText(tabNameInput){
        let num        = tabNameInput.attr('data-num');
        let parentWrap = tabNameInput.parents('.times');
        let value      = tabNameInput.val();
        if (value) {
            parentWrap.find('.nav-link[href*="tab_' + num + '"]').text(tabNameInput.val());
        }
    },

    addExceptionRow(){
        let exceptionTable = this.timeTypes[this.timeTypeValue].find('.tab-pane.active .exception-times .regular-hours-table');
        let firstRow       = exceptionTable.find('tbody tr:first-child');
        let LastDayNum     = exceptionTable.find('tbody tr:last-child').attr('data-day-num');
        let newRow         = firstRow.clone().css('display', 'table-row');
        let newDataNum     = parseInt(LastDayNum) + 1;
        newRow.attr('data-day-num', newDataNum);
        newRow.find('.open-status').prop('disabled', false).val('closed').trigger('change');
        newRow.find('.open-status option').attr('selected', false);
        newRow.find('.open-status option[value="closed"]').attr('selected', true);

        newRow.find('.date-range').daterangepicker({
            locale: window.datepickerLocaleObject
        }).val('').attr('placeholder', 'Choose').prop('disabled', false);
        newRow.find('.opening-time').val('').toggleClass('d-none', true).prop('disabled', true).inputmask();
        newRow.find('.closing-time').val('').toggleClass('d-none', true).prop('disabled', true).inputmask();
        newRow.find('.appointment-checkbox').prop('checked', false);
        newRow.find('.appointment-checkbox').parents('label').removeClass('d-none').addClass('d-none');
        newRow.find('[name]').each(function(index, el){
            let nameAttr = $(el).prop('name');
            let count    = 0;
            let newName  = nameAttr.replace(/\[(\d+)\]/g, function(match){
                count++;
                return ( count === 2 ) ? '[' + newDataNum + ']' : match;
            });
            $(el).attr('name', newName);
        });
        newRow.find('.exception-reason').toggleClass('d-none', false).prop('disabled', false);
        newRow.find('.btn-add-time').toggleClass('d-none', true);
        exceptionTable.find('tbody tr:last-child').after(newRow);
        $('.day-name .invalid-feedback').hide();

        // remove the hidden template html
        if (firstRow.css('display') === 'none') {
            firstRow.remove();
        }
    },

    renderHolidayTable(){
        let url          = $('.step3').attr('data-holidays-api-route');
        let holidayTable = this.timeTypes[this.timeTypeValue].find('.tab-pane.active .holiday-times .regular-hours-table');

        if (holidayTable.find('tbody tr').length !== 1) {
            return false;
        }

        let holidaysData = JSON.parse(holidayTable.attr('data-holidays'));
        if (this.holidaysFromApi) {
            this.renderHolidaysRows(holidayTable);
            this.syncTableData(holidaysData, holidayTable);
        }
        else {
            axios.get(url)
                .then(function(response){
                    if (response.data) {
                        step3.holidaysFromApi = response.data;
                        step3.renderHolidaysRows(holidayTable);
                    }
                }).then(function(){
                step3.syncTableData(holidaysData, holidayTable);
            }).catch(function(error){
                console.log(error);
            });
        }
    },

    renderHolidaysRows(holidayTable){
        holidayTable.find('tbody tr:not(:first-child)').remove();
        holidayTable.attr('data-holidays-ids', Object.keys(step3.holidaysFromApi));
        let firstRow = holidayTable.find('tbody tr:first-child');
        Object.keys(step3.holidaysFromApi).forEach(function(id){
            let newRow       = firstRow.clone();
            let copyToSelect = newRow.find('.copy-to');
            Object.keys(step3.holidaysFromApi).forEach(function(holidayId){
                if (id !== holidayId) {
                    let holidayName  = step3.holidaysFromApi[holidayId]['translation'] || step3.holidaysFromApi[holidayId]['name'];
                    let newCopyToRow = new Option(holidayName, holidayId, false, false);
                    copyToSelect.append(newCopyToRow);
                }
            });
            let holidayName = step3.holidaysFromApi[id]['translation'] || step3.holidaysFromApi[id]['name'];
            newRow.attr('data-day-num', id);
            newRow.find('.day-name span').text(holidayName + ' (' + step3.holidaysFromApi[id]['short_date'] + ')');
            newRow.find('.day-name').attr('data-label', holidayName);
            newRow.find('input').attr('checked', false).inputmask();
            newRow.find('input:not([type="checkbox"])').val('');
            newRow.find('.hidden-name').val(step3.holidaysFromApi[id]['short_name']);
            newRow.find('.opening-time, .closing-time').attr('disabled', true).toggleClass('d-none', true);
            newRow.find('[name]').each(function(index, el){
                let nameAttr = $(el).prop('name');
                let count    = 0;
                let newName  = nameAttr.replace(/\[(\d+)\]/g, function(match){
                    count++;
                    return ( count === 2 ) ? '[' + id + ']' : match;
                });
                $(el).attr('name', newName);
            });
            newRow.find('select.open-status option[value*="open"]').attr('selected', false);
            newRow.find('select.open-status option[value="closed"]').attr('selected', true);
            newRow.find('select.open-status').prop('disabled', false).val('closed').change().toggleClass('btn-outline-success', false).toggleClass('btn-outline-danger', true);
            holidayTable.find('tbody tr:last-child').after(newRow);
        });
        firstRow.remove();
        holidayTable.find('.copy-to').select2({
            placeholder: 'Copy to',
            width: '100%',
            minimumResultsForSearch: 10
        });
    },

    syncTableData(data, table){
        if (Object.keys(data).length > 0) {
            for (let id in data){

                if (data.hasOwnProperty(id)) {
                    let rowData    = data[id];
                    let openStatus = rowData.hasOwnProperty('open_status') ? rowData['open_status'] : 'closed';

                    if (rowData.hasOwnProperty('opening_time')) {
                        for (let index in rowData['opening_time']){
                            if (rowData['opening_time'].hasOwnProperty(index)) {
                                let row = table.find('tr[data-day-num="' + id + '"]').eq(index);
                                if (row.length === 0 || ( parseInt(id) === 0 && row.is(':hidden') )) {
                                    let previousRow = table.find('tr[data-day-num="' + id + '"]').eq(parseInt(index) - 1);
                                    previousRow.length ? step3.addNewTimeRow(previousRow) : step3.addExceptionRow();
                                    row = table.find('tr[data-day-num="' + id + '"]').eq(parseInt(index));
                                }
                                this.updateTableData(row, rowData, index, openStatus);
                            }
                        }
                    }
                    else {
                        let row = table.find('tr[data-day-num="' + id + '"]').eq(0);
                        if (row.length === 0 && table.parents('.exception-times').length && row.hasOwnProperty('period')) {
                            step3.addExceptionRow();
                            row = table.find('tr[data-day-num="' + id + '"]').eq(0);
                        }
                        this.updateTableData(row, rowData, 0, openStatus);
                    }
                }
            }
        }
    },

    updateTableData(row, rowData, index, openStatus){
        if (row.css('display') !== 'none') {
            row.find('.open-status').prop('disabled', false);
            row.find('.date-range').prop('disabled', false);
        }
        row.find('.open-status option').attr('selected', false);
        row.find('.open-status [value="' + openStatus + '"]').attr('selected', true);
        row.find('.open-status').val(openStatus).change().toggleClass('btn-outline-success', openStatus !== 'closed').toggleClass('btn-outline-danger', openStatus === 'closed');

        let openStatusArray = Array.isArray(openStatus) ? openStatus : [openStatus];
        let isDisabled      = openStatusArray.includes('closed') || openStatusArray.includes('open_24') || openStatusArray.includes('open_regular');

        row.find('.opening-time').val(rowData.hasOwnProperty('opening_time') ? rowData['opening_time'][index] : '').prop('disabled', isDisabled).toggleClass('d-none', isDisabled);
        row.find('.closing-time').val(rowData.hasOwnProperty('closing_time') ? rowData['closing_time'][index] : '').prop('disabled', isDisabled).toggleClass('d-none', isDisabled);

        let appointmentCheckbox = row.find('.appointment-checkbox');
        let appointmentNewName  = appointmentCheckbox.attr('name').replace(/\[(\d+)\]$/g, '[' + index + ']');
        appointmentCheckbox
            .prop('checked', rowData.hasOwnProperty('appointment') ? rowData['appointment'][index] === 'on' : false)
            .attr('name', appointmentNewName);
        row.find('.appointment-hidden-default').attr('name', appointmentNewName);

        if (openStatus === 'open_24') {
            row.find('.opening-time').val('00:00');
            row.find('.closing-time').val('00:00');
            row.find('.btn-add-time').toggleClass('d-none', true);
        }
        if (row.parents('.exception-times').length > 0) {
            row.find('.date-range').val(rowData.hasOwnProperty('period') ? rowData['period'] : '');
            row.find('.exception-reason').val(rowData.hasOwnProperty('reason') ? rowData['reason'] : '').prop('disabled', openStatus !== 'closed');
        }
    },

    renderButtons(){
        $('.kt-form__actions').find('.btn').toggleClass('d-none', true);
        let showButtons = this.timeTypeRadio.filter(':checked').length !== 0;
        this.promotionsBtn.toggleClass('d-none', !showButtons);
        this.backBtn.toggleClass('d-none', false);
        this.handleNewSubmitLocationBtn(showButtons);
    },

    renderHtml(forceChange = true){
        let checkedType = this.timeTypeRadio.filter(':checked');

        if (checkedType.length === 0 || !this.timeTypeValue || !this.timeTypes.hasOwnProperty(this.timeTypeValue)) {
            return;
        }

        this.timeTypeValue = checkedType.val();

        if (forceChange) {
            this.renderButtons();
            checkedType.change();
        }

        let timeTypeEl     = this.timeTypes[this.timeTypeValue];
        let isTabs         = timeTypeEl.find('.tab-pane.active').length;
        let table          = isTabs ? timeTypeEl.find('.tab-pane.active .week-times .regular-hours-table') : this.table;
        let exceptionTable = isTabs ? timeTypeEl.find('.tab-pane.active .exception-times .regular-hours-table') : this.table;
        this.syncTableData(JSON.parse(table.attr('data-regular')), table);
        this.syncTableData(JSON.parse(table.attr('data-exceptions')), exceptionTable);
        this.renderHolidayTable();
        timeTypeEl.show();
        // let sundayStatus = $('.step3 .sunday-status');
        // sundayStatus.trigger('change');
        // sundayStatus.trigger('change.select2');
    },

    handleNewSubmitLocationBtn(showButtons){
        let submitNewLocationBtnClass = 'btn-submit-new-location';
        let submitNewLocationBtn      = $('.' + submitNewLocationBtnClass);
        if (submitNewLocationBtn.length) {
            submitNewLocationBtn.toggleClass('d-none', false);
        }
        else {
            submitNewLocationBtn = this.promotionsBtn.clone();
            submitNewLocationBtn.removeClass('btn-brand').addClass('btn-store-new-location btn-success').text(submitNewLocationBtn.attr('data-add-new-location-btn-text')).toggleClass('d-none', !showButtons).attr('name', 'new-location');
            this.promotionsBtn.before(submitNewLocationBtn);
        }
    },

    resetFormWarning(oldTimeTypeValue, newTimeTypeValue){
        let translations = window.trans[window.locale].wizard.step3 || {};
        swal.fire({
            type: 'error',
            title: translations.reset_current_input,
            text: translations.no_revert,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: translations.reset_btn,
            cancelButtonText: translations.cancel_btn
        }).then((result) => {
            if (result.value) {
                let allTables = $('.tab-pane .table');
                allTables.find('.opening-time, .closing-time').val('');
                allTables.find('.open-status').val('closed').trigger('change');
                step3.isDirty = false;
                this.timeTypeRadio.filter(function(){
                    return $(this).attr('value') === newTimeTypeValue;
                }).click();
            }
        });
    },

    eventHandlers(){
        $(document).on('change input', '.tab-pane.active input:not(.times-type-radio), .tab-pane.active .open-status', function(){
            step3.isDirty = true;
        });

        this.timeTypeRadio.click(function(e){
            let oldValue        = step3.timeTypeValue;
            step3.timeTypeValue = $(this).val();
            if (step3.isDirty) {
                e.preventDefault();
                step3.resetFormWarning(oldValue, step3.timeTypeValue);
                return;
            }

            if (step3.timeTypes.hasOwnProperty(step3.timeTypeValue)) {
                step3.timeTypes.all.hide();
                step3.renderHtml(false);
                step3.isDirty = false;
            }
        });

        $(document).on('click', '.btn-add-time', function(){
            let thisRow    = $(this).parents('tr');
            let table      = $(this).parents('.regular-hours-table');
            let dataDayNum = thisRow.attr('data-day-num');
            if (table.find('tr[data-day-num="' + dataDayNum + '"]').length < 10) {
                step3.addNewTimeRow(thisRow);
            }
        });

        $(document).on('click', '.step3 .btn-remove-time', function(e){
            $(this).parents('tr').remove();
        });

        $(document).on('click', '.step3 .btn-remove-exception', function(e){
            let dayNum           = $(this).parents('tr').attr('data-day-num');
            let rows             = $(this).parents('.table').find('tr[data-day-num="' + dayNum + '"]');
            let firstChildDayNum = $(this).parents('.table').find('tr[data-day-num]:first-child').attr('data-day-num');
            rows.each(function(index, row){
                row = $(row);
                if (parseInt(firstChildDayNum) === parseInt(dayNum) && index === 0) {
                    row.css('display', 'none');
                    row.find('.open-status').prop('disabled', false).val('closed').trigger('change');
                    row.find('.open-status option').attr('selected', false);
                    row.find('.open-status option[value="closed"]').attr('selected', true);
                    row.find('.date-range').daterangepicker({
                        locale: window.datepickerLocaleObject
                    }).val('').attr('placeholder', 'Choose').prop('disabled', false);
                    row.find('.opening-time').val('').toggleClass('d-none', true).prop('disabled', true).inputmask();
                    row.find('.closing-time').val('').toggleClass('d-none', true).prop('disabled', true).inputmask();
                    row.find('.appointment-checkbox').prop('checked', false);
                    row.find('.appointment-checkbox').parents('label').removeClass('d-none').addClass('d-none');
                    row.find('.exception-reason').toggleClass('d-none', false).prop('disabled', false);
                    row.find('.btn-add-time').toggleClass('d-none', true);
                }
                else {
                    row.remove();
                }
            });
        });

        $(document).on('change', '.open-status', function(){
            let el          = $(this);
            let open_status = el.val();
            step3.handleTimeInputsAvailability(el, open_status === 'open');
            if (open_status === 'open_24' || open_status === 'open_regular') {
                el.removeClass('btn-outline-danger').addClass('btn-outline-success');
                let reason = el.parents('tr').find('.exception-reason');
                if (reason) {
                    reason.toggleClass('d-none', true);
                }
            }
        });

        let changedFocus = false;
        $(document).on('keypress', '.opening-time', function(){
            let regex       = /([0-2][0-9]):([0-5][0-9])/;
            let closingTime = $(this).parents('tr').find('.closing-time');
            if (!changedFocus && regex.test($(this).val()) && closingTime.val().length !== 5) {
                closingTime.change().focus();
                changedFocus = true;
            }
        });

        $(document).on('change', 'select.copy-to', function(){
            let thisRow = $(this).parents('tr');
            let value   = $(this).val();
            let dayNum  = thisRow.attr('data-day-num');
            let table   = $(this).parents('.regular-hours-table');

            // copy this day times to all other days (except saturday & sunday)
            if (value === 'all') {
                let days = table.attr('data-type') === 'regular' ?
                    [0, 1, 2, 3, 4] :
                    table.attr('data-holidays-ids').split(',');
                days.forEach(function(day){
                    if (parseInt(day) !== parseInt(dayNum)) {
                        table.find('tr[data-day-num="' + dayNum + '"]').each(function(index, el){
                            step3.copyToRow(index, el, day);
                        });
                    }
                });
            }
            // else just copy to selected days
            else {
                table.find('tr[data-day-num="' + dayNum + '"]').each(function(index, el){
                    step3.copyToRow(index, el, value);
                });
            }
        });

        $(document).on('change', '.step3 .sunday-status', function(){
            let el       = $(this);
            let previous = el.attr('data-last').split(',');
            step3.handleSundayAvailability(previous, el);
            step3.handleTimeInputsAvailability(el, !el.val().includes('closed') && !el.val().includes('open_24'));
            el.attr('data-last', el.val().join(','));
        });

        $(document).on('click', '.step3 .nav-tabs .nav-link:not(.add-tab, .remove-tab)', function(e){
            e.preventDefault();
            let parentWrap = $(this).parents('.times');
            let activeTab  = parentWrap.find($(this).attr('href'));
            parentWrap.find('.tab-pane').toggleClass('active', false);
            activeTab.toggleClass('active', true);

            locationWizard.step3.renderHolidayTable();

            activeTab.find('.sunday-status').select2({
                placeholder: 'Select',
                width: '100%',
                minimumResultsForSearch: 10
            });
            activeTab.find('.copy-to').select2({
                placeholder: 'Copy to',
                width: '100%',
                minimumResultsForSearch: 10
            });
        });

        $(document).on('click', '.step3 .nav-tabs .remove-tab', function(e){
            e.preventDefault();
            let parentWrap = $(this).parents('.times');
            if (parentWrap.find('.tab-pane').length > 1) {
                let tabContentId = $(this).siblings('a').attr('href');
                parentWrap.find(tabContentId).remove();
                $(this).parent().remove();
                parentWrap.find('.nav-tabs li:first-child a:first-child').click();
            }
        });

        $(document).on('click', '.step3 .add-tab', function(e){
            e.preventDefault();
            step3.addNewTab($(this));
        });

        $(document).on('input', '.tab-name', delayFn(function(e){
            step3.updateTabText($(this));
        }, 500));

        $(document).on('change', '.tab-name.date-range', delayFn(function(e){
            step3.updateTabText($(this));
        }, 500));

        $(document).on('input', '.step3 .isMainDepartmentCheckbox', function(e){
            e.preventDefault();
            $('.step3 .isMainDepartmentCheckbox').attr('checked', false);
            $(this).attr('checked', !$(this).attr('checked'));
        });

        $(document).on('click', '.step3 .exception-times .add-exception-btn', function(e){
            step3.addExceptionRow();
        });
    }
};

export default step3;
