let step2 = {
    el: $('.step2'),
    invalidAddressWrap: $('.step2 .invalid-address-wrap'),
    googlePlacesJson: $('.step2 .google-places-json'),
    fullAddress: $('.step2 .full-address'),
    addressId: $('.step2 .address-id'),
    postcode: $('.step2 .postcode'),
    houseNumber: $('.step2 .house-number'),
    streetNumber: $('.step2 .street-number'),
    houseNumberAddition: $('.step2 .addition'),
    street: $('.step2 .street'),
    city: $('.step2 .city'),
    country: $('.step2 .country'),
    lat: $('.step2 .lat'),
    lng: $('.step2 .lng'),
    contactFields: $('.step2 .contact-fields-wrap'),
    email: $('.step2 .email'),
    phone: $('.step2 .phone'),
    localFields: $('.step2 .local-fields-wrap'),
    locationMap: $('.step2 #location-map'),
    nextBtn: $('.btn-next'),
    backBtn: $('.btn-back'),
    addressApiUrl: '',
    marker: null,
    mapLoadingWrapper: $('.map-loading-wrap'),
    map: null,
    bounds: undefined,

    resolveAddress(){

        let country = locationWizard.el.attr('data-country');

        if (this.postcode.val() === '' || (country === 'nl' && this.houseNumber.val() === '')) {
            step2.mapLoadingWrapper.hide();
            return;
        }

        step2.mapLoadingWrapper.show();
        step2.invalidAddressWrap.toggleClass('d-none', true);

        this.addressApiUrl = this.addressApiUrl || this.el.attr('data-address-api-route');
        let url            = this.addressApiUrl + '?postal_code=' + this.postcode.val() + '&house_number=' + this.houseNumber.val() + '&addition=' + this.houseNumberAddition.val() + '&street=' + this.street.val() + '&city=' + this.city.val() + '&lat=' + this.lat.val() + '&lng=' + this.lng.val()+'&google_place_details=' + this.googlePlacesJson.val();

        axios.get(url)
            .then(function(response){
                if (response.data && Object.keys(response.data).length) {
                    let addressData = Array.isArray(response.data) ? response.data[0] : response.data;

                    step2.addressId.val(addressData.addressId);
                    step2.street.val(addressData.street);
                    step2.streetNumber.val(addressData.houseNumber);
                    step2.houseNumber.toggleClass('is-invalid', false).siblings('.invalid-feedback').remove();
                    step2.houseNumber.val(addressData.houseNumber);
                    step2.postcode.toggleClass('is-invalid', false).siblings('.invalid-feedback').remove();
                    step2.postcode.val(addressData.postalCode);
                    step2.houseNumberAddition.val(addressData.houseNumberAddition);
                    step2.city.val(addressData.city);
                    step2.lat.val(addressData.lat);
                    step2.lng.val(addressData.lng);
                    step2.country.val(addressData.countryCode);
                    step2.renderMap();

                    if ($('.step1 .is-retail-chain').is(':checked')) {
                        step2.localFields.show();
                    }
                }
                else {
                    step2.resetAddressFields();
                }
            })
            .catch(function(error){
                step2.invalidAddressWrap.toggleClass('d-none', false);
                console.log(error);
            })
            .finally(function(){
                $('.container-content').removeClass('loading');
                step2.mapLoadingWrapper.hide();
                $('.container-content .loader-wrap').remove();
            });
    },

    resetAddressFields(){
        step2.googlePlacesJson.val('');
        step2.addressId.val('');
        step2.postcode.val('');
        step2.street.val('');
        step2.houseNumber.val('');
        step2.streetNumber.val('');
        step2.houseNumberAddition.val('');
        step2.city.val('');
        step2.lat.val('');
        step2.lng.val('');
        step2.country.val('');
        step2.locationMap.hide();
    },

    autocompleteAddress(placeObject){

        // reset existing values
        step2.resetAddressFields();

        // save the json
        step2.googlePlacesJson.val(JSON.stringify(placeObject));

        // make fields editable
        step2.postcode.removeAttr('readonly');
        step2.houseNumber.removeAttr('readonly');
        step2.houseNumberAddition.removeAttr('readonly');
        step2.street.removeAttr('readonly');
        step2.city.removeAttr('readonly');

        // add values for lat and lng
        step2.lat.val(placeObject.geometry.location.lat());
        step2.lng.val(placeObject.geometry.location.lng());

        // place.address_components are google.maps.GeocoderAddressComponent objects http://goo.gle/3l5i5Mr
        for (const component of placeObject.address_components){
            if(component.types.includes('postal_code') || component.types.includes('"postal_code_prefix"')){
                step2.postcode.val(component.long_name);
            }
            if(component.types.includes('street_number')){
                step2.houseNumber.val(component.long_name);
            }
            if(component.types.includes('premise')){
                step2.houseNumberAddition.val(component.long_name);
            }
            if(component.types.includes('subpremise')){
                let val = step2.houseNumberAddition.val();
                step2.houseNumberAddition.val(val ? `${ val }-${ component.long_name }` : component.long_name);
            }
            if(component.types.includes('street_address') || component.types.includes('route')){
                step2.street.val(component.long_name);
            }
            if(component.types.includes('locality') || component.types.includes('postal_town')){
                step2.city.val(component.long_name);
            }
        }

        if (step2.postcode.val() || step2.houseNumber.val()) {
            step2.postcode.trigger('input');
        }
    },

    validateAddress(country = null){

        locationWizard.invalid = step2.addressId.val().length === 0 || step2.street.val().length === 0 || step2.city.val().length === 0;

        if (locationWizard.invalid){

            if(step2.street.val().length === 0 || step2.city.val().length === 0) {
                this.postcode.siblings('.error.invalid-feedback').remove();
                this.postcode.toggleClass('is-invalid', true).after('<div class="error invalid-feedback">Invalid address</div>');
                this.houseNumber.siblings('.error.invalid-feedback').remove();
                this.houseNumber.toggleClass('is-invalid', true).after('<div class="error invalid-feedback">Invalid address</div>');
            }

            if (country === 'nl') {
                swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: 'We don\'t recognize this address, please update the information.',
                    confirmButtonClass: 'btn btn-secondary'
                });
            }
            else{
                swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: 'We don\'t recognize this address, please update the information.',
                    showCancelButton: true,
                    cancelButtonText: 'Manually add address',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Edit'
                }).then((result) => {
                    // if Manually add address is clicked
                    if (!result.value) {
                        step2.street.attr('readonly', false);
                        step2.city.attr('readonly', false);
                    }
                    else {
                        step2.street.attr('readonly', true);
                        step2.city.attr('readonly', true);
                    }
                });
            }
            return false;
        }
        else {
            return true;
        }
    },

    renderMap(){
        let markerPosition = {
            lat: parseFloat(this.lat.val()),
            lng: parseFloat(this.lng.val())
        };

        this.map = new google.maps.Map(
            document.getElementById('location-map'),
            {
                zoom: 15,
                center: markerPosition,
                mapTypeId: 'terrain'
            }
        );

        this.marker = new google.maps.Marker({
            position: markerPosition,
            map: step2.map,
            draggable: true,
            title: 'Drag me!'
        });

        google.maps.event.addListener(this.map, 'bounds_changed', function(){
            if (step2.bounds === undefined) {
                step2.bounds = this.getBounds();
            }
        });

        google.maps.event.addListener(this.marker, 'dragend', function(e){
            const newPosition = {
                lat: e.latLng.lat(),
                lng: e.latLng.lng()
            };

            let position = step2.bounds.contains(newPosition) ? newPosition : markerPosition;

            this.setPosition(position);
            step2.lat.val(position.lat);
            step2.lng.val(position.lng);
        });

        step2.mapLoadingWrapper.hide();
        step2.locationMap.show();
    },

    showNextButton(){
        this.nextBtn.text(this.nextBtn.attr('data-opening-times-btn-text')).toggleClass('d-none', false);
    },

    renderButtons(){
        $('.kt-form__actions').find('.btn').toggleClass('d-none', true);
        this.backBtn.toggleClass('d-none', false);
        this.showNextButton();
    },

    renderHtml(){
        this.renderButtons();
        this.resolveAddress();
        this.showNextButton();
    },

    eventHandlers(){
        // On postcode and houseNumber change call the api to get address
        this.postcode.add(this.houseNumber).add(this.houseNumberAddition).on('input', delayFn(function(e){
            step2.resolveAddress();
            if (step2.nextBtn.length) {
                step2.showNextButton();
            }
        }, 1000));
    }
};

export default step2;
