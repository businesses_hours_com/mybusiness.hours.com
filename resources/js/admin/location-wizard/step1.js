let step1 = {
    businessNameSelect: $('.step1 #business-name'),
    businessNameInput: $('.step1 .business-name-input'),
    businessNameInputId: $('.step1 .business-name-input-id'),
    businessesTable: $('.step1 .business-table'),
    newBusinessName: $('.step1 .new-business-name'),
    orText: $('.step1 .or-text'),
    businessNameInputWrap: $('.step1 .business-name-input-wrap'),
    selectBusinessWrap: $('.step1 .select-business-wrap'),
    selectLocationsWrap: $('.step1 .select-locations-wrap'),
    locationsTable: $('.step1 .locations-table'),
    addBusinessWrap: $('.step1 .add-business-wrap'),
    businessWebsite: $('.step1 .website'),
    categorySelect: $('.step1 .categories-select'),
    addNewLocationBtn: $('.btn-add-new-location'),
    manageBtn: $('.btn-manage'),
    resetBtn: $('.btn-reset'),
    addBusinessBtn: $('.btn-add-new-business'),
    animationSpeed: 0,
    businessName: '',
    businessId: '',
    locations_count: 0,
    scrapeIconsRoute: $('.step1').attr('data-scrape-icons-route'),
    businesses: [],
    selectedLocations: [],
    locations: [],
    loadBusinessesRequest: null,

    loadBusinesses(){
        this.reset();
        this.selectLocationsWrap.hide(this.animationSpeed);

        if (!this.businessNameInput.val()) {
            return;
        }

        // cancel  previous ajax if exists
        if (this.loadBusinessesRequest) {
            this.loadBusinessesRequest.cancel();
        }

        locationWizard.addLoadingWrapper(this.selectBusinessWrap);

        // creates a new cancel token (overwrite the previous one)
        this.loadBusinessesRequest = axios.CancelToken.source();

        axios
            .get(this.businessNameInput.attr('data-businesses-route'), {
                    cancelToken: this.loadBusinessesRequest.token,
                    params: {
                        name: step1.businessNameInput.val(),
                        limit: 10
                    }
                }
            )
            .then(function(response){
                let data         = response.data ? response.data : [];
                data             = Array.isArray(data) ? data : ( data && data.hasOwnProperty('data') ? data.data : [] );
                step1.businesses = data;
                locationWizard.removeLoadingWrapper(step1.selectBusinessWrap);
                step1.showBusinesses();
            })
            .catch(function(error){
                if (!axios.isCancel(error)) {
                    console.log(error);
                    locationWizard.removeLoadingWrapper(step1.selectBusinessWrap);
                }
            });
    },

    showBusinesses(){
        this.businessesTable.find('tbody').empty();
        this.businessesTable.siblings('.no-businesses-found').remove();

        this.selectBusinessWrap.show(this.animationSpeed);
        this.addBusinessBtn.toggleClass('d-none', false);

        if (this.businesses.length === 0) {
            this.businessesTable.hide();
            let message = this.businessesTable.attr('data-found-no-businesses');
            this.businessesTable.before('<p class="no-businesses-found mb-0">' + message + '</p>');
        }
        else {
            this.businessesTable.siblings('.no-businesses-found').remove();
            this.businessesTable.show();
            this.businesses.forEach(function(business){
                step1.businessesTable.find('tbody').append(
                    '<tr role="row" class="business-tr">' +
                    '<td>' + business.name + '</td>' +
                    '<td>' + business.locations_count + '</td>' +
                    '<td class="w-25">' +
                    '<button type="button" class="btn btn-brand btn-sm btn-font-sm btn-tall btn-wide kt-font-bold kt-font-transform-u btn-select-business" ' +
                    'data-name="' + business.name + '" ' + 'data-id="' + business.id + '" ' + 'data-locations-count="' + ( business.locations_count || 0 ) + '">' +
                    $('th.add_manage_location').text() +
                    '</button>' +
                    '</td>' +
                    '</tr>'
                );
            });
        }
    },

    resolveLocations(){
        this.selectBusinessWrap.hide(this.animationSpeed);
        this.selectLocationsWrap.show(this.animationSpeed);
        this.selectLocationsWrap.find('input').prop('disabled', false);
        this.addBusinessBtn.toggleClass('d-none', true);
        this.orText.hide(this.animationSpeed);
        this.addBusinessWrap.hide(this.animationSpeed);
        this.addBusinessWrap.find('input').prop('disabled', true);
        this.addBusinessWrap.find('select').prop('disabled', true);

        let heading = this.selectLocationsWrap.find('.kt-heading');
        heading.text(heading.attr('data-text') + ' ' + this.businessName);

        locationWizard.addLoadingWrapper(this.selectLocationsWrap);

        axios.get(this.businessNameInput.attr('data-locations-route'), {
            params: {
                'business_id': step1.businessId,
                'obfuscate_emails': true
            }
        })
            .then(function(response){
                step1.locations = response.data || [];
                if (step1.locations.length === 0) {
                    step1.locations_count = 0;
                }
                step1.showLocations();
            })
            .catch(function(error){
                console.log('resolve locations error');
                console.log(error);
            })
            .finally(function(){
                locationWizard.removeLoadingWrapper(step1.selectLocationsWrap);
            });
    },

    showLocations(){
        this.selectedLocations = JSON.parse(this.businessNameInput.attr('data-selected-locations'));
        this.manageBtn.toggleClass('d-none', this.selectedLocations.length === 0);
        this.addNewLocationBtn.toggleClass('d-none', false);
        this.resetBtn.toggleClass('d-none', false);

        step1.locationsTable.DataTable({
            data: step1.locations,
            responsive: true,
            destroy: true,
            deferRender: true,
            language: {
                emptyTable: 'We couldn\'t find any locations for this business'
            },
            columnDefs: [
                {
                    orderable: false,
                    className: 'select-checkbox',
                    targets: 0
                }],
            select: {
                style: 'os',
                selector: 'td:first-child'
            },
            columns: [
                {
                    'data': 'id',
                    'render': (data, type, row, meta) => {
                        let isSelected     = step1.selectedLocations.hasOwnProperty(row.id);
                        let isAlreadyOwned = !isSelected && row.users.length > 0;
                        let baseClasses    = 'kt-switch kt-switch-- kt-switch--sm kt-switch--icon d-block';
                        let labelClasses   = 'pt-1 mb-0';

                        if (isAlreadyOwned) {
                            return '<span data-owner-email="'+row.users[0].email+'" class="request-access-modal">' + step1.locationsTable.attr('data-owned-location-text') + '</span>';
                        }

                        return '<span class="' + baseClasses + '"><label class="' + labelClasses + '"><input type="checkbox" class="select-location-checkbox" ' + ( isSelected ? 'checked' : '' ) + ' name="step1[locations][' + row.id + ']" data-id="' + row.id + '"><span></span></label></span>';
                    }
                },
                {'data': 'address_schema.postal_code'},
                {'data': 'city.name'},
                {
                    'data': 'address_schema.street',
                    'render': (data, type, row, meta) =>
                        row.address_schema.street + ' ' + row.address_schema.street_number
                }
            ]
        });

        // Add select all checkbox
        this.addSelectAllLocationsCheckbox();
    },

    reset: function(shouldResetInput = true){
        this.businessNameInputWrap.show(this.animationSpeed);
        this.addBusinessWrap.hide(this.animationSpeed);
        this.selectBusinessWrap.hide(this.animationSpeed);
        this.selectLocationsWrap.hide(this.animationSpeed);
        this.businessNameInput.prop('disabled', false);
        this.addBusinessWrap.find('select').prop('disabled', true);
        this.locationsTable.find('.location-tr').remove();
        this.selectLocationsWrap.find('input').val('').prop('disabled', false);
        this.selectLocationsWrap.find('input[type="checkbox"]').prop('checked', false);
        this.manageBtn.toggleClass('d-none', true);
        this.resetBtn.toggleClass('d-none', true);
        this.addNewLocationBtn.toggleClass('d-none', true);
        this.addBusinessBtn.toggleClass('d-none', true);
        if (shouldResetInput) {
            this.addBusinessWrap.find('input').prop('disabled', false);
            this.addBusinessWrap.find('input[type="checkbox"]').prop('checked', false);
            Dropzone.forElement('.dropzone-favicon').removeAllFiles(true);
            Dropzone.forElement('.dropzone-logo').removeAllFiles(true);
        }
    },

    showNewBusinessFields(shouldResetInput = true){
        this.reset(shouldResetInput);
        this.businessNameInput.prop('disabled', true);
        this.businessNameInputWrap.hide(this.animationSpeed);
        this.addBusinessWrap.show(this.animationSpeed);
        this.addBusinessWrap.find('input').prop('disabled', false);
        this.addBusinessWrap.find('select').prop('disabled', false);
        this.selectLocationsWrap.find('input').prop('disabled', true);
        if (!this.newBusinessName.val()) {
            this.newBusinessName.val(this.businessNameInput.val());
        }
        this.newBusinessName.attr('required', true);
        this.addNewLocationBtn.toggleClass('d-none', false);
        this.resetBtn.toggleClass('d-none', false);
    },

    addSelectAllLocationsCheckbox(){
        $('#locations-table_wrapper .row:last-child').before(
            '<div class="col-sm-12 col-md-5">' +
            '<div class="row">' +
            '<label><input type="checkbox" value="1" name="select_all_locations" class="mr-1"><span>Select all</span></label>' +
            '</div>' +
            '</div>'
        );

        $('[name="select_all_locations"]').click(function(e){
            let table              = step1.locationsTable.DataTable();
            let locationCheckboxes = table.$('.select-location-checkbox');

            if ($(this).siblings('span').text() === 'Select all') {
                locationCheckboxes.prop('checked', true);
                $(this).siblings('span').text('Unselect all');
                $(this).prop('checked', false);
                step1.manageBtn.toggleClass('d-none', false);
            }
            else {
                locationCheckboxes.prop('checked', false);
                $(this).siblings('span').text('Select all');
                $(this).prop('checked', false);
                step1.manageBtn.toggleClass('d-none', true);
            }
            step1.selectedLocations = [];

            locationCheckboxes.each(function(index, el){
                const id                    = $(el).attr('data-id');
                step1.selectedLocations[id] = 'on';
            });

            step1.businessNameInput.attr('data-selected-locations', JSON.stringify(step1.selectedLocations));
        });
    },

    scrapeWebsite(website){
        $('.step1 .social-link').val('');
        axios.post(this.scrapeIconsRoute, {website: website})
            .then(function(response){
                let logo                = response.data.logo;
                let icons               = response.data.icons;
                let social              = response.data.social;
                let showMoreSocialMedia = false;
                for (let iconType in icons){
                    if (icons.hasOwnProperty(iconType) && Object.keys(icons[iconType]).length > 0) {
                        let dropzoneClass = ( iconType === 'favicon' ) ? '.dropzone-favicon' : '.dropzone-logo';
                        step1.addFileToDropzone(dropzoneClass, icons[iconType]);
                    }
                }
                if (Object.keys(logo).length > 0) {
                    step1.addFileToDropzone('.dropzone-logo', logo);
                }
                for (let socialType in social){
                    if (social.hasOwnProperty(socialType) && Object.keys(social[socialType]).length > 0) {
                        $('.social-link.' + socialType).val(social[socialType]);
                        if (['pinterest', 'instagram', 'linkedin', 'foursquare'].includes(socialType)) {
                            showMoreSocialMedia = true;
                        }
                    }
                }
                $('#more-social').toggleClass('show', showMoreSocialMedia);
            })
            .catch(function(error){
                console.log('scrapeWebsite error:');
                console.log(error);
            })
            .finally(function(){
                // always executed
            });
    },

    addFileToDropzone(dropzoneClass, file){
        let dropzone = Dropzone.forElement(dropzoneClass);

        if (dropzone.files && dropzone.files.length > 0) {
            // return true;
            dropzone.removeAllFiles(true);
        }

        $(dropzoneClass).siblings(dropzoneClass + '-url').val(file.url);
        $(dropzoneClass).siblings(dropzoneClass + '-name').val(file.name);
        $(dropzoneClass).siblings(dropzoneClass + '-type').val(file.type);
        $(dropzoneClass).siblings(dropzoneClass + '-size').val(file.size);

        dropzone.on('addedfile', function(file){
            file.previewElement.addEventListener('click', function(){
                let url = $(this).find('img').attr('src');
                if (url.length > 0) {
                    let win = window.open(url, '_blank');
                    win.focus();
                }
            });
        });
        dropzone.addCustomFile({
                // flag: processing is complete
                processing: true,
                // flag: file is accepted (for limiting maxFiles)
                accepted: true,
                // name of file on page
                name: file.name,
                // image size
                size: file.size,
                // image type
                type: file.type,
                // flag: status upload
                status: Dropzone.SUCCESS
            },
            // Thumbnail url
            file.file,
            // Custom response for event success
            {
                status: 'success'
            });
    },

    renderButtons(){
        $('.kt-form__actions').find('.btn').toggleClass('d-none', true);
        this.addNewLocationBtn.text(this.addNewLocationBtn.attr('data-text')).toggleClass('d-none', true);
        if (this.businessNameInput.val()) {
            let showManageBtn = parseInt($('.step1').attr('data-manage-btn')) === 1;
            this.manageBtn.toggleClass('d-none', !showManageBtn);
        }
    },

    renderHtml(){
        this.renderButtons();
        if (this.newBusinessName.val()) {
            let shouldResetInput = false;
            this.showNewBusinessFields(shouldResetInput);
        }
        else if (this.businessNameInput.val()) {
            this.businessName = this.businessNameInput.val();
            this.businessId   = this.businessNameInput.attr('data-id') || this.businessId;
            this.businessNameInputId.val(this.businessId);

            if (this.businessId) {
                this.resolveLocations();
            }
            else {
                this.loadBusinesses();
            }
        }
    },

    eventHandlers(){
        this.businessNameInput.on('input', delayFn(function(e){
            if (e.target.value.length > 3) {
                step1.loadBusinesses();
            }
        }, 400));

        $(document).on('click', '.step1 .btn-select-business', function(e){
            e.preventDefault();
            step1.businessName = $(this).attr('data-name');
            step1.businessId   = $(this).attr('data-id');
            step1.businessNameInputId.val(step1.businessId);
            step1.locations_count = parseInt($(this).attr('data-locations-count'));
            step1.businessNameInput.val(step1.businessName);
            if (step1.locations_count === 0) {
                locationWizard.goNext();
            }
            else {
                step1.resolveLocations();
            }
        });

        this.categorySelect.change(function(){
            let categoryName = $(this).find('option:selected').text();
            $(this).siblings('.category-hidden-name').val(categoryName);
        });

        $(document).on('click', '.select-location-checkbox', function(){
            let table                   = step1.locationsTable.DataTable();
            let locationCheckboxes      = table.$('.select-location-checkbox');
            const id                    = $(this).attr('data-id');
            step1.selectedLocations[id] = 'on';
            step1.manageBtn.toggleClass('d-none', locationCheckboxes.length === 0);
            step1.businessNameInput.attr('data-selected-locations', JSON.stringify(step1.selectedLocations));
        });

        this.addBusinessBtn.click(function(){
            step1.showNewBusinessFields();
        });

        this.businessWebsite.on('input', delayFn(function(e){
            if (!$(this).parents('.form-group').hasClass('is-invalid') && validateUrl($(this).val())) {
                step1.scrapeWebsite($(this).val());
            }
        }, 1000));

        $(document).on('click', '.request-access-modal', function(e){
            e.preventDefault();
            let ownerEmail = $(this).attr('data-owner-email');
            let html       = step1.locationsTable.attr('data-owned-location-modal-text');
            html = html.replace(':email', '<strong>'+ownerEmail+'</strong>');
            let title = step1.locationsTable.attr('data-owned-location-modal-title') || 'Request access';
            swal.fire({
                title: title.charAt(0).toUpperCase() + title.slice(1),
                html: html,
                type: 'warning',
                showConfirmButton: false,
                showCancelButton: false
            });
        });
    }
};

export default step1;
