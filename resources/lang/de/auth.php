<?php

return array (
  'already_have_account' => 'Already have an account ?',
  'confirm_password' => 
  array (
    'confirm_password_btn_text' => 'Confirm password',
    'description' => 'Please confirm your password before continuing.',
    'title' => 'Confirm Password',
  ),
  'email_placeholder' => 'Email',
  'failed' => 'These credentials do not match our records.',
  'forgot_password' => 'Forgot your password?',
  'login' => 
  array (
    'description' => 'Enter your credentials',
    'email_placeholder' => 'Email',
    'login_btn_text' => 'Sign in',
    'password_placeholder' => 'Password',
    'remember_me' => 'Remember me',
    'title' => 'Sign In',
  ),
  'no_account_yet' => 'Don\'t have an account yet ?',
  'password_placeholder' => 'Password',
  'register' => 
  array (
    'account_type' => 'Account type',
    'agree_with_the' => 'I agree with the',
    'business_employee' => 'Business employee',
    'business_owner' => 'Business owner',
    'company_placeholder' => 'Company name',
    'description' => 'Enter your details to create your account',
    'first_name_placeholder' => 'First name',
    'last_name_placeholder' => 'Last name',
    'marketing_company' => 'Marketing company',
    'terms_and_conditions' => 'terms and conditions',
    'title' => 'Sign Up',
  ),
  'register_btn_text' => 'Sign up!',
  'request_new_password' => 
  array (
    'back_to_login_btn_text' => 'Back to login',
    'description' => 'Enter your email to reset your password:',
    'reset_btn_text' => 'Request',
    'title' => 'Forgotten Password ?',
  ),
  'reset_password' => 
  array (
    'confirm_password' => 'Confirm password',
    'description' => 'Enter your new password',
    'reset_password_btn_text' => 'Reset password',
    'title' => 'Reset Password',
  ),
  'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
  'verify_email' => 
  array (
    'description' => 'Before proceeding, please check your email for a verification link.',
    'not_received_yet' => 'If you did not receive the email click below to request another email.',
    'request_another_email_btn_text' => 'Request another email',
    'title' => 'Verify Your Email Address',
    'verification_email_has_been_sent' => 'A fresh verification link has been sent to your email address.',
  ),
);
