<?php

return array (
  'reset' => 'Je wachtwoord is gereset!',
  'sent' => 'We hebben je link voor het opnieuw instellen van je wachtwoord gemaild!',
  'throttled' => 'Wacht alstublieft voordat u het opnieuw probeert.',
  'token' => 'Deze token voor het opnieuw instellen van het wachtwoord is ongeldig.',
  'user' => 'We kunnen geen account met dat e-mailadres vinden.',
);
