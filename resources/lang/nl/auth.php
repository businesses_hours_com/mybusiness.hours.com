<?php

return array (
  'already_have_account' => 'Heb je al een account?',
  'confirm_password' => 
  array (
    'confirm_password_btn_text' => 'Bevestig wachtwoord',
    'description' => 'Bevestig uw wachtwoord voordat u doorgaat.',
    'title' => 'bevestig wachtwoord',
  ),
  'current_password' => 'Mijn huidige wachtwoord',
  'email_placeholder' => 'Email',
  'failed' => 'Deze gegevens komen niet overeen met onze gegevens.',
  'forgot_password' => 'Je wachtwoord vergeten?',
  'login' => 
  array (
    'description' => 'Voer uw inloggegevens in',
    'email_placeholder' => 'Email',
    'login_btn_text' => 'Inloggen',
    'password_placeholder' => 'Wachtwoord',
    'remember_me' => 'Onthoudt mijn gegevens',
    'title' => 'Inloggen',
  ),
  'new_password' => 'Nieuw paswoord',
  'no_account_yet' => 'Registreer hier een nieuw account',
  'password_placeholder' => 'Wachtwoord',
  'register' => 
  array (
    'account_type' => 'Account type',
    'agree_with_the' => 'Ik ga akkoord met de',
    'business_employee' => 'Bedrijfsmedewerker',
    'business_owner' => 'Bedrijfseigenaar',
    'company_placeholder' => 'Bedrijfsnaam',
    'description' => 'Voer uw gegevens in om uw account aan te maken',
    'first_name_placeholder' => 'Voornaam',
    'last_name_placeholder' => 'Achternaam',
    'marketing_company' => 'Anders',
    'terms_and_conditions' => 'Algemene voorwaarden',
    'title' => 'Inschrijven',
  ),
  'register_btn_text' => 'Inschrijven!',
  'request_new_password' => 
  array (
    'back_to_login_btn_text' => 'Terug naar Inloggen',
    'description' => 'Voer uw e-mailadres in om uw wachtwoord opnieuw in te stellen:',
    'reset_btn_text' => 'Aanvragen',
    'title' => 'Wachtwoord vergeten?',
  ),
  'reset_password' => 
  array (
    'confirm_password' => 'Bevestig wachtwoord',
    'description' => 'Voer je nieuwe wachtwoord in',
    'reset_password_btn_text' => 'Wachtwoord opnieuw instellen',
    'title' => 'Wachtwoord opnieuw instellen',
  ),
  'throttle' => 'Te veel inlogpogingen. Probeer het opnieuw over :seconds seconden.',
  'verify_email' => 
  array (
    'description' => 'Controleer voordat u verder gaat uw mailbox voor een e-mail met verificatielink.',
    'not_received_yet' => 'Als je de e-mail niet hebt ontvangen, klik dan hieronder om een ​​nieuwe e-mail aan te vragen.',
    'request_another_email_btn_text' => 'Vraag nog een activatie e-mail aan',
    'title' => 'Verifieer je e-mailadres',
    'verification_email_has_been_sent' => 'Er is een nieuwe verificatielink naar uw e-mailadres gestuurd.',
  ),
  'verify_password' => 'Bevestig het nieuwe wachtwoord',
);
