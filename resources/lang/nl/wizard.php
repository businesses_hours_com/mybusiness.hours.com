<?php

return array (
  'datepicker' =>
  array (
    'december' => 'December',
  ),
  'location_wizard' => 'Location wizard',
  'nav' =>
  array (
    'business' => 'Bedrijf',
    'location' => 'Vestiging',
    'opening_times' => 'Openingstijden',
    'promotions' => 'Promoties',
    'step1_description' => 'Bedrijfsgegevens',
    'step2_description' => 'Adres & Contact',
    'step3_description' => 'Open of dicht?',
    'step4_description' => 'Extra Opvallen (Optioneel)',
  ),
  'promotions' =>
  array (
    'add_btn_text' => 'Add',
    'basic_plan' => 'Basic plan',
    'basic_plan_description_line_1' => 'Get your listing online',
    'basic_plan_description_line_2' => 'for free, including',
    'basic_pricing_description_text' => '(Really free, you pay nothing!)',
    'discount_code' => 'Discount code:',
    'enter_the_code_if_you_have_one' => 'Enter the code if you have one',
    'feature_1' => 'Address',
    'feature_10' => 'Statistics',
    'feature_2' => 'Opening times',
    'feature_3' => 'Exceptions dates',
    'feature_4' => 'Multiple accounts',
    'feature_5' => 'Website shown',
    'feature_6' => 'Business description',
    'feature_7' => 'Show logo',
    'feature_8' => 'Show photos',
    'feature_9' => 'Stand out',
    'finish_btn_text' => 'Finish',
    'free' => 'Free',
    'invoiced_as' => 'Invoiced as',
    'main_heading' => 'Transparent & Simple Pricing',
    'monthly_price_per_location_text' => 'Monthly price per location',
    'no' => 'No',
    'popular_choice' => 'Popular choice',
    'premium_plan' => 'Premium plan',
    'premium_plan_description_line_1' => 'Reach many consumers for',
    'premium_plan_description_line_2' => 'your physical location and',
    'premium_plan_description_line_3' => 'business website',
    'subscribe_btn_text' => 'Subscribe',
    'title' => 'Promotions',
    'year' => 'year',
    'yes' => 'Yes',
  ),
  'step1' =>
  array (
    'add_additional_social_networks' => 'Meer sociale netwerken..',
    'add_location_btn' => 'Voeg locatie toe',
    'add_manage_location' => 'Add/manage location',
    'add_new_btn' => 'Nieuw toevoegen',
    'add_new_business' => 'Nieuw bedrijf toevoegen',
    'add_to_my_account' => 'Toevoegen aan mijn account',
    'address' => 'Adres',
    'business_name' => 'Bedrijfsnaam',
    'business_name_help_text' => 'Voer uw bedrijfsnaam in.',
    'category' => 'Categorie',
    'category_help_text' => 'Voer uw categorie in.',
    'choose_a_category' => 'Kies een categorie',
    'city' => 'Stad',
    'drop_your_favicon_here' => 'Zet je favicon hier neer of klik om te uploaden.',
    'drop_your_favicon_here_help_text' => 'Alleen afbeeldingsbestanden zijn toegestaan ​​voor uploaden',
    'drop_your_logo_here' => 'Zet je logo hier neer of klik om te uploaden.',
    'drop_your_logo_here_help_text' => 'Alleen afbeeldingsbestanden zijn toegestaan ​​voor uploaden',
    'found_no_businesses' => 'We did not found any businesses with such name',
    'go_back' => 'Go back',
    'locations' => 'Locations',
    'manage_selected_locations_btn' => 'Beheer geselecteerde locaties',
    'or' => 'OF',
    'part_of_retail_chain' => 'Onderdeel van een winkelketen',
    'postcode' => 'Postcode',
    'reset_btn' => 'Reset',
    'select_existing_business' => 'Selecteer bestaand bedrijf',
    'select_existing_business_help_text' => 'Selecteer uw bedrijf.',
    'this_location_already_has_owner' => 'This location already has an owner',
    'title' => 'Bedrijf',
    'we_found_the_following_locations_for' => 'We hebben de volgende locaties gevonden voor',
    'website' => 'Website',
    'website_help_text' => 'Voer uw website in.',
  ),
  'step2' =>
  array (
    'city' => 'Stad',
    'contact_information' => 'Contactgegevens',
    'enter_your_city' => '',
    'enter_your_fax' => 'Faxnummer',
    'enter_your_house_number' => 'Huisnummer zonder toevoegingen',
    'enter_your_house_number_addition' => 'Huisnummer toevoeging',
    'enter_your_local_business_name' => 'Naam van uw vestiging, bijvoorbeeld Bedrijfsnaam Jan Pier & Zonen.',
    'enter_your_local_est_code' => 'vestigingsnummer of code',
    'enter_your_local_website' => 'Website van uw vestiging',
    'enter_your_phone' => 'Telefoonnummer',
    'enter_your_phone_number' => 'Telefoonnummer',
    'enter_your_postcode' => 'Graag uw postcode invoeren 0000XX',
    'enter_your_street_address' => 'voer je adres in',
    'enter_your_full_address' => 'voer je volledige adres in',
    'establishment_code' => 'Vestigingsnummer',
    'fax' => 'Fax',
    'full_address' => 'Volledig adres',
    'house_number' => 'Huisnummer',
    'house_number_addition' => 'Toevoeging',
    'local_info' => 'Lokale informatie',
    'opening_times_btn' => 'Volgende (Openingstijden)',
    'phone' => 'Telefoon',
    'street' => 'Straat',
    'title' => 'Adres- en contactgegevens',
    'we_will_never_share_your_email' => 'E-mailadres waar klanten u kunnen bereiken',
  ),
  'step3' =>
  array (
    '1st_sunday' => '1e zondag v/d maand',
    '2nd_sunday' => '2e zondag',
    '3rd_sunday' => '3e zondag',
    '4th_sunday' => '4e zondag',
    'action' => 'Tussendoor gesloten?',
    'add_department' => 'Afdeling toevoegen',
    'add_exception_date' => 'Uitzonderingsdag toevoegen',
    'add_new_location_btn' => 'Nog een vestiging toevoegen',
    'add_season_period' => 'Seizoen periode toevoegen',
    'add_time' => 'Tijd toevoegen',
    'appointment' => 'Op afspraak?',
    'cancel_btn' => 'Annuleren',
    'closed' => 'Gesloten',
    'closing_time' => 'Sluit om',
    'copy_to' => 'Koperen naar..',
    'copy_to_all_other_days' => 'Ma - Vr',
    'copy_to_all_other_holidays' => 'Andere feestdagen',
    'datepicker' =>
    array (
      'apply' => 'Toepassen',
      'april' => 'April',
      'august' => 'Augustus',
      'cancel' => 'Annuleren',
      'custom' => 'Op maat',
      'december' => 'December',
      'february' => 'Februari',
      'fridayLabel' => 'Vr',
      'from' => 'Van',
      'january' => 'Januari',
      'july' => 'Juli',
      'june' => 'Juni',
      'march' => 'Maart',
      'may' => 'Mei',
      'mondayLabel' => 'Ma',
      'november' => 'November',
      'october' => 'Oktober',
      'saturdayLabel' => 'Za',
      'september' => 'September',
      'sundayLabel' => 'Zo',
      'thursdayLabel' => 'Do',
      'to' => 'Naar',
      'tuesdayLabel' => 'Di',
      'wednesdayLabel' => 'Wo',
      'weekLabel' => 'W',
    ),
    'datepicker_december' => 'December',
    'day' => 'Dag',
    'department_name' => 'Afdelingsnaam',
    'departments' => 'Afdelingen',
    'enter_your_department_name' => 'Voer de naam in van de afdeling',
    'enter_your_season_period' => 'Voer uw seizoensperiode in',
    'every_sunday_in_month' => 'Elke zondag',
    'exceptions' => 'Uitzonderingsdagen',
    'exceptions_help_text_line1' => 'Als uw winkel tijdelijk gesloten is, of aangepaste tijden heeft voor één of meerdere dagen, laat het uw klanten dan weten!',
    'exceptions_help_text_line2' => 'Denk bijvoorbeeld aan een ‘vakantie’, ‘winkelrenovatie’, ‘privéomstandigheden’, enz....',
    'friday' => 'Vrijdag',
    'holiday' => 'Feestdagen',
    'holidays' => 'Holidays',
    'last_sunday' => 'Laatste zondag',
    'main_department' => 'Hoofdafdeling',
    'monday' => 'Maandag',
    'new_department' => 'Nieuwe afdeling',
    'new_season_period' => 'New season period',
    'no_revert' => 'U kunt dit niet terugdraaien!',
    'open' => 'Open',
    'open_24' => '24 uur open',
    'open_closed' => 'Open/Dicht',
    'open_regular' => 'Als normale dag',
    'opening_time' => 'Open vanaf',
    'opening_times_type_heading' => 'Heeft u bedrijf reguliere openingstijden (meest gekozen), of wisselen de openingstijden per afdeling of seizoen?',
    'other' => 'Iets anders',
    'overlapping_time_error' => 'Invalide openingstijden; de tijden overlappen elkaar.',
    'period' => 'Periode',
    'promotions_btn' => 'Volgende (Afronden & Promoties)',
    'reason' => 'Reden',
    'regular' => 'Reguliere openingstijden',
    'renovation' => 'Verbouwing',
    'reset_btn' => 'Resetten!',
    'reset_current_input' => 'Hiermee worden al uw huidige gegevens gereset',
    'saturday' => 'Zaterdag',
    'season_period' => 'Seizoen start & einde',
    'seasonal' => 'Seizoenen',
    'second_last_sunday' => 'Een na laatste zondag',
    'select_date_range' => 'Selecteer datum',
    'select_reason' => 'Reden (online zichtbaar!)',
    'select_season_period' => 'Please select the season period.',
    'sick_leave' => 'Afwezig i.v.m. privé omstandigheden',
    'sunday' => 'Zondag',
    'thursday' => 'Donderdag',
    'title' => 'Openingstijden',
    'tuesday' => 'Dinsdag',
    'wednesday' => 'Woensdag',
  ),
  'title' => 'Locatie wizard',
);
