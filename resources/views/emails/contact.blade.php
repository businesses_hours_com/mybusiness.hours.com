@component('mail::message')

<strong>@lang('dashboard.name')</strong>: {{ $name }} <br>
<strong>@lang('dashboard.email')</strong>: {{ $email }}<br>
<strong>@lang('dashboard.registered_email')</strong>: {{ auth()->user()->email }}<br>
<strong>@lang('dashboard.subject')</strong>: {{ $about }}<br>
<br>
<strong>@lang('dashboard.message')</strong>:<br>
{{ $text }}

@endcomponent
