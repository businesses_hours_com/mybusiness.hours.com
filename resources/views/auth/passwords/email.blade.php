@extends('auth._layouts.auth')

@section('content')
    <div class="kt-login__signin request_password_wrap">
        <div class="kt-login__head">
            <h3 class="kt-login__title">@lang('auth.request_new_password.title')</h3>
            <div class="kt-login__desc">@lang('auth.request_new_password.description')</div>
            @if ($errors->any())
                <p class="kt-login__desc my-1">
                    @foreach ($errors->all() as $error)
                        <span class="invalid-feedback d-inline-block font-weight-bold">{{ $error }}</span>
                    @endforeach
                </p>
            @endif
            @if (session('status'))
                <div class="kt-login__desc">
                    <p class="valid-feedback d-inline-block font-weight-bold">{{ session('status') }}</p>
                </div>
            @endif
        </div>
        <form class="kt-form" action="{{ route('password.email') }}" method="POST">
            @csrf
            <div class="input-group">
                <input class="form-control" type="text" placeholder="@lang('auth.email_placeholder')" name="email" id="kt_email" autocomplete="on">
            </div>
            <div class="kt-login__actions">
                <button id="kt_login_forgot_submit" class="btn btn-brand btn-elevate kt-login__btn-primary">@lang('auth.request_new_password.reset_btn_text')</button>&nbsp;&nbsp;
                <a class="kt-login__btn-secondary" href="{{ route('login') }}">@lang('auth.request_new_password.back_to_login_btn_text')</a>
            </div>
        </form>
    </div>
@endsection
