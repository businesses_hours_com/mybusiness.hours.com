@extends('auth._layouts.auth')

@section('content')

    <div class="kt-login__signin request_password_wrap">
        <div class="kt-login__head">
            <h3 class="kt-login__title">@lang('auth.reset_password.title')</h3>
            <div class="kt-login__desc">@lang('auth.reset_password.description')</div>
            @if ($errors->any())
                <p class="kt-login__desc my-1">
                    @foreach ($errors->all() as $error)
                        <span class="invalid-feedback d-inline-block font-weight-bold">{{ $error }}</span>
                    @endforeach
                </p>
            @endif
        </div>

        <form method="POST" action="{{ route('password.update') }}">
            @csrf

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">@lang('auth.email_placeholder')</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $email) }}" required autofocus readonly>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">@lang('auth.password_placeholder')</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">@lang('auth.reset_password.confirm_password')</label>
                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required>

                    @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        @lang('auth.reset_password.reset_password_btn_text')
                    </button>
                </div>
            </div>
        </form>
    </div>

@endsection
