@extends('auth._layouts.auth')

@section('content')
    <div class="kt-login__signin login_wrap">

        <div class="kt-login__head">
            <h3 class="kt-login__title">@lang('auth.login.title')</h3>
            <p class="kt-login__desc">@lang('auth.login.description')</p>
            @if ($errors->any())
                <p class="kt-login__desc my-1">
                    @foreach ($errors->all() as $error)
                        <span class="invalid-feedback d-inline-block font-weight-bold">{{ $error }}</span>
                    @endforeach
                </p>
            @endif
        </div>

        <form class="kt-form" method="POST" action="{{ route('login') }}">
            @csrf
            <div class="input-group">
                <input class="form-control" type="text" placeholder="@lang('auth.email_placeholder')" name="email" required value="{{ old('email') }}">
            </div>
            <div class="input-group">
                <input class="form-control" type="password" placeholder="@lang('auth.password_placeholder')" name="password" required>
            </div>
            <div class="row kt-login__extra">
                <div class="col">
                    <label class="kt-checkbox">
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> @lang('auth.login.remember_me')
                        <span></span>
                    </label>
                </div>
                <div class="col kt-align-right">
                    <a href="{{ route('password.request') }}" id="kt_login_forgot" class="kt-login__link">@lang('auth.forgot_password')</a>
                </div>
            </div>
            <div class="kt-login__actions">
                <button id="kt_login_signin_submit" class="btn btn-brand btn-elevate kt-login__btn-primary">@lang('auth.login.login_btn_text')</button>
            </div>
            <div class="kt-login__account">
                <span class="kt-login__account-msg">
                    @lang('auth.no_account_yet')
                </span>
                &nbsp;&nbsp;
                <a href="{{ route('register') }}" id="kt_login_signup" class="kt-login__account-link">@lang('auth.register_btn_text')</a>
            </div>
        </form>

    </div>
@endsection
