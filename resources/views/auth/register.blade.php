@extends('auth._layouts.auth')

@section('content')
    <div class="kt-login__signin register_wrap">
        <div class="kt-login__head">
            <h3 class="kt-login__title">@lang('auth.register.title')</h3>
            <p class="kt-login__desc">@lang('auth.register.description')</p>
            @if ($errors->any())
                <p class="kt-login__desc my-1">
                    @foreach ($errors->all() as $error)
                        <span class="invalid-feedback d-inline-block font-weight-bold">{{ $error }}</span>
                    @endforeach
                </p>
            @endif
        </div>

        <form class="kt-form" method="POST" action="{{ route('register') }}">
            @csrf

            <input type="hidden" name="verify" value="{{ request()->input('verify') === 'false' ? 0 : 1 }}">
            <input type="hidden" name="country" value="{{ request()->input('country', config('app.country')) }}">

            <div class="input-group">
                <input title="first_name" class="form-control col mr-2" type="text" placeholder="@lang('auth.register.first_name_placeholder')" name="first_name" required value="{{ old('first_name') }}">
                <input title="last_name" class="form-control col" type="text" placeholder="@lang('auth.register.last_name_placeholder')" name="last_name" required value="{{ old('last_name') }}">
            </div>
            <div class="input-group">
                <input title="email" class="form-control" type="email" placeholder="@lang('auth.email_placeholder')" name="email" autocomplete="off" required value="{{ old('email') }}">
            </div>
            <div class="input-group">
                <input title="company" class="form-control" type="text" placeholder="@lang('auth.register.company_placeholder')" name="company" required value="{{ old('company') }}">
            </div>
            <div class="input-group">
                <select class="form-control" name="retailer_type" title="Account type" required>
                    <option {{ !old('retailer_type') ? 'selected' : '' }} disabled value="account_type" class="px-2">
                        @lang('auth.register.account_type')
                    </option>
                    <option value="business_owner" {{ old('retailer_type') === 'business_owner' ? 'selected' : '' }}>
                        @lang('auth.register.business_owner')</option>
                    <option value="business_employee" {{ old('retailer_type') === 'business_employee' ? 'selected' : '' }}>
                        @lang('auth.register.business_employee')
                    </option>
                    <option value="marketing_company" {{ old('retailer_type') === 'marketing_company' ? 'selected' : '' }}>
                        @lang('auth.register.marketing_company')
                    </option>
                </select>
            </div>
            <div class="input-group">
                <input title="password" class="form-control" type="password" placeholder="@lang('auth.password_placeholder')" name="password" required>
            </div>
            <div class="row kt-login__extra">
                <div class="col kt-align-left">
                    <label class="kt-checkbox">
                        <input type="checkbox" name="terms" {{ old('terms') ? 'checked' : '' }} required>
                        @lang('auth.register.agree_with_the') <strong data-toggle="modal" data-target="#terms-modal" class="kt-link kt-login__link kt-font-bold">@lang('auth.register.terms_and_conditions')</strong>.
                        <span></span>
                    </label>
                    <span class="form-text text-muted"></span>
                </div>
            </div>
            <div class="kt-login__actions">
                <button id="kt_login_signup_submit" class="btn btn-brand btn-elevate kt-login__btn-primary">@lang('auth.register_btn_text')</button>
            </div>

            <div class="row kt-login__extra">
                <a href="{{ route('login') }}" class="kt-login__link">@lang('auth.already_have_account')</a>
            </div>
            <div class="row kt-login__extra">
                <a href="{{ route('password.request') }}" class="kt-login__link">@lang('auth.forgot_password')</a>
            </div>
        </form>

    </div>

    @include('auth._includes.terms-modal')

@endsection
