@extends('auth._layouts.auth')

@section('content')

    <div class="kt-login__signin verification_wrap">
        <div class="kt-login__head">
            <h3 class="kt-login__title">@lang('auth.verify_email.title')</h3>
            <p class="kt-login__desc">
                @lang('auth.verify_email.description')
            </p>
        </div>

        @if (session('resent'))
            <div class="alert alert-success" role="alert">
                @lang('auth.verify_email.verification_email_has_been_sent')
            </div>
        @endif

        <form class="kt-form" method="POST" action="{{ route('verification.resend') }}">
            @csrf
            <label title="kt_login_verification_submit">
                @lang('auth.verify_email.not_received_yet')
            </label>
            <button id="kt_login_verification_submit" class="btn btn-brand btn-elevate kt-login__btn-primary d-block mx-auto">
                @lang('auth.verify_email.request_another_email_btn_text')
            </button>
        </form>

    </div>
@endsection
