@php use App\Models\Country; @endphp
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

@push('css')
    <link href="{{ asset('theme/dist/assets/css/pages/login/login-3.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ mix('css/auth.css') }}" rel="stylesheet" type="text/css"/>
@endpush

@include('admin._includes.head')

<body class="body-auth kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url({{ asset('theme/dist/assets/media/bg/bg-3.jpg') }});">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__logo" style="max-width:200px">
                        <img class="w-100" src="{{ Country::getCountryLogo() }}" alt="{{ config('app.name') }}">
                    </div>

                    @yield('content')

                </div>
            </div>
        </div>
    </div>
</div>

@include('admin._includes.scripts')

</body>
</html>
