<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

@include('admin._includes.head')

<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed">

@include('flash::message')

@include('admin._includes.header-mobile')

<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <!-- Uncomment this to display the close button of the panel
        <button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
        -->

        @include('admin._includes.aside')

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            @include('admin._includes.header')

            <div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                    <div class="kt-container kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">@yield('title')</h3>
                            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                            @include('admin._includes.breadcrumbs')
                        </div>
                    </div>
                </div>

                <div class="kt-container container-content kt-container--fluid kt-grid__item kt-grid__item--fluid">
                    @yield('content')
                </div>

            </div>

{{--            @include('admin._includes.footer')--}}

        </div>
    </div>
</div>

@include('admin._includes.scripts')

</body>
</html>
