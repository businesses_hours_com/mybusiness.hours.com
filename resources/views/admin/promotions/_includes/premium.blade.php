<div class="kt-pricing-4__top-item">
    <span class="kt-pricing-4__icon kt-font-info">
        <i class="fa flaticon-confetti"></i>
    </span>

    <h2 class="kt-pricing-4__subtitle font-weight-bold">@lang('promotions.premium_plan') <br> <span>@lang('promotions.popular_choice')</span></h2>
    <div class="kt-pricing-4__features">
        <span>@lang('promotions.premium_plan_description_line_1')</span><br>
        <span>@lang('promotions.premium_plan_description_line_2')</span><br>
        <span>@lang('promotions.premium_plan_description_line_3')</span>
    </div>

    <div class="kt-pricing-4__top-items-mobile">
        <div class="kt-pricing-4__top-item-mobile">
            <span>@lang('promotions.feature_1')</span>
            <span>@lang('promotions.yes')</span>
        </div>
        <div class="kt-pricing-4__top-item-mobile">
            <span>@lang('promotions.feature_2')</span>
            <span>@lang('promotions.yes')</span>
        </div>
        <div class="kt-pricing-4__top-item-mobile">
            <span>@lang('promotions.feature_3')</span>
            <span>@lang('promotions.yes')</span>
        </div>
        <div class="kt-pricing-4__top-item-mobile">
            <span>@lang('promotions.feature_4')</span>
            <span>@lang('promotions.yes')</span>
        </div>
        <div class="kt-pricing-4__top-item-mobile">
            <span>@lang('promotions.feature_5')</span>
            <span>@lang('promotions.yes')</span>
        </div>
        <div class="kt-pricing-4__top-item-mobile">
            <span>@lang('promotions.feature_6')</span>
            <span>@lang('promotions.yes')</span>
        </div>
{{--        <div class="kt-pricing-4__top-item-mobile">--}}
{{--            <span>@lang('promotions.feature_7')</span>--}}
{{--            <span>@lang('promotions.yes')</span>--}}
{{--        </div>--}}
{{--        <div class="kt-pricing-4__top-item-mobile">--}}
{{--            <span>@lang('promotions.feature_8')</span>--}}
{{--            <span>@lang('promotions.yes')</span>--}}
{{--        </div>--}}
{{--        <div class="kt-pricing-4__top-item-mobile">--}}
{{--            <span>@lang('promotions.feature_9')</span>--}}
{{--            <span>@lang('promotions.yes')</span>--}}
{{--        </div>--}}
{{--        <div class="kt-pricing-4__top-item-mobile">--}}
{{--            <span>@lang('promotions.feature_10')</span>--}}
{{--            <span>@lang('promotions.yes')</span>--}}
{{--        </div>--}}
        <div class="kt-pricing-4__top-item-mobile">
            <span>@lang('promotions.monthly_price_per_location_text')</span>
            <span>€{{ $monthlyPrice }} (@lang('promotions.invoiced_as') €{{ $yearlyPrice }}/@lang('promotions.year'))</span>
        </div>
        <div class="kt-pricing-4__top-item-mobile">
            <span>@lang('promotions.total') ({{ $numOfLocations . ' ' .($numOfLocations === 1 ? __('dashboard.location') : __('dashboard.locations')) }})</span>
            <span>€{{ $numOfLocations * $monthlyPrice }} (@lang('promotions.invoiced_as') €{{ $numOfLocations * $yearlyPrice }}/@lang('promotions.year'))</span>
        </div>
        <div class="kt-pricing-4__top-btn">
            <button type="submit" class="btn btn-brand btn-upper btn-bold">
                @lang('promotions.subscribe_btn_text')
            </button>
        </div>
    </div>
</div>
