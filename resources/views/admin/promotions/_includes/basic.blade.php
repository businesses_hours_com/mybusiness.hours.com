<div class="kt-pricing-4__top-item">
    <span class="kt-pricing-4__icon kt-font-info">
        <i class="fa flaticon-rocket"></i>
    </span>

    <h2 class="kt-pricing-4__subtitle">@lang('promotions.basic_plan')</h2>

    <div class="kt-pricing-4__features">
        <span>@lang('promotions.basic_plan_description_line_1')</span><br>
        <span>@lang('promotions.basic_plan_description_line_2')</span>
    </div>

    <div class="kt-pricing-4__top-items-mobile">
        <div class="kt-pricing-4__top-item-mobile">
            <span>@lang('promotions.feature_1')</span>
            <span>@lang('promotions.yes')</span>
        </div>
        <div class="kt-pricing-4__top-item-mobile">
            <span>@lang('promotions.feature_2')</span>
            <span>@lang('promotions.yes')</span>
        </div>
        <div class="kt-pricing-4__top-item-mobile">
            <span>@lang('promotions.feature_3')</span>
            <span>@lang('promotions.yes')</span>
        </div>
        <div class="kt-pricing-4__top-item-mobile">
            <span>@lang('promotions.feature_4')</span>
            <span>@lang('promotions.yes')</span>
        </div>
        <div class="kt-pricing-4__top-item-mobile">
            <span>@lang('promotions.feature_5')</span>
            <span>@lang('promotions.no')</span>
        </div>
        <div class="kt-pricing-4__top-item-mobile">
            <span>@lang('promotions.feature_6')</span>
            <span>@lang('promotions.no')</span>
        </div>
{{--        <div class="kt-pricing-4__top-item-mobile">--}}
{{--            <span>@lang('promotions.feature_7')</span>--}}
{{--            <span>@lang('promotions.no')</span>--}}
{{--        </div>--}}
{{--        <div class="kt-pricing-4__top-item-mobile">--}}
{{--            <span>@lang('promotions.feature_8')</span>--}}
{{--            <span>@lang('promotions.no')</span>--}}
{{--        </div>--}}
{{--        <div class="kt-pricing-4__top-item-mobile">--}}
{{--            <span>@lang('promotions.feature_9')</span>--}}
{{--            <span>@lang('promotions.no')</span>--}}
{{--        </div>--}}
{{--        <div class="kt-pricing-4__top-item-mobile">--}}
{{--            <span>@lang('promotions.feature_10')</span>--}}
{{--            <span>@lang('promotions.no')</span>--}}
{{--        </div>--}}
        <div class="kt-pricing-4__top-item-mobile">
            <span>@lang('promotions.monthly_price_per_location_text')</span>
            <span>@lang('promotions.free') @lang('promotions.basic_pricing_description_text')</span>
        </div>
        <div class="kt-pricing-4__top-btn">
            <a href="{{ route('admin.locations.index') }}" class="btn btn-brand btn-upper btn-bold">
                @lang('promotions.finish_btn_text')
            </a>
        </div>
    </div>
</div>
