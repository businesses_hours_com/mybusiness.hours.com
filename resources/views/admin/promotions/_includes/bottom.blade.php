<div class="kt-pricing-4__bottom">
    <div class="kt-pricing-4__bottok-container kt-pricing-4__bottok-container--fixed">
        <div class="kt-pricing-4__bottom-items">
            <div class="kt-pricing-4__bottom-item">@lang('promotions.feature_1')</div>
            <div class="kt-pricing-4__bottom-item"><span class="text-success">&#10003;</span></div>
            <div class="kt-pricing-4__bottom-item"><span class="text-success">&#10003;</span></div>
        </div>
        <div class="kt-pricing-4__bottom-items">
            <div class="kt-pricing-4__bottom-item">@lang('promotions.feature_2')</div>
            <div class="kt-pricing-4__bottom-item"><span class="text-success">&#10003;</span></div>
            <div class="kt-pricing-4__bottom-item"><span class="text-success">&#10003;</span></div>
        </div>
        <div class="kt-pricing-4__bottom-items">
            <div class="kt-pricing-4__bottom-item">@lang('promotions.feature_3')</div>
            <div class="kt-pricing-4__bottom-item"><span class="text-success">&#10003;</span></div>
            <div class="kt-pricing-4__bottom-item"><span class="text-success">&#10003;</span></div>
        </div>
        <div class="kt-pricing-4__bottom-items">
            <div class="kt-pricing-4__bottom-item">@lang('promotions.feature_4')</div>
            <div class="kt-pricing-4__bottom-item"><span class="text-success">&#10003;</span></div>
            <div class="kt-pricing-4__bottom-item"><span class="text-success">&#10003;</span></div>
        </div>
        <div class="kt-pricing-4__bottom-items">
            <div class="kt-pricing-4__bottom-item">@lang('promotions.feature_5')</div>
            <div class="kt-pricing-4__bottom-item"><span class="text-success">&#10003;</span></div>
            <div class="kt-pricing-4__bottom-item"><span class="text-danger">&#10006;</span></div>
        </div>
        <div class="kt-pricing-4__bottom-items">
            <div class="kt-pricing-4__bottom-item">@lang('promotions.feature_6')</div>
            <div class="kt-pricing-4__bottom-item"><span class="text-success">&#10003;</span></div>
            <div class="kt-pricing-4__bottom-item"><span class="text-danger">&#10006;</span></div>
        </div>
        {{--        <div class="kt-pricing-4__bottom-items">--}}
        {{--            <div class="kt-pricing-4__bottom-item">@lang('promotions.feature_7')</div>--}}
        {{--            <div class="kt-pricing-4__bottom-item"><span class="text-success">&#10003;</span></div>--}}
        {{--            <div class="kt-pricing-4__bottom-item"><span class="text-danger">&#10006;</span></div>--}}
        {{--        </div>--}}
        {{--        <div class="kt-pricing-4__bottom-items">--}}
        {{--            <div class="kt-pricing-4__bottom-item">@lang('promotions.feature_8')</div>--}}
        {{--            <div class="kt-pricing-4__bottom-item"><span class="text-success">&#10003;</span></div>--}}
        {{--            <div class="kt-pricing-4__bottom-item"><span class="text-danger">&#10006;</span></div>--}}
        {{--        </div>--}}
        {{--        <div class="kt-pricing-4__bottom-items">--}}
        {{--            <div class="kt-pricing-4__bottom-item">@lang('promotions.feature_9')</div>--}}
        {{--            <div class="kt-pricing-4__bottom-item"><span class="text-success">&#10003;</span></div>--}}
        {{--            <div class="kt-pricing-4__bottom-item"><span class="text-danger">&#10006;</span></div>--}}
        {{--        </div>--}}
        {{--        <div class="kt-pricing-4__bottom-items">--}}
        {{--            <div class="kt-pricing-4__bottom-item">@lang('promotions.feature_10')</div>--}}
        {{--            <div class="kt-pricing-4__bottom-item"><span class="text-success">&#10003;</span></div>--}}
        {{--            <div class="kt-pricing-4__bottom-item"><span class="text-danger">&#10006;</span></div>--}}
        {{--        </div>--}}
        <div class="kt-pricing-4__bottom-items">
            <div class="kt-pricing-4__bottom-item">@lang('promotions.monthly_price_per_location_text')</div>
            <div class="kt-pricing-4__bottom-item text-success price-cell">
                <span class="price">€{{ $monthlyPrice }}</span><br>
                <span class="explanation">(@lang('promotions.invoiced_as') €{{ $yearlyPrice }}/@lang('promotions.year'))</span>
            </div>
            <div class="kt-pricing-4__bottom-item text-success price-cell">
                <span class="price">@lang('promotions.free')</span><br>
                <span class="explanation">@lang('promotions.basic_pricing_description_text')</span>
            </div>
        </div>

        <div class="kt-pricing-4__bottom-items">
            <div class="kt-pricing-4__bottom-item">
                @lang('promotions.total') ({{ $numOfLocations . ' ' .($numOfLocations === 1 ? __('dashboard.location') : __('dashboard.locations')) }})
            </div>
            <div class="kt-pricing-4__bottom-item text-success price-cell">
                <span class="price">€{{ $numOfLocations * $monthlyPrice }}</span><br>
                <span class="explanation">(@lang('promotions.invoiced_as') €{{ $numOfLocations * $yearlyPrice }}/@lang('promotions.year'))</span>
            </div>
            <div class="kt-pricing-4__bottom-item text-success price-cell">
                <span class="price">@lang('promotions.free')</span><br>
                <span class="explanation">@lang('promotions.basic_pricing_description_text')</span>
            </div>
        </div>

        <div class="kt-pricing-4__bottom-items">
            <div class="kt-pricing-4__bottom-item"></div>
            <div class="kt-pricing-4__bottom-item">
                <button type="submit" class="btn btn-brand btn-upper btn-bold w-50" {{ $numOfLocations === 0 ? 'disabled="disabled"' : '' }}>
                    @lang('promotions.subscribe_btn_text')
                </button>
            </div>
            <div class="kt-pricing-4__bottom-item">
                <a href="{{ route('admin.locations.index') }}" class="btn btn-brand btn-upper btn-bold w-50">
                    @lang('promotions.finish_btn_text')
                </a>
            </div>
        </div>

        <hr>

        <div class="row mt-4">
            <div class="col-3">
                <div class="form-group">
                    <label>@lang('promotions.discount_code')</label>
                    <input title="Coupon" type="text" class="form-control" name="coupon" placeholder="">
                    <span class="coupon-message form-text"></span>
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label>&nbsp;</label>
                    <button type="button" class="validateCoupon btn btn-success btn-sm btn-wide kt-font-bold kt-font-transform-u d-block">
                        @lang('promotions.add_btn_text')
                    </button>
                </div>
            </div>
        </div>

    </div>
</div>

@push('scripts')
    <script>
        $(document).ready(function(){

            let couponInput   = $('[name="coupon"]');
            let couponMessage = $('.coupon-message');

            $('.validateCoupon').click(function(e){
                e.preventDefault();
                couponMessage.html('').removeClass('text-success text-danger');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin.validate-coupon') }}",
                    data: {
                        coupon: couponInput.val()
                    },
                    success: function(result){
                        if (result.hasOwnProperty('valid')) {
                            let message  = result.valid ? 'Successfully applied coupon code.' : 'Invalid coupon code.';
                            let cssClass = result.valid ? 'text-success' : 'text-danger';
                            couponMessage.text(message).addClass(cssClass);
                            if (!result.valid) {
                                couponInput.val('');
                            }
                        }
                        else {
                            console.log(result);
                        }
                    },
                    error: function(error){
                        console.log(error);
                        couponMessage.text('Invalid coupon code.');
                        couponInput.val('');
                    }
                });
            });
        });
    </script>
@endpush
