@extends('admin._layouts.admin')

@section('title', 'Promotions')

@push('css')
    <link href="{{ asset('theme/dist/assets/css/pages/pricing/pricing-4.css') }}" rel="stylesheet" type="text/css"/>
@endpush

@section('breadcrumbs')
    <a href="" class="kt-subheader__breadcrumbs-link">@lang('promotions.title')</a>
@endsection

@section('content')
    <div class="kt-portlet">
        <div class="kt-portlet__body kt-portlet__body--fit">

            <form class="kt-pricing-4" method="POST" action="{{ route('admin.subscribe') }}">
                @csrf
                <div class="kt-pricing-4__top">
                    <div class="kt-pricing-4__top-container kt-pricing-4__top-container--fixed">

                        <div class="kt-pricing-4__top-header">
                            <div class="kt-pricing-4__top-title kt-font-light">
                                <h1 class="pl-4">@lang('promotions.main_heading')</h1>
                            </div>
                        </div>

                        <div class="kt-pricing-4__top-body">

                            <div class="kt-pricing-4__top-items">
                                @include('admin.promotions._includes.premium')
                                @include('admin.promotions._includes.basic')
                            </div>

                        </div>
                    </div>
                </div>

                @include('admin.promotions._includes.bottom')

            </form>

        </div>
    </div>
@endsection
