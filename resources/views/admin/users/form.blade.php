@extends('admin._layouts.admin')

@section('title', __('dashboard.my_profile'))

@section('breadcrumbs')
    @lang('dashboard.my_profile')
@endsection

@section('content')

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    @lang('dashboard.my_profile')
                </h3>
            </div>
        </div>

        <form class="kt-form" method="POST" action="{{ route('admin.users.update', $item->id) }}">
            @method('PUT')
            @csrf

            <div class="kt-portlet__body user-edit-form px-0">

{{--                <div class="form-group row px-4">--}}
{{--                    <label for="profile-retailer-type" class="col-form-label col-3 text-lg-right text-left">--}}
{{--                        @lang('auth.register.account_type')--}}
{{--                    </label>--}}
{{--                    <div class="col-9">--}}
{{--                        <input disabled id="profile-retailer-type" class="form-control form-control-solid" type="text" value="{{ ucfirst(str_replace(['_', '-'], ' ', $item->retailer_type ?? 'Admin')) }}">--}}
{{--                    </div>--}}
{{--                </div>--}}

                <div class="form-group row px-4">
                    <label for="profile-firstname" class="col-form-label col-3 text-lg-right text-left">
                        @lang('auth.register.first_name_placeholder')
                    </label>
                    <div class="col-9">
                        <input id="profile-firstname" class="form-control form-control-solid @error('name') is-invalid @enderror" type="text" name="name" value="{{ $item->name }}" required>
                        @error('name')
                        <div class="error invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row px-4">
                    <label for="profile-lastname" class="col-form-label col-3 text-lg-right text-left">
                        @lang('auth.register.last_name_placeholder')
                    </label>
                    <div class="col-9">
                        <input id="profile-lastname" class="form-control form-control-solid @error('last_name') is-invalid @enderror" type="text" name="last_name" value="{{ $item->last_name }}"
                               required>
                        @error('last_name')
                        <div class="error invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row px-4">
                    <label for="profile-company" class="col-form-label col-3 text-lg-right text-left">
                        @lang('auth.register.company_placeholder')
                    </label>
                    <div class="col-9">
                        <input id="profile-company" class="form-control form-control-solid @error('company') is-invalid @enderror" type="text" name="company" value="{{ $item->company }}">
                        @error('company')
                        <div class="error invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row px-4">
                    <label for="profile-email" class="col-form-label col-3 text-lg-right text-left">
                        @lang('auth.email_placeholder')
                    </label>
                    <div class="col-9">
                        <div class="input-group input-group-solid">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="la la-at"></i></span>
                            </div>
                            <input id="profile-email" title="email" type="email" class="form-control form-control-solid @error('email') is-invalid @enderror" value="{{ $item->email }}"
                                   placeholder="Email" name="email" required>
                            @error('email')
                            <div class="error invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="form-group row px-4">
                    <label for="profile-email" class="col-form-label col-3 text-lg-right text-left">
                        @lang('auth.register.account_type')
                    </label>
                    <div class="col-9">
                        <select class="form-control @error('retailer_type') is-invalid @enderror" name="retailer_type" title="Account type" required type="text">
                            <option {{ !old('retailer_type') ? 'selected' : '' }} disabled value="account_type" class="px-2">
                                @lang('auth.register.account_type')
                            </option>
                            <option value="business_owner" {{ $item->retailer_type === 'business_owner' ? 'selected' : '' }}>
                                @lang('auth.register.business_owner')</option>
                            <option value="business_employee" {{  $item->retailer_type === 'business_employee' ? 'selected' : '' }}>
                                @lang('auth.register.business_employee')
                            </option>
                            <option value="marketing_company" {{  $item->retailer_type === 'marketing_company' ? 'selected' : '' }}>
                                @lang('auth.register.marketing_company')
                            </option>
                        </select>
                        @error('retailer_type')
                            <div class="error invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="kt-portlet__head mb-5">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            @lang('auth.reset_password.title')
                        </h3>
                    </div>
                </div>

                <div class="form-group row px-4">
                    <label for="current_password" class="col-form-label col-3 text-lg-right text-left">
                        @lang('auth.current_password')
                    </label>
                    <div class="col-9">
                        <input id="current_password" class="form-control form-control-solid mb-1 @error('current_password') is-invalid @enderror" type="password" name="current_password">
                        @error('current_password')
                        <div class="error invalid-feedback">{{ $message }}</div>
                        @enderror
                        <a href="{{ route('password.request') }}" class="font-weight font-size-sm">@lang('auth.forgot_password')</a>
                    </div>
                </div>

                <div class="form-group row px-4">
                    <label for="new_password" class="col-form-label col-3 text-lg-right text-left">@lang('auth.new_password')</label>
                    <div class="col-9">
                        <input id="new_password" class="form-control form-control-solid @error('new_password') is-invalid @enderror" type="password" name="new_password">
                        @error('new_password')
                        <div class="error invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row px-4">
                    <label for="verify_password" class="col-form-label col-3 text-lg-right text-left">@lang('auth.verify_password')</label>
                    <div class="col-9">
                        <input id="verify_password" class="form-control form-control-solid @error('verify_password') is-invalid @enderror" type="password" name="verify_password">
                        @error('verify_password')
                        <div class="error invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <button class="btn btn-brand d-inline-block px-5 mx-auto mt-4" type="submit">@lang('translation::translation.save')</button>

            </div>
        </form>
    </div>
@endsection
