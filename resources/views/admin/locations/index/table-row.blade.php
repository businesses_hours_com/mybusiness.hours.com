<tr>
    <td class="pl-0 py-7">
        <label class="checkbox checkbox-lg checkbox-inline {{ auth()->user()->getSubscriptionForLocation($location->hash) ? 'checkbox-disabled ' : '' }}">
            <input type="checkbox" value="1" class="select-location" data-hash="{{ $location->hash }}" {{ auth()->user()->getSubscriptionForLocation($location->hash) ? 'disabled="disabled"' : '' }}">
            <span></span>
        </label>
    </td>
    <td class="pl-0">
        <span class="text-dark-75 mb-1 font-size-lg">{{ optional($location->address)->full_address }}</span>
    </td>
    <td>
        <span class="text-dark-75 d-block font-size-lg">{{ optional(\Carbon\Carbon::parse($location->updated_at))->format('d/m/Y') }}</span>
    </td>
    @if($location->source !== \App\Models\Location::SOURCE_YEXT)
        <td class="text-center">
            <a href="{{ route('admin.locations.hours', $location->hash) }}" class="text-dark-75 text-hover-primary font-size-lg d-inline-block" target="_blank">
            <span class="btn btn-icon btn-light btn-hover-primary btn-sm">
                <span class="svg-icon svg-icon-primary svg-icon-2x">
                    @include('admin._includes.svg.hours')
                </span>
            </span>
            </a>
        </td>
        <td class="text-center">
            <a href="{{ route('admin.locations.address', $location->hash) }}" class="text-dark-75 text-hover-primary font-size-lg d-inline-block" target="_blank">
            <span class="btn btn-icon btn-light btn-hover-primary btn-sm">
                <span class="svg-icon svg-icon-primary svg-icon-2x">
                    @include('admin._includes.svg.address')
                </span>
            </span>
            </a>
        </td>
        <td class="text-center">
            <a href="{{ route('admin.locations.images', $location->hash) }}" class="text-dark-75 text-hover-primary font-size-lg d-inline-block" target="_blank">
            <span class="btn btn-icon btn-light btn-hover-primary btn-sm">
                <span class="svg-icon svg-icon-primary svg-icon-2x">
                    @include('admin._includes.svg.images')
                </span>
            </span>
            </a>
        </td>
        @unless(auth()->user()->canAccessBusiness($location->business_id) && count($locations) === 1)
            <td class="text-center">
                <a href="{{ route('admin.locations.texts', $location->hash) }}" class="text-dark-75 text-hover-primary font-size-lg d-inline-block" target="_blank">
            <span class="btn btn-icon btn-light btn-hover-primary btn-sm">
                <span class="svg-icon svg-icon-primary svg-icon-2x">
                    @include('admin._includes.svg.texts')
                </span>
            </span>
                </a>
            </td>
        @endunless
        <td class="text-center">
            <a href="{{ route('admin.locations.destroy', $location->hash) }}" class="ajax-delete text-dark-75 text-hover-primary font-size-lg d-inline-block"
               data-modal-title="{{ __('dashboard.delete_modal_title') }}"
               data-modal-cancel="{{ __('wizard.step3.cancel_btn') }}"
               data-modal-yes="{{ __('dashboard.yes_btn') }}"
            >
            <span class="btn btn-icon btn-light btn-hover-primary btn-sm">
                <span class="svg-icon svg-icon-primary svg-icon-2x">
                    @include('admin._includes.svg.trash')
                </span>
            </span>
            </a>
        </td>
    @else
        <td colspan="5" style="max-width:350px">
            <i class="mr-2 fa fa-info-circle" style="color: #3699ff;" aria-hidden="true"></i>@lang('dashboard.listings.listing_managed_by_yext_cannot_be_updated_here', [
            'yext_link' => '<a href="'.config('services.yext.login_url.'.config('app.locale')).'" target="_blank">Yext</a>'
            ]
        )
        </td>
    @endif
    <td class="text-center">
        @if(!auth()->user()->getSubscriptionForLocation($location->hash))
            <a href="{{ route('admin.promotions', ['hash' => $location->hash]) }}" class="text-dark-75 text-hover-primary font-size-lg d-inline-block" target="_blank">
            <span class="btn btn-icon btn-light btn-hover-primary btn-sm">
                <i class="fas fa-lg fa-sort-amount-up-alt" style="color: #3699ff;"></i>
            </span>
            </a>
        @else
            <span class="btn btn-icon btn-sm mt-1" style="cursor:default;">
                <i class="fas fa-medal fa-lg" style="color:gold;"></i>
            </span>
        @endif
    </td>
</tr>
