<div class="card-header border-0 py-5">
    <h3 class="card-title align-items-start flex-column">
        <span class="card-label font-weight-bolder text-dark">{{ $businessName }}</span>
        <span class="text-muted mt-3 font-weight-bold font-size-sm">
            @lang('dashboard.listings.here_you_can_manage_and_update_your_business_listings', ['business_name' => $businessName])
        </span>
    </h3>
    <div class="card-toolbar">
        @if(Auth::user()->canAccessBusiness($locations[0]->business_id))
            <a href="{{ route('admin.businesses.edit', ['business' => $locations[0]->business_id]) }}" class="btn btn-success font-weight-bolder font-size-sm">
                <span class="svg-icon svg-icon-md svg-icon-white">@include('admin._includes.svg.edit-business')</span>@lang('dashboard.edit_business')
            </a>
        @endif
        <a href="{{ route('admin.location-wizard.add-location',[
                        'business_id' => $locations[0]->business_id,
                        'business_name' => $businessName,
                    ]) }}"
           class="btn btn-success font-weight-bolder font-size-sm mx-3">
            <span class="svg-icon svg-icon-md svg-icon-white">@include('admin._includes.svg.add-listing')</span>@lang('dashboard.listings.add_listing')
        </a>
    </div>
</div>
