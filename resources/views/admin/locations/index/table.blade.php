<table class="table shadow-none table-head-custom table-vertical-center" id="kt_advance_table_widget_3">
    <thead>
    <tr class="text-left text-uppercase">
        <th class="pl-0" style="width: 20px">
            <label class="checkbox checkbox-lg checkbox-inline mr-2">
                <input type="checkbox" class="select-all-locations" value="1">
                <span></span>
            </label>
        </th>
        <th style="width:50%">@lang('dashboard.address')</th>
        <th>@lang('dashboard.edited')</th>
        <th style="width: 70px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@lang('dashboard.hours')&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
        <th style="width: 70px;">@lang('dashboard.address')</th>
        <th style="width: 70px;">@lang('dashboard.images')</th>

        @unless(auth()->user()->canAccessBusiness($locations[0]->business_id) && count($locations) === 1)
            <th style="width: 70px;">@lang('dashboard.texts')</th>
        @endunless

        <th style="width: 70px;">@lang('dashboard.delete')</th>
        <th style="width: 70px;">@lang('dashboard.promote')</th>
    </tr>
    </thead>
    <tbody>
    @foreach($locations as $location)
        @include('admin.locations.index.table-row', ['location' => $location])
    @endforeach
    </tbody>
</table>
