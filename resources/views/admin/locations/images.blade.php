@extends('admin._layouts.admin')

@section('title', optional($item->address)->full_address)

@section('breadcrumbs')
    <a href="{{ route('admin.locations.index') }}" class="kt-subheader__breadcrumbs-link">{{ __('dashboard.listings') }}</a>
    <span class="kt-subheader__breadcrumbs-separator"></span>
    {{ optional($item->address)->full_address }}
@endsection

@section('content')
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    {{ __('dashboard.images.add_images') }}
                </h3>
            </div>
        </div>
        <form class="kt-form images-form" method="POST" action="{{ route('admin.locations.update', $item->hash) }}">
            @method('PUT')
            @csrf
            <div class="d-flex flex-column">
                @if($item->promoted_at === null && $images)
                    <div class="alert alert-custom alert-outline-success fade show mb-0 mx-auto w-100" style="max-width:750px;" role="alert">
                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                        <div class="alert-text">{{ __('dashboard.images.warning_text') }}</div>
                        <div class="alert-close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="ki ki-close"></i></span>
                            </button>
                        </div>
                    </div>
                @endif
                <section id="uppy" class="mx-auto w-100" data-route="{{ route('admin.locations.images', $item->hash) }}"></section>
            </div>

            @if($images)
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ __('dashboard.images') }}
                        </h3>
                    </div>
                </div>
            @endif

            <div class="kt-portlet__body location-images">
                <section class="preview d-flex flex-wrap mt-3 align-items-start">
                    @foreach($images+$images+$images+$images+$images as $image)
                        <div class="position-relative shadow mx-3 mb-2">
                            <img style="max-width:400px;" class="h-auto" src="{{ $image->ready_url }}" alt="{{ $image->fileable_slug }}">
                            <a href="{{ route('admin.locations.destroy-image', ['hash'=> $item->hash, 'file_id' => $image->id]) }}"
                               class="ajax-delete text-white position-absolute px-2 py-1"
                               style="background: #BD362F; top:0; right:0;"
                               data-modal-title="{{ __('dashboard.delete_modal_title') }}"
                               data-modal-cancel="{{ __('wizard.step3.cancel_btn') }}"
                               data-modal-yes="{{ __('dashboard.yes_btn') }}"
                            >
                                @include('admin._includes.svg.trash', ['fill' => '#fff'])
                            </a>
                        </div>
                    @endforeach
                </section>

                @include('admin.locations._includes.form-buttons', ['noUpdateBtn' => true])
            </div>

        </form>
    </div>

@endsection
