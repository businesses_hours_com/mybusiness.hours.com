@extends('admin._layouts.admin')

@section('title', optional($item->address)->full_address)

@section('breadcrumbs')
    <a href="{{ route('admin.locations.index') }}" class="kt-subheader__breadcrumbs-link">@lang('dashboard.listings')</a>
    <span class="kt-subheader__breadcrumbs-separator"></span>
    {{ optional($item->address)->full_address }}
@endsection

@section('content')
    <ul class="nav nav-tabs mb-0" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" href="#address-tab" data-toggle="tab" role="tab" aria-controls="table" aria-selected="true">Address</a>
        </li>
        @if($wizard)
            <li class="nav-item">
                <a class="nav-link" href="#hours-tab" data-toggle="tab" role="tab" aria-controls="summary" aria-selected="false">Hours</a>
            </li>
        @endif
    </ul>
    <div class="tab-content">
        <div id="address-tab" class="tab-pane fade active show" role="tabpanel" aria-labelledby="address-tab">
            @include('admin.locations._includes.address-form')
        </div>
        @if($wizard)
            <div id="hours-tab" class="tab-pane fade" role="tabpanel" aria-labelledby="hours-tab">
                @include('admin.locations._includes.hours-form')
            </div>
        @endif
    </div>
@endsection
