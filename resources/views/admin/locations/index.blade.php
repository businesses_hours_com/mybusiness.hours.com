@extends('admin._layouts.admin')

@section('title', __('dashboard.listings'))

@section('breadcrumbs')
    <a href="{{ route('admin.locations.index') }}" class="kt-subheader__breadcrumbs-link">@lang('dashboard.listings')</a>
@endsection

@push('css')
    <link href="{{ asset('css/theme-v7.2.8.css') }}" rel="stylesheet" type="text/css"/>
@endpush

@section('content')

    <div class="card card-custom gutter-b">
        @foreach($items as $businessName => $locations)
            @if($locations->isNotEmpty())
                @include('admin.locations.index.header')
                <div class="card-body business-wrap py-0 pb-3">
                    <div class="table-responsive">
                        @include('admin.locations.index.table', ['locations' => $locations])
                    </div>

                    <form class="d-none promote-selected-wrap kt-pricing-4" method="POST" action="{{ route('admin.promotions') }}">
                        @csrf
                        <button class="promote-all-selected-btn font-size-lg d-inline-block btn btn-sm btn-bg-white btn-hover-bg-light"
                                type="submit"
                                style="color: #3699ff;">
                            <i class="fas fa-lg fa-sort-amount-up-alt" style="color: #3699ff;"></i>
                            {{ __('dashboard.listings.promote_selected_listings_btn') }}
                        </button>
                    </form>
                </div>
            @endif
        @endforeach
    </div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            $('input.select-all-locations').click(function(e){
                const businessWrap = $(this).parents('.business-wrap');
                businessWrap.find('input.select-location:not(:disabled)').prop('checked', $(this).prop('checked'));
                let showPromoteAllButton = $(this).prop('checked') && businessWrap.find('input.select-location:checked').length > 1;
                businessWrap.find('.promote-selected-wrap').toggleClass('d-none', !showPromoteAllButton);
            });

            $('input.select-location').click(function(e){
                const businessWrap = $(this).parents('.business-wrap');
                businessWrap.find('.promote-selected-wrap').toggleClass('d-none', $('input.select-location:checked').length <= 1);
            });

            $('.promote-all-selected-btn').click(function(e){
                e.preventDefault();
                const businessWrap = $(this).parents('.business-wrap');
                let form = $(this).parents('form');
                businessWrap.find('input.select-location:checked').each(function(){
                    form.prepend('<input type="hidden" name="hashes[]" value="' + $(this).attr('data-hash') + '">');
                });
                form.submit();
            });
        });
    </script>
@endpush
