@extends('admin._layouts.admin')

@section('title', optional($item->address)->full_address)

@section('breadcrumbs')
    <a href="{{ route('admin.locations.index') }}" class="kt-subheader__breadcrumbs-link">{{ __('dashboard.listings') }}</a>
    <span class="kt-subheader__breadcrumbs-separator"></span>
    {{ optional($item->address)->full_address }}
@endsection

@section('content')
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    {{ __('dashboard.location') }} {{ strtolower(__('dashboard.texts')) }}
                </h3>
            </div>
        </div>
        <form class="kt-form texts-form" method="POST" action="{{ route('admin.locations.update-texts', $item->hash) }}">
            @method('PUT')
            @csrf

            <div class="kt-portlet__body location-texts">

                <div class="row">
                    <div class="col-xl-6">
                        <div class="form-group">
                            <label for="promoted_description">{{ __('wizard.step1.description') }}</label>
                            <textarea maxlength="250" rows="5" id="promoted_description" class="form-control" name="texts[promoted-description]">{{ $description->content ?? '' }}</textarea>
                            <span class="form-text text-muted">{{ __('dashboard.texts.warning_text') }}</span>
                        </div>
                    </div>
                </div>

                @include('admin.locations._includes.form-buttons')
            </div>

        </form>
    </div>

@endsection
