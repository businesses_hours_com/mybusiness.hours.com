@extends('admin._layouts.admin')

@section('title', optional($item->address)->full_address)

@section('breadcrumbs')
    <a href="{{ route('admin.locations.index') }}" class="kt-subheader__breadcrumbs-link">@lang('dashboard.listings')</a>
    <span class="kt-subheader__breadcrumbs-separator"></span>
    {{ optional($item->address)->full_address }}
@endsection

@section('content')
    @include('admin.locations._includes.address-form')
@endsection
