<div class="d-flex flex-row mt-4 align-content-between">
    <a href="{{ route('admin.locations.index') }}" class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bolder kt-font-transform-u mr-auto">@lang('wizard.step1.go_back')</a>
    @unless($noUpdateBtn ?? false)
        <button type="submit" class="btn btn-success btn-md btn-tall btn-wide kt-font-bolder kt-font-transform-u ml-auto">@lang('dashboard.update')</button>
    @endunless
</div>
