<div class="kt-portlet kt-wizard-v4">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                @lang('dashboard.hours')
            </h3>
        </div>
    </div>
    <form class="kt-form" method="POST" action="{{ route('admin.locations.update', $item->hash) }}">
        @method('PUT')
        @csrf
        <input type="hidden" name="current_step" value="3">

        <div class="kt-portlet__body location-hours-edit-form step3"
             data-holidays-api-route="{{ route('admin.location-wizard.load-holidays') }}">

            @include('admin.location-wizard.step3._includes.hours-type')
            @include('admin.location-wizard.step3._includes.regular')
            @include('admin.location-wizard.step3._includes.departments')
            @include('admin.location-wizard.step3._includes.seasonal')

            @include('admin.locations._includes.form-buttons')
        </div>
    </form>
</div>
