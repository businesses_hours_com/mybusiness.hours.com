<div class="kt-portlet">

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                @lang('dashboard.address')
            </h3>
        </div>
    </div>
    <form class="kt-form" method="POST" action="{{ route('admin.locations.update', $item->hash) }}">
        @method('PUT')
        @csrf
        <input type="hidden" name="current_step" value="2">

        <div class="kt-portlet__body location-address-edit-form step2"
             data-resolve-on-render="false"
             data-country="{{ config('app.country') }}"
             data-address-api-route="{{ route('admin.location-wizard.resolve-address') }}">

            @include('admin.location-wizard.step2._includes.location-fields', ['address' => $address])
            @include('admin.location-wizard.step2._includes.map')
            @include('admin.location-wizard.step2._includes.contact-fields', ['contact' => $contact])
            @include('admin.location-wizard.step2._includes.local-fields', ['local' => $contact])

            @include('admin.locations._includes.form-buttons')
        </div>

    </form>
</div>

@include('admin.location-wizard.step2._includes.autocomplete-and-map')
