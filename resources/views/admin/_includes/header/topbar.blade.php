<div class="kt-header__topbar flex-grow-1">

    <div class="kt-header__topbar-item kt-header__topbar-item--date ml-0 mr-auto">
        @if(auth()->user()->isAdmin())
            <div class="kt-header__topbar-wrapper">
                <div class="dropdown-toggle nav-link pt-2 cursor-pointer align-self-center" id="dropdown-flag" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="font-weight-bold">Country: </span>
                    <img class="mx-2" style="width:17px; height:auto" src="{{ asset('theme/dist/assets/media/flags/'.auth()->user()->getCountryName(config('app.country')).'.svg') }}"
                         alt="{{ auth()->user()->getCountryName(config('app.country'), true) }}">
                    <span>{{ auth()->user()->getCountryName(config('app.country'), true) }}</span>
                </div>
                <div class="dropdown-menu align-self-center" aria-labelledby="dropdown-flag">
                    @foreach(auth()->user()->country_codes as $countryCode)
                        <a class="dropdown-item flex" href="{{ route('admin.change-country', strtolower($countryCode)) }}">
                            <img class="mr-2" style="width:17px; height:auto" src="{{ asset('theme/dist/assets/media/flags/'.auth()->user()->getCountryName($countryCode).'.svg') }}"
                                 alt="{{ auth()->user()->getCountryName($countryCode) }}">
                            <span>{{ auth()->user()->getCountryName($countryCode, true) }}</span>
                        </a>
                    @endforeach
                </div>
            </div>
        @endif
    </div>

    <div class="kt-header__topbar-item kt-header__topbar-item--date mr-3">
        <div class="kt-header__topbar-wrapper" style="cursor:default">
        <span class="align-self-center">
           <span id="localDate"></span>
            <script>
                document.addEventListener('DOMContentLoaded', () => {
                    document.getElementById('localDate').innerText = ( new Date() ).toLocaleDateString();
                });
            </script>
        </span>
        </div>
    </div>

    @include('admin._includes.header.language-item')
    @include('admin._includes.header.user-item')
</div>

