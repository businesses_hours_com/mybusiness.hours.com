<div class="kt-header__topbar-item kt-header__topbar-item--user">
    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
        <div class="kt-header__topbar-user">

            <span class="kt-header__topbar-welcome kt-hidden-mobile">Hi </span>
            @if(auth()->user()->name)
                <span class="kt-header__topbar-username kt-hidden-mobile">{{ auth()->user()->name }}</span>
            @endif

            @if($gravatar = auth()->user()->gravatar)
                <img class="" alt="Pic" src="{{ $gravatar }}"/>
            @else
                <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">{{ auth()->user()->name[0] ?? ucfirst(auth()->user()->email[0] ?? 'x') }}</span>
            @endif
        </div>
    </div>

    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

        <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url({{ asset('theme/dist/assets/media/misc/bg-1.jpg') }})">
            <div class="kt-user-card__avatar">
                @if($gravatar = auth()->user()->gravatar)
                    <img class="" alt="Pic" src="{{ $gravatar }}"/>
                @else
                    <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">{{ auth()->user()->name[0] ?? ucfirst(auth()->user()->email[0] ?? 'x') }}</span>
                @endif
            </div>
            <div class="kt-user-card__name">
                {{ auth()->user()->full_name }}
            </div>
        </div>

        <div class="kt-notification">
            <a href="{{ route('admin.users.edit', auth()->id()) }}" class="kt-notification__item">
                <div class="kt-notification__item-icon">
                    <i class="flaticon2-calendar-3 kt-font-success"></i>
                </div>
                <div class="kt-notification__item-details">
                    <div class="kt-notification__item-title kt-font-bold">
                        @lang('dashboard.my_account')
                    </div>
                    <div class="kt-notification__item-time">
                        @lang('dashboard.account_settings_and_more')
                    </div>
                </div>
            </a>
            <div class="kt-notification__custom kt-space-between">
                <form action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-label btn-label-brand btn-sm btn-bold">@lang('Logout')</button>
                </form>
            </div>
        </div>

    </div>
</div>
