<div class="kt-header__topbar-item kt-header__topbar-item--langs">
    <div class="kt-header__topbar-wrapper"
        {{ count(auth()->user()->getCurrentCountryLocales()) > 1 ? 'data-toggle=dropdown data-offset=10px,0px' : '' }}>
        <div class="kt-header__topbar-icon">
            <img src="{{ asset('theme/dist/assets/media/flags/'.auth()->user()->getCountryName(config('app.locale')).'.svg') }}" alt="{{ auth()->user()->getCountryName(config('app.locale')) }}">
        </div>
    </div>

    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround">
        <ul class="kt-nav kt-margin-t-10 kt-margin-b-10">
            @foreach(auth()->user()->getCurrentCountryLocales() as $localeCode => $localeName)
                <li class="kt-nav__item {{ app()->getLocale() === $localeCode ? 'kt-nav__item--active' : '' }}">
                    <a href="{{ route('admin.change-locale', $localeCode) }}" class="kt-nav__link">
                        <span class="kt-nav__link-icon"><img src="{{ asset('theme/dist/assets/media/flags/'.auth()->user()->getCountryName($localeCode).'.svg') }}"
                                                             alt="{{ auth()->user()->getCountryName($localeCode) }}"/></span>
                        <span class="kt-nav__link-text">{{ Locale::getDisplayLanguage($localeCode) }}</span>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>

</div>
