<div class="btn-group btn-group-sm text-center row-actions justify-content-end {{ $locationButtons ?? false ? 'location-actions' : ''}}" role="group" aria-label="edit action">
    @if($editRoute ?? null)
        <a href="{{ $editRoute }}" class="btn mx-1 btn-outline-hover-brand btn-elevate btn-icon">
            <i class="la la-edit la-2x"></i>
        </a>
    @endif
    @if($addLocationRoute ?? null)
        <a href="{{ $addLocationRoute }}" class='ml-2 text-nowrap flex-no-grow btn btn-outline-primary btn-sm edit px-3 kt-font-transform-u'>
            @lang('wizard.step1.add_location_btn')
        </a>
    @endif
    @if($locationButtons ?? false)

        @if(collect($location->users)->firstWhere('id', auth()->id()))
            <a href="{{ $addressRoute }}" class='text-nowrap flex-no-grow btn btn-outline-primary btn-sm px-2 kt-font-transform-u'>
                @lang('dashboard.address')
            </a>
            <a href="{{ $hoursRoute }}" class='text-nowrap flex-no-grow btn btn-outline-primary btn-sm px-2 kt-font-transform-u'>
                @lang('dashboard.hours')
            </a>
        @endif

        @if(auth()->user()->shouldShowQaAndReviews())
            <a href="{{ $reviewsRoute }}" class='text-nowrap flex-no-grow btn btn-outline-primary btn-sm px-2 kt-font-transform-u'>
                @lang('dashboard.reviews')
            </a>
            <a href="{{ $qaRoute }}" class='text-nowrap flex-no-grow btn btn-outline-primary btn-sm px-2 kt-font-transform-u'>
                @lang('dashboard.q_and_a')
            </a>
        @endif

    @endif

    @if($responseRoute ?? false)
        <a href="{{ $responseRoute }}" class='text-nowrap flex-no-grow btn btn-outline-primary btn-sm edit px-3 kt-font-transform-u'>
            {{ $responseText }}
        </a>
    @endif

    @if($answersRoute ?? false)
        <a href="{{ $answersRoute }}" class='text-nowrap flex-no-grow btn btn-outline-primary btn-sm edit px-3 kt-font-transform-u'>
            @lang('dashboard.answers') ({{ $answersNumber }})
        </a>
    @endif

    @if(($destroyRoute ?? false) || ($reportRoute ?? false))
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right" style="">
                @if($destroyRoute ?? false)
                    <a class="dropdown-item ajax-delete" href="{{ $destroyRoute }}">
                        <i class="la la-remove"></i> @lang('dashboard.delete')
                    </a>
                @endif
                @if($reportRoute ?? false)
                    <a class="dropdown-item ajax-report" href="{{ $reportRoute }}" data-reports="{{ json_encode($reports ?? []) }}" data-is-appropriate="{{ $isAppropriate ? 1 : 0}}">
                        <i class="la la-exclamation-circle"></i> @lang('dashboard.report')
                    </a>
                @endif
            </div>
        </div>
    @endif

</div>
