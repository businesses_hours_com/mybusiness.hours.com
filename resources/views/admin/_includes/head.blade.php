<head>
    <meta charset="utf-8"/>
    <meta name="description" content="Updates and statistics">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name') }}</title>

    <link rel="apple-touch-icon" href="{{ asset('images/ico/apple-icon-120x120.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/ico/favicon.ico') }}">

    <!--begin::Global Theme Styles(used by all pages) -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

    <link href="{{ mix('css/admin.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ mix('css/theme.css') }}" rel="stylesheet" type="text/css"/>

    <!--begin::Global Theme Styles(used by all pages)-->
{{--    <link href="https://preview.keenthemes.com//metronic/theme/html/demo1/dist/assets/plugins/global/plugins.bundle.css?v=7.2.8" rel="stylesheet" type="text/css" />--}}
{{--    <link href="https://preview.keenthemes.com//metronic/theme/html/demo1/dist/assets/plugins/custom/prismjs/prismjs.bundle.css?v=7.2.8" rel="stylesheet" type="text/css" />--}}
{{--    <link href="https://preview.keenthemes.com//metronic/theme/html/demo1/dist/assets/css/style.bundle.css?v=7.2.8" rel="stylesheet" type="text/css" />--}}
<!--end::Global Theme Styles-->

    <!--begin::Layout Themes(used by all pages)-->
{{--    <link href="https://preview.keenthemes.com//metronic/theme/html/demo1/dist/assets/css/themes/layout/header/base/light.css?v=7.2.8" rel="stylesheet" type="text/css" />--}}
{{--    <link href="https://preview.keenthemes.com//metronic/theme/html/demo1/dist/assets/css/themes/layout/header/menu/light.css?v=7.2.8" rel="stylesheet" type="text/css" />--}}
<!--end::Layout Themes-->

    @stack('css')
</head>
