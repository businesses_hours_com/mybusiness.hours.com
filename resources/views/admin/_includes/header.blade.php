<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

    <!-- Uncomment this to display the close button of the panel
    <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
    -->

    @include('admin._includes.header.menu')
    @include('admin._includes.header.topbar')
</div>
