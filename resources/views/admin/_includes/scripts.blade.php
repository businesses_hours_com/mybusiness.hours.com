<script>
    var KTAppOptions = {
        'colors': {
            'state': {
                'brand': '#5d78ff',
                'dark': '#282a3c',
                'light': '#ffffff',
                'primary': '#5867dd',
                'success': '#34bfa3',
                'info': '#36a3f7',
                'warning': '#ffb822',
                'danger': '#fd3995'
            },
            'base': {
                'label': [
                    '#c5cbe3',
                    '#a1a8c3',
                    '#3d4465',
                    '#3e4466'
                ],
                'shape': [
                    '#f0f3ff',
                    '#d9dffa',
                    '#afb4d4',
                    '#646c9a'
                ]
            }
        }
    };
    window.locale    = "{{ config('app.locale') }}";
    window.debug123  = {'url': "{{ config('app.url') }}", 'country': "{{ config('app.country') }}", 'locale': "{{ config('app.locale') }}"};
</script>

<!--begin::Global Theme Bundle(used by all pages) -->
{{--<script src="{{ asset('theme/dist/assets/plugins/global/plugins.bundle.js') }}" type="text/javascript"></script>--}}
{{--<script src="{{ asset('theme/dist/assets/js/scripts.bundle.js') }}" type="text/javascript"></script>--}}
<!--end::Global Theme Bundle -->

<!--begin::Page Vendors(used by this page) -->
{{--<script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script>--}}
{{--<script src="{{ asset('theme/dist/assets/plugins/custom/gmaps/gmaps.js') }}" type="text/javascript"></script>--}}
<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<!--end::Page Scripts -->

<script src="{{ mix('js/theme.js') }}"></script>
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js" defer></script>--}}

<!-- Datatables -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js" defer></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" defer></script>

<script src="{{ mix('js/admin.js') }}"></script>

<!-- Custom scripts -->
@stack('scripts')
