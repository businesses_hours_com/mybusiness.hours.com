<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body kt-portlet__body--fit">
        <div class="form-group mb-0">
            <table class="w-100 table table-striped table-bordered table-hover table-checkable no-footer mb-0">
                {{ $slot }}
            </table>
        </div>
    </div>
</div>