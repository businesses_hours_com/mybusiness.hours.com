<div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-footer__copyright">
            2019&nbsp;©&nbsp;<a href="{{ config('app.url') }}" target="_blank" class="kt-link">{{ config('app.name') }}</a>
        </div>
        <div class="kt-footer__menu">
            <a href="{{ config('app.url') }}" target="_blank" class="kt-footer__menu-link kt-link">About</a>
            <a href="{{ config('app.url') }}" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
            <a href="{{ config('app.url') }}" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
        </div>
    </div>
</div>