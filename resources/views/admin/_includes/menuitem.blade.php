@if($view)
    @php($relativeUrl = str_replace(request()->getSchemeAndHttpHost().'/', '', $route))
    <li class="kt-menu__item  kt-menu__item--submenu {{ request()->is("*$relativeUrl") ? 'kt-menu__item--active' : '' }}"
        aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
        <a href="{{ $route }}" class="kt-menu__link kt-menu__toggle">
            @if(isset($icon))
                <span class="kt-menu__link-icon">
                    {!! $icon !!}
                </span>
            @endif
            <span class="kt-menu__link-text">{{ $title }}</span>
        </a>
    </li>
@endif
