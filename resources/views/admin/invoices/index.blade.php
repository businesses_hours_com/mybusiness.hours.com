@extends('admin._layouts.admin')

@section('title', __('dashboard.listings'))

@section('breadcrumbs')
    <a href="{{ route('admin.invoices.index') }}" class="kt-subheader__breadcrumbs-link">@lang('dashboard.invoices')</a>
@endsection

@push('css')
    <link href="{{ asset('css/theme-v7.2.8.css') }}" rel="stylesheet" type="text/css"/>
@endpush

@section('content')

    <div class="card card-custom gutter-b">

        <div class="card-header border-0 py-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bolder text-dark">@lang('dashboard.invoices')</span>
            </h3>
        </div>

        <div class="card-body py-0 pb-3">
            <div class="table-responsive">
                <table class="table shadow-none table-head-custom table-vertical-center" id="kt_advance_table_widget_3">
                    <thead>
                    <tr class="text-left text-uppercase">
                        <th class="pb-3"></th>
                        <th class="pb-3">@lang('dashboard.status')</th>
                        <th class="pb-3">@lang('dashboard.date')</th>
                        <th class="pb-3">@lang('dashboard.total')</th>
                        <th class="pb-3 text-center">@lang('dashboard.download')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(auth()->user()->orders()->processed()->get() as $order)
                        @if($order->total)
                            <tr data-id="{{ $order->id }}">
                                <td>{{ $order->id }}</td>
                                <td>{{ $order->mollie_payment_status === 'paid' ? __('dashboard.paid') : $order->mollie_payment_status }}</td>
                                <td>{{ $order->processed_at_formatted }}</td>
                                <td>{{ ($order->total/100) . ' ' . $order->currency }}</td>
                                <td class="text-center">
                                    <a href="{{ route('admin.invoices.download', ['order_id' => $order->id]) }}" class="text-dark-75 text-hover-primary font-size-lg d-inline-block">
                                    <span class="btn btn-icon btn-light btn-hover-primary btn-sm">
                                        <i class="fas fa-lg fa-file-pdf" style="color: #3699ff;"></i>
                                    </span>
                                    </a>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>

@endsection
