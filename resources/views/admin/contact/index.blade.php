@extends('admin._layouts.admin')

@section('title', __('dashboard.contact'))

@section('breadcrumbs')
    <a href="{{ route('admin.contact.index') }}" class="kt-subheader__breadcrumbs-link">@lang('dashboard.contact')</a>
@endsection

@section('content')
    <div class="card card-custom gutter-b">
        <div class="card-header bg-white">
            <div class="card-title mx-2 my-4">
                <h5 class="card-label" style="color:#181c32; font-weight:500;">@lang('Send us your enquiries')</h5>
            </div>
        </div>

        <form class="form" method="POST" action="{{ route('admin.contact.send') }}">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-3"></div>
                    <div class="col-xl-6">
                        <div class="form-group">
                            <label for="name">@lang('dashboard.name')</label>
                            <input id="name" name="name" type="text" class="form-control form-control-solid form-control-lg" placeholder="Enter your name" value="{{ auth()->user()->full_name }}">
                        </div>

                        <div class="form-group">
                            <label for="email">@lang('dashboard.email')</label>
                            <input id="email" required name="email" type="email" class="form-control form-control-solid form-control-lg text-sm" placeholder="Enter email" value="{{ auth()->user()->email }}">
                        </div>

                        <div class="form-group">
                            <label for="about">@lang('dashboard.subject')</label>
                            <select name="about" id="about" class="form-control form-control-solid">
                                <option value="@lang('dashboard.invoices')">@lang('dashboard.invoices')</option>
                                <option value="@lang('dashboard.claim_listing')">@lang('dashboard.claim_listing')</option>
                                <option value="@lang('dashboard.update_listing')">@lang('dashboard.update_listing')</option>
                                <option value="@lang('dashboard.other')">@lang('dashboard.other')</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="text">@lang('Your Message')</label>
                            <textarea required class="form-control form-control-solid form-control-lg" id="text" name="text" rows="3"></textarea>
                            <span class="form-text text-muted">@lang('We\'ll never share your email with anyone else.')</span>
                        </div>

                    </div>
                    <div class="col-xl-3"></div>
                </div>
            </div>

            <div class="card-footer py-4 bg-white">
                <div class="row">
                    <div class="col-xl-3"></div>
                    <div class="col-xl-6">
                        <button type="submit" class="btn btn-primary font-weight-bold mr-2">@lang('Submit')</button>
                    </div>
                    <div class="col-xl-3"></div>
                </div>
            </div>

        </form>

    </div>
@endsection
