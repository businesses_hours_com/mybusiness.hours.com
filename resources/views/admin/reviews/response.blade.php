@extends('admin._layouts.admin')

@section('title', 'Response')

@section('breadcrumbs')
    <a href="{{ route('admin.reviews.index') }}" class="kt-subheader__breadcrumbs-link">
        {{ ucfirst(__('dashboard.reviews')) }}
    </a>
@endsection

@section('content')
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    {{ optional($item)->id ? ucfirst(__('dashboard.response')) : __('dashboard.new_response') }}
                </h3>
            </div>
        </div>

        <form class="kt-form px-2" method="POST" action="{{ route('admin.reviews.responses.'.($item ? 'update' : 'store'), [$review->id, optional($item)->id]) }}">

            @if($item)
                @method('PUT')
            @endif

            @csrf
            <div class="kt-portlet__body answer-form mx-6">

                <div class="row form-group mb-3">
                    <h4 class="w-100">
                        @lang('dashboard.review')
                    </h4>
                </div>

                <div class="row mb-3">
                    @include('admin.reviews._includes.stars', [
                       'value' => $review->avg_rating,
                       'readOnly' => true,
                   ])
                </div>

                <p class="row w-75 mb-5">{{ trim($review->text) }}</p>

                <div class="row mb-3">
                    <h4 class="w-100">@lang('dashboard.response')</h4>
                </div>

                <div class="row form-group">
                    <div class="col-xl-6 pl-0">
                        <div class="form-group">
                            <label>@lang('dashboard.user_name')</label>
                            <input type="text" class="form-control"
                                   name="name"
                                   placeholder="User name"
                                   value="{{ optional($item)->name ?? Auth::user()->name ?? null }}">
                        </div>
                    </div>
                    <div class="col-xl-6 pr-0">
                        <div class="form-group">
                            <label>@lang('dashboard.email')</label>
                            <input type="email" class="form-control website"
                                   name="email" required
                                   placeholder="name@email.com"
                                   value="{{ optional($item)->email ?? Auth::user()->email ?? null }}">
                        </div>
                    </div>
                </div>

                <div class="row form-group">
                    <label>@lang('dashboard.text')</label>
                    <textarea class="form-control" name="text" id="text" rows="7" placeholder="Text" required>{{ trim(optional($item)->text) }}</textarea>
                </div>

                <div class="d-flex flex-row mt-4 align-content-between">
                    <a href="{{ route('admin.reviews.index') }}" class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bolder kt-font-transform-u mr-auto">
                        @lang('dashboard.back')
                    </a>
                    @if(!$item)
                        <button type="submit" class="btn btn-success btn-md btn-tall btn-wide kt-font-bolder kt-font-transform-u ml-auto">
                            @lang('dashboard.create')
                        </button>
                    @else
                        <button type="submit" class="btn btn-success btn-md btn-tall btn-wide kt-font-bolder kt-font-transform-u ml-auto">
                            @lang('dashboard.update')
                        </button>
                    @endif
                </div>
            </div>

        </form>
    </div>
@endsection
