@php
    $id =$id ?? 'general';
    $name = ($single ?? false) ? 'score' : 'score['.$id.']';
    $readOnly = $readOnly ?? 'false';
    $phpRender = $phpRender ?? false;
@endphp

<div class="ratingRow pb-2">
    @if($title ?? false)
        <p class="mb-1">{{ ucfirst($title) }}</p>
    @endif
    <div id="{{ $id }}" style="cursor: pointer;"></div>
</div>

@if($phpRender)
    <script>
      $(document).ready(function () {

        $.fn.raty.defaults.path = '{{ asset('images/raty/') }}';

        // Single
        $('#{{ $id }}').raty({
          numberMax: 5,
          scoreName: '{{ $name }}',
          score: {{ $value ?? 0 }},
          readOnly: {{ $readOnly }},
        });
      });
    </script>
@else
    @push('scripts')
        <script>
          $(document).ready(function () {

            $.fn.raty.defaults.path = '{{ asset('images/raty/') }}';

            // Single
            $('#{{ $id }}').raty({
              numberMax: 5,
              scoreName: '{{ $name }}',
              score: {{ $value ?? 0 }},
              readOnly: {{ $readOnly }},
            });

          });
          @if($removeHiddenInput ?? false)
          setTimeout(function () {
            $('[name^="score"]').val('');
          }, 3000);
            @endif
        </script>
    @endpush
@endif
