@extends('admin._layouts.admin')

@section('title', __('dashboard.reviews'))

@section('breadcrumbs')
    <a href="{{ route('admin.reviews.index') }}" class="kt-subheader__breadcrumbs-link">@lang('dashboard.reviews')</a>
@endsection

@section('content')
    @foreach($businesses as $businessId => $businessName)
        @if($items->has($businessName))
            <section class="mb-5">
                <div class="d-flex flex-row">
                    <h4 class="ml-0 mr-auto btn-font-dark">{{ $businessName }}</h4>
                    @if(Auth::user()->canAccessBusiness($businessId))
                        <a class="mb-2 btn btn-primary btn-font-sm btn-sm btn-thin kt-font-transform-u" href="{{ route('admin.businesses.edit', $businessId) }}">
                            <i class="fa fa-fw fa-edit"></i> @lang('dashboard.edit')
                        </a>
                    @endif
                </div>

                @component('admin._includes.index-table')
                    <thead>
                    <tr class="header-tr mb-2">
                        <th class="pb-3">@lang('dashboard.location')</th>
                        <th class="pb-3">@lang('dashboard.user_id')</th>
                        <th class="pb-3">@lang('dashboard.email')</th>
                        <th class="pb-3">@lang('dashboard.votes')</th>
                        <th class="pb-3">@lang('dashboard.text')</th>
                        <th class="pb-3">@lang('dashboard.rating')</th>
                        <th class="pb-3">@lang('dashboard.reported')</th>
                        <th class="pb-3">@lang('dashboard.date')</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items->get($businessName) as $location)
                        @foreach($location->location_hash->reviews as $review)
                            <tr data-id="{{ $location->id }}">
                                <td>{{ optional($location->address)->full_address }}</td>
                                <td>{{ $review->user_id }}</td>
                                <td>{{ $review->total_votes }}</td>
                                <td style="max-width: 300px;">{{ Illuminate\Support\Str::limit($review->text, 50) }}</td>
                                <td style="min-width:190px">
                                    @include('admin.reviews._includes.stars', [
                                        'value' => $review->avg_rating,
                                        'readOnly' => true,
                                    ])
                                </td>
                                <td>{{ optional($review->report)->text ?? 'no' }}</td>
                                <td>{{ optional(\Carbon\Carbon::parse($review->updated_at))->format('d/m/Y') }}</td>
                                <td class="px-2 text-right">
                                    @include('admin._includes.row-actions', [
                                        'responseRoute' => collect($review->responses)->isEmpty() ?
                                            route('admin.reviews.responses.create', $review->id) :
                                            route('admin.reviews.responses.edit', [$review->id, collect($review->responses)->first()->id]),
                                        'responseText' => collect($review->responses)->isEmpty() ?
                                            __('dashboard.respond') :
                                            __('dashboard.edit_response'),
                                        'destroyRoute' => route('admin.reviews.destroy', $review->id),
                                        'reportRoute' =>route('admin.reviews.report', $review->id),
                                        'isAppropriate' => !is_null($review->appropriate_at)
                                    ])
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                    @if(! $items->has($businessName))
                        <tr>
                            <td colspan="9">
                                @lang('dashboard.no_reviews_found')
                            </td>
                        </tr>
                    @endif
                    </tbody>
                @endcomponent
            </section>
        @endif
    @endforeach
@endsection
