@extends('admin._layouts.admin')

@push('css')
    <link href="{{ asset('theme/dist/assets/css/pages/wizard/wizard-4.css') }}" rel="stylesheet" type="text/css"/>
@endpush

@section('title', __('wizard.title'))

@section('breadcrumbs')
    <a href="" class="kt-subheader__breadcrumbs-link">@lang('dashboard.add_listing')</a>
@endsection

@section('content')
    <div class="location-wizard kt-wizard-v4" id="location-wizard"
         data-ktwizard-state="first"
         data-current-step="{{ $currentStep }}"
         data-save-progress-route="{{ route('admin.location-wizard.save-progress') }}"
         data-destroy-progress-route="{{ route('admin.location-wizard.destroy-progress') }}"
         data-country="{{ config('app.country') }}"
         data-user-id="{{ auth()->id() }}">

        @include('admin.location-wizard._includes.nav')

        <div class="kt-portlet">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-grid">
                    <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">

                        <form class="wizard-form kt-form" id="kt_form" method="POST" action="{{ route('admin.location-wizard.store') }}" novalidate="novalidate">
                            @csrf

                            @include('admin.location-wizard.step1.index')
                            @include('admin.location-wizard.step2.index')
                            @include('admin.location-wizard.step3.index')

                            <div class="kt-form__actions justify-content-end">

                                <button type="button" class="btn-add-new-business btn btn-success btn-md kt-font-bold kt-font-transform-u d-none">
                                    @lang('wizard.step1.add_new_btn')
                                </button>

                                <button type="button" data-ktwizard-type="action-prev" class="btn btn-back btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u mr-auto d-none btn-reset">
                                    @lang('wizard.step1.go_back')
                                </button>

                                <button type="submit" name="manage" class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u btn-manage ml-auto d-none">
                                    @lang('wizard.step1.manage_selected_locations_btn')
                                </button>

                                <button type="button" data-ktwizard-type="action-next" class="btn btn-next btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u btn-add-new-location ml-2 d-none"
                                        data-text="{{ __('wizard.step1.add_location_btn') }}"
                                        data-opening-times-btn-text="{{ __('wizard.step2.opening_times_btn') }}">
                                    @lang('wizard.step1.add_location_btn')
                                </button>

                                <button type="submit" name="promotions" class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u ml-2 btn-promotions d-none"
                                        data-add-new-location-btn-text="{{ __('wizard.step3.add_new_location_btn') }}">
                                    @lang('wizard.step3.promotions_btn')
                                </button>

                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
