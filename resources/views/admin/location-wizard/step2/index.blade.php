<div class="kt-wizard-v4__content step2"
     data-ktwizard-type="step-content"
     data-step="2"
     data-address-api-route="{{ route('admin.location-wizard.resolve-address') }}">

    <div class="kt-heading kt-heading--md">@lang('wizard.step2.title')</div>

    <div class="kt-form__section kt-form__section--first">
        <div class="kt-wizard-v4__form">
            @include('admin.location-wizard.step2._includes.location-fields', ['address' => $step2['new_location']['address'] ?? []])
            @include('admin.location-wizard.step2._includes.map')
            @include('admin.location-wizard.step2._includes.contact-fields', ['contact' => $step2['new_location'] ?? []])
            @include('admin.location-wizard.step2._includes.local-fields', ['local' => $step2['new_location'] ?? []])
        </div>
    </div>

</div>

@include('admin.location-wizard.step2._includes.autocomplete-and-map')

