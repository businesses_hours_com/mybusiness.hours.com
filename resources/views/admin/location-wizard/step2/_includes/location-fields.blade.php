<div class="row invalid-address-wrap d-none">
    <p class="mx-2 px-4 py-2 alert-danger w-100">{{ __('wizard.step2.invalid_address') }}</p>
</div>
<div class="row">
    <input type="hidden" class="form-control address-id" name="step2[new_location][address][address_id]"
           value="{{ $address['address_id'] ?? $address['id'] ?? null }}">
    <input type="hidden" class="form-control country" name="step2[new_location][address][country]"
           value="{{ $address['country'] ?? null }}">
    <input type="hidden" class="form-control lat" name="step2[new_location][address][lat]"
           value="{{ $address['lat'] ?? null }}">
    <input type="hidden" class="form-control lng" name="step2[new_location][address][lng]"
           value="{{ $address['lng'] ?? null }}">
    <input type="hidden" class="form-control street-number" name="step2[new_location][address][street_number]"
           value="{{ $address['street_number'] ?? null }}">

    <input type="hidden" class="form-control google-places-json" name="step2[new_location][address][google_places_json]"
           value="{{ $address['google_places_json'] ?? null }}">

        <div class="col-12">
            <div class="form-group">
                <label>@lang('wizard.step2.full_address')</label>
                <input title="{{ __('wizard.step2.full_address') }}" type="text" class="form-control full-address"
                       name="step2[new_location][address][full_address]"
                       placeholder=""
                       required
                       value="{{ $address['full_address'] ?? null }}"
                >
                <span class="form-text text-muted">@lang('wizard.step2.enter_your_full_address')</span>
            </div>
        </div>

    <div class="col-xl-4">
        <div class="form-group">
            <label>@lang('wizard.step1.postcode')</label>
            <input title="{{ __('wizard.step1.postcode') }}" type="text" class="form-control postcode"
                   name="step2[new_location][address][postal_code]"
                   placeholder=""
                   required
                   readonly
                   value="{{ $address['postal_code'] ?? null }}"
            >
            <span class="form-text text-muted">@lang('wizard.step2.enter_your_postcode')</span>
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group">
            <label>@lang('wizard.step2.house_number')</label>
            <input title="{{ __('wizard.step2.house_number') }}" type="text" class="form-control house-number"
                   name="step2[new_location][address][house_number]"
                   placeholder=""
                  readonly
                   value="{{ $address['house_number'] ?? $address['street_number'] ?? null }}"
            >
            <span class="form-text text-muted">@lang('wizard.step2.enter_your_house_number')</span>
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group">
            <label>@lang('wizard.step2.house_number_addition')</label>
            <input title="{{ __('wizard.step2.house_number_addition') }}" type="text" class="form-control addition"
                   name="step2[new_location][address][house_number_addition]"
                   placeholder=""
                  readonly
                   value="{{ $address['house_number_addition'] ?? null }}"
            >
            <span class="form-text text-muted">@lang('wizard.step2.enter_your_house_number_addition')</span>
        </div>
    </div>
</div>

<div class="form-group">
    <label>@lang('wizard.step2.street')</label>
    <input title="{{ __('wizard.step2.street') }}" type="text" class="form-control street"
           name="step2[new_location][address][street]"
           readonly
           placeholder=""
           value="{{ $address['street'] ?? null }}"
    >
    <span class="form-text text-muted">@lang('wizard.step2.enter_your_street_address')</span>
</div>

<div class="form-group">
    <label>@lang('wizard.step2.city')</label>
    <input title="{{ __('wizard.step2.city') }}" type="text" class="form-control city"
           name="step2[new_location][address][locality]"
           readonly
           placeholder=""
           value="{{ $address['locality'] ?? null }}"
    >
    <span class="form-text text-muted">@lang('wizard.step2.enter_your_city')</span>
</div>

