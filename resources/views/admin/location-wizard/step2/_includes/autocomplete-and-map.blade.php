@push('scripts')
    <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9saGRrneL_JOvv9tvJomli_DjRiIJkQ4&libraries=places&callback=initAutocompleteAddress"></script>

    <script>
        function initAutocompleteAddress(){
            let fullAddress = document.querySelector('.full-address');

            if (! fullAddress) {
                return;
            }

            const autocomplete = new google.maps.places.Autocomplete(fullAddress, {
                componentRestrictions: {country: ["{{ config('app.country') }}"]},
                fields: ['place_id', 'address_components', 'formatted_address', 'geometry', 'name', 'photo', 'type']
            });

            autocomplete.addListener('place_changed', function(){
                const place = autocomplete.getPlace();
                console.log(place);
                locationWizard.step2.autocompleteAddress(place);
            });
        }
    </script>
@endpush
