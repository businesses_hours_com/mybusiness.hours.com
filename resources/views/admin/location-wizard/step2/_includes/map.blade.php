<div class="row">
    <div class="col-12">
        <p class="map-loading-wrap text-center" style="font-size:2rem;display:none;"><i class="fa fa-spinner fa-spin mr-2"></i>Map is loading...</p>
        <div id="location-map" style="height: 500px;width: 100%; display:none;"></div>
    </div>
</div>
