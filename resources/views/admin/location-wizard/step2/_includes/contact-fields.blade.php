<div class="contact-fields-wrap">
    <h2 class="kt-heading kt-heading--md">@lang('wizard.step2.contact_information')</h2>

    <div class="row contact-row">
        <div class="col-lg-4">
            <div class="form-group">
                <label for="step2-email">@lang('dashboard.email')</label>
                <input id="step2-email" type="email" class="form-control email" name="step2[new_location][email]" placeholder="" value="{{ $contact['email'] ?? null }}" data-validation="step2.new_location.email">
                <span class="form-text text-muted">@lang('wizard.step2.we_will_never_share_your_email')</span>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label for="step2-tel">@lang('wizard.step2.phone')</label>
                <input id="step2-tel" type="tel" class="form-control phone" name="step2[new_location][phone]" placeholder=""
                       value="{{ $contact['phone'] ?? null }}">
                <span class="form-text text-muted">@lang('wizard.step2.enter_your_phone')</span>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label for="step2-fax">@lang('wizard.step2.fax')</label>
                <input id="step2-fax" type="tel" class="form-control fax" name="step2[new_location][fax]" placeholder=""
                       value="{{ $contact['fax'] ?? null }}">
                <span class="form-text text-muted">@lang('wizard.step2.enter_your_fax')</span>
            </div>
        </div>
    </div>
</div>
