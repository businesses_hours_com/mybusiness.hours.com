<div class="local-fields-wrap">
    <h2 class="kt-heading kt-heading--md">@lang('wizard.step2.local_info')</h2>

    <div class="row contact-row">
        <div class="col-xl-4">
            <div class="form-group">
                <label>@lang('wizard.step1.website')</label>
                <input title="Website" type="url" class="form-control" name="step2[new_location][website]"
                       value="{{ $local['website'] ?? null }}">
                <span class="form-text text-muted">@lang('wizard.step2.enter_your_local_website')</span>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="form-group" style="{{ ($showLocalFields ?? false) ? '' : 'display:none' }}">
                <label>@lang('wizard.step1.business_name')</label>
                <input title="Local name" type="text" class="form-control" name="step2[new_location][name]"
                       value="{{ $local['name'] ?? null }}">
                <span class="form-text text-muted">@lang('wizard.step2.enter_your_local_business_name')</span>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="form-group" style="{{ ($showLocalFields ?? false) ? '' : 'display:none' }}">
                <label>@lang('wizard.step2.establishment_code')</label>
                <input title="Local est code" type="text" class="form-control" name="step2[new_location][est_code]"
                       value="{{ $local['est_code'] ?? null }}">
                <span class="form-text text-muted">@lang('wizard.step2.enter_your_local_est_code')</span>
            </div>
        </div>
    </div>
</div>
