<div class="kt-wizard-v4__nav">
    <div class="kt-wizard-v4__nav-items kt-wizard-v4__nav-items--clickable">
        <div class="kt-wizard-v4__nav-item"
             data-ktwizard-state="{{ $currentStep === 1 ? 'current' : 'pending' }}"
             data-step="1"
             data-ktwizard-type="step">

            <div class="kt-wizard-v4__nav-body">
                <div class="kt-wizard-v4__nav-number">
                    1
                </div>
                <div class="kt-wizard-v4__nav-label">
                    <div class="kt-wizard-v4__nav-label-title">
                        @lang('wizard.nav.business')
                    </div>
                    <div class="kt-wizard-v4__nav-label-desc">
                        @lang('wizard.nav.step1_description')
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-wizard-v4__nav-item"
             data-ktwizard-state="{{ $currentStep === 2 ? 'current' : 'pending' }}"
             data-step="2"
             data-ktwizard-type="step">

            <div class="kt-wizard-v4__nav-body">
                <div class="kt-wizard-v4__nav-number">
                    2
                </div>
                <div class="kt-wizard-v4__nav-label">
                    <div class="kt-wizard-v4__nav-label-title">
                        @lang('wizard.nav.location')
                    </div>
                    <div class="kt-wizard-v4__nav-label-desc">
                        @lang('wizard.nav.step2_description')
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-wizard-v4__nav-item"
             data-ktwizard-state="{{ $currentStep === 3 ? 'current' : 'pending' }}"
             data-step="3"
             data-ktwizard-type="step">

            <div class="kt-wizard-v4__nav-body">
                <div class="kt-wizard-v4__nav-number">
                    3
                </div>
                <div class="kt-wizard-v4__nav-label">
                    <div class="kt-wizard-v4__nav-label-title">
                        @lang('wizard.nav.opening_times')
                    </div>
                    <div class="kt-wizard-v4__nav-label-desc">
                        @lang('wizard.nav.step3_description')
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-wizard-v4__nav-item"
             data-ktwizard-type="step"
             data-step="4"
             data-ktwizard-state="pending">
            <div class="kt-wizard-v4__nav-body">
                <div class="kt-wizard-v4__nav-number">
                    4
                </div>
                <div class="kt-wizard-v4__nav-label">
                    <div class="kt-wizard-v4__nav-label-title">
                        @lang('wizard.nav.promotions')
                    </div>
                    <div class="kt-wizard-v4__nav-label-desc">
                        @lang('wizard.nav.step4_description')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
