<div class="kt-wizard-v4__content step3"
     data-ktwizard-type="step-content"
     data-step="3"
     data-holidays-api-route="{{ route('admin.location-wizard.load-holidays') }}">

    <div class="kt-heading kt-heading--md">@lang('wizard.step3.title')</div>

    <div class="kt-form__section kt-form__section--first">
        <div class="kt-wizard-v4__form">
            @include('admin.location-wizard.step3._includes.hours-type')
            @include('admin.location-wizard.step3._includes.regular')
            @include('admin.location-wizard.step3._includes.departments')
            @include('admin.location-wizard.step3._includes.seasonal')
        </div>
    </div>
</div>
