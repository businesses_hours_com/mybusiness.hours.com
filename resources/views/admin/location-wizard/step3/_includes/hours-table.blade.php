<?php
$type         = $type ?? 'regular';
$isExceptions = $type === 'exceptions';
?>

<table class="regular-hours-table w-100 mt-3 table table-striped table-bordered table-hover no-footer"
       data-type="{{ $type }}"
       data-regular="{{ json_encode($valueArray) }}"
       data-holidays="{{ json_encode($holidays) }}"
       data-exceptions="{{ json_encode($exceptions) }}">
    <thead>
    <tr class="header-tr mb-2">
        <th scope="col" class="pb-3">
            {{ $isExceptions ? __('wizard.step3.period') : __('wizard.step3.day') }}
        </th>
        <th scope="col" class="pb-3">@lang('wizard.step3.open_closed')</th>
        @if($isExceptions)
            <th class="pb-3">@lang('wizard.step3.reason')</th>
        @endif
        <th scope="col" class="pb-3 opening-time">@lang('wizard.step3.opening_time')</th>
        <th scope="col" class="pb-3 closing-time">@lang('wizard.step3.closing_time')</th>
        <th scope="col" class="pb-3">@lang('wizard.step3.appointment')</th>
        <th scope="col" class="pb-3">@lang('wizard.step3.action')</th>
    </tr>
    </thead>

    <tbody>
    @foreach($rows as $i => $day)
        <tr data-day-num="{{ $i }}" style="{{ $isExceptions && is_string($day) ? 'display:none' : '' }}">
            <td data-label="{{ $isExceptions ? ($valueArray[$i]['period'] ?? '') : $day }}" class="day-name pb-1 td-day">
                @if($isExceptions)
                    <div class="input-group">
                        <input data-num="{{ $i }}"
                               type="text" class="form-control date-range" title="date-range"
                               placeholder="{{ __('wizard.step3.select_date_range') }}"
                               name="{{ $namePrefix }}[{{ $i }}][period]"
                               value="{{ $valueArray[$i]['period'] ?? '' }}"
                            {{ $isExceptions && is_string($day) ? 'disabled' : '' }}>
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                        </div>
                    </div>
                @else
                    <span>{{ $day }}</span>
                    <input type="hidden" class="hidden-name" name="{{ $namePrefix }}[{{ $i }}][name]" value="">
                    @if($i < count($rows)-1 || count($rows) === 1)
                        <select class="copy-to form-control {{ ($type ?? false) !== 'holidays' ? 'select2' : '' }}"
                                title="Copy to" aria-invalid="false" data-placeholder="{{ __('wizard.step3.copy_to') }}">
                            <option value="">@lang('wizard.step3.copy_to')</option>
                            <option value="all">
                                @if(($type ?? false) === 'holidays')
                                    @lang('wizard.step3.copy_to_all_other_holidays')
                                @else
                                    @lang('wizard.step3.copy_to_all_other_days')
                                @endif
                            </option>
                            @foreach($rows as $index => $dayName)
                                @if($index !== $i && strtolower($dayName) !== 'sunday')
                                    <option value="{{ $index }}">{{ ucfirst($dayName) }}</option>
                                @endif
                            @endforeach
                        </select>
                    @endif
                @endif
            </td>
            <td data-label="Open/Closed" class="td-open">
                @if(! $sundaySpecial || ($sundaySpecial && $i !== 6))
                    <select name="{{ $namePrefix }}[{{ $i }}][open_status]"
                            {{ $isExceptions && is_string($day) ? 'disabled' : '' }}
                            class="form-control open-status btn dropdown-toggle {{ in_array($valueArray[$i]['open_status'] ?? 'closed', ['closed'], true) ? 'btn-outline-danger' : 'btn-outline-success' }}"
                            title="select open status" aria-describedby="open-status" aria-invalid="false">
                        <option value="closed" {{ ($valueArray[$i]['open_status'] ?? 'closed') === 'closed' ? 'selected' : '' }}>@lang('wizard.step3.closed')</option>
                        <option value="open" {{ ($valueArray[$i]['open_status'] ?? null) === 'open' ? 'selected' : '' }}>@lang('wizard.step3.open')</option>
                        <option value="open_24" {{ ($valueArray[$i]['open_status'] ?? null) === 'open_24' ? 'selected' : '' }}>@lang('wizard.step3.open_24')</option>
                        @if(($type ?? false) === 'holidays')
                            <option value="open_regular" {{ ($valueArray[$i]['open_status'] ?? null) === 'open_regular' ? 'selected' : '' }}>@lang('wizard.step3.open_regular')</option>
                        @endif
                    </select>
                @elseif($sundaySpecial && $i === 6)
                    @php
                        $openStatusArray = Arr::wrap($valueArray[$i]['open_status'] ?? []);
                    @endphp
                    <select name="{{ $namePrefix }}[{{ $i }}][open_status][]" data-last="closed"
                            class="form-control sunday-status select2
                            {{ in_array('closed', $openStatusArray, true) ? 'btn-outline-danger' : 'btn-outline-success' }}"
                            title="select sunday status" multiple="multiple"
                            aria-describedby="sunday-status" aria-invalid="false">
                        <option value="closed" {{ in_array('closed', $openStatusArray, true) ? 'selected' : '' }}>
                            @lang('wizard.step3.closed')
                        </option>
                        <optgroup label="Open">
                            <option value="sunday_all" {{ in_array('sunday_all', $openStatusArray, true) ? 'selected' : '' }}>
                                @lang('wizard.step3.every_sunday_in_month')
                            </option>
                            <option value="sunday_first" {{ in_array('sunday_first', $openStatusArray, true) ? 'selected' : '' }}>
                                @lang('wizard.step3.1st_sunday')
                            </option>
                            <option value="sunday_second" {{ in_array('sunday_second', $openStatusArray, true) ? 'selected' : '' }}>
                                @lang('wizard.step3.2nd_sunday')
                            </option>
                            <option value="sunday_third" {{ in_array('sunday_third', $openStatusArray, true) ? 'selected' : '' }}>
                                @lang('wizard.step3.3rd_sunday')
                            </option>
                            <option value="sunday_fourth" {{ in_array('sunday_fourth', $openStatusArray, true) ? 'selected' : '' }}>
                                @lang('wizard.step3.4th_sunday')
                            </option>
                            <option value="sunday_second_last" {{ in_array('sunday_second_last', $openStatusArray, true) ? 'selected' : '' }}>
                                @lang('wizard.step3.second_last_sunday')
                            </option>
                            <option value="sunday_last" {{ in_array('sunday_last', $openStatusArray, true) ? 'selected' : '' }}>
                                @lang('wizard.step3.last_sunday')
                            </option>
                            <option value="open_24" {{ in_array('open_24', $openStatusArray, true) ? 'selected' : '' }}>
                                @lang('wizard.step3.open_24')
                            </option>
                        </optgroup>
                    </select>
                @endif
            </td>
            @if($isExceptions)
                <td data-label="Reason" class="td-exception-reason">
                    <select name="{{ $namePrefix }}[{{ $i }}][reason]" class="form-control exception-reason {{ ($valueArray[$i]['open_status'] ?? 'closed') === 'closed' ? '' : 'd-none' }}"
                            title="choose exception reason" aria-describedby="exception-reason" aria-invalid="false">
                        <option value="">@lang('wizard.step3.select_reason')</option>
                        <option value="holiday" {{ ($valueArray[$i]['reason'] ?? null) === 'holiday' ? 'selected' : '' }}>@lang('wizard.step3.holiday')</option>
                        <option value="renovation" {{ ($valueArray[$i]['reason'] ?? null) === 'renovation' ? 'selected' : '' }}>@lang('wizard.step3.renovation')</option>
                        <option value="sick-leave" {{ ($valueArray[$i]['reason'] ?? null) === 'sick-leave' ? 'selected' : '' }}>@lang('wizard.step3.sick_leave')</option>
                        <option value="other" {{ ($valueArray[$i]['reason'] ?? null) === 'other' ? 'selected' : '' }}>@lang('wizard.step3.other')</option>
                    </select>
                </td>
            @endif
            <td data-label="Opening time" class="td-opening-time">
                <input title="opening time" name="{{ $namePrefix }}[{{ $i }}][opening_time][]"
                       class="opening-time form-control {{ in_array($valueArray[$i]['open_status'] ?? 'closed', ['closed', 'open_24'], true) ? 'd-none' : '' }}"
                       value="{{ $valueArray[$i]['opening_time'][0] ?? '' }}"
                       {{ ($valueArray[$i]['opening_time'][0] ?? null) ? '' : 'disabled' }}
                       type="time" inputmode="numeric">
            </td>
            <td data-label="Closing time" class="td-closing-time">
                <input title="closing time" name="{{ $namePrefix }}[{{ $i }}][closing_time][]"
                       class="closing-time form-control {{ in_array($valueArray[$i]['open_status'] ?? 'closed', ['closed', 'open_24'], true) ? 'd-none' : '' }}"
                       value="{{ $valueArray[$i]['closing_time'][0] ?? '' }}"
                       {{ ($valueArray[$i]['closing_time'][0] ?? null) ? '' : 'disabled' }}
                       type="time" inputmode="numeric">
            </td>
            <td data-label="Appointment" class="td-appointment">
                <span class="kt-switch kt-switch--sm kt-switch--icon d-block">
                    <label class="pt-1 mb-0 {{ ($valueArray[$i]['open_status'] ?? 'closed') === 'closed' ? 'd-none' : '' }}">
                        <input type="hidden" class="appointment-hidden-default" name="{{ $namePrefix }}[{{ $i }}][appointment][0]" value="off">
                        <input type="checkbox" class="appointment-checkbox"
                               {{ ($valueArray[$i]['appointment'][0] ?? 'off') === 'on' ? 'checked' : '' }}
                               name="{{ $namePrefix }}[{{ $i }}][appointment][0]">
                        <span></span>
                    </label>
                </span>
            </td>
            <td data-label="Actions" class="td-actions">
                <button type="button" class="btn-add-time btn btn-success btn-sm btn-font-sm {{ ($valueArray[$i]['open_status'] ?? 'closed') === 'closed' ? 'd-none' : '' }}">
                    + @lang('wizard.step3.add_time')</button>
                @if($isExceptions)
                    <button type="button" class="ml-1 btn-remove-exception btn btn-danger btn-sm btn-font-sm">✖</button>
                @endif
                <button type="button" class="btn-remove-time btn btn-danger btn-sm btn-font-sm" style="display:none;">&#10006;</button>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
