<div class="row">
    <div class="col-md-12">
        <div class="form-group mb-2">
            <label class="mb-3">@lang('wizard.step3.opening_times_type_heading')</label>

            <div class="kt-radio-inline">
                <label for="regular" class="kt-radio">
                    <input id="regular" class="times-type-radio" type="radio" name="step3[times][type]"
                           value="regular" {{ ($times['type'] ?? 'regular') === 'regular' ? 'checked' : '' }}> @lang('wizard.step3.regular')
                    <span></span>
                </label>
                <label for="departments" class="kt-radio">
                    <input id="departments" class="times-type-radio" type="radio" name="step3[times][type]"
                           value="departments" {{ ($times['type'] ?? 'regular') === 'departments' ? 'checked' : '' }}> @lang('wizard.step3.departments')
                    <span></span>
                </label>
                <label for="seasonal" class="kt-radio">
                    <input id="seasonal" class="times-type-radio" type="radio" name="step3[times][type]"
                           value="seasonal" {{ ($times['type'] ?? 'regular') === 'seasonal' ? 'checked' : '' }}> @lang('wizard.step3.seasonal')
                    <span></span>
                </label>
            </div>
        </div>
    </div>
</div>
