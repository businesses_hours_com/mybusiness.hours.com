<div class="times seasonal-times" style="{{ ($times['type'] ?? 'regular') === 'seasonal' ? '' : 'display:none;' }}">
    <h2 class="kt-heading kt-heading--md">@lang('wizard.step3.seasonal')</h2>

    <ul class="nav nav-tabs seasonal-nav-tabs" role="tablist">
        @foreach($times['seasonal'] ?? [] as $key => $season)
            <li class="{{ $key===0 ? 'active' : '' }} nav-item">
                <a class="nav-link {{$key===0 ? 'active' : ''}}" href="#seasonal_tab_{{ $key }}" data-toggle="tab">{{ $season['period'] ?? __('wizard.step3.new_season_period') }}</a>
                <span class="remove-tab">&#10006;</span>
            </li>
        @endforeach
        <li class="nav-item">
            <a href="#" class="add-tab nav-link">+ @lang('wizard.step3.add_season_period')</a>
        </li>
    </ul>

    <div class="tab-content seasonal-tab-content">
        @foreach($times['seasonal'] ?? ['default'] as $key => $season)
            <div class="tab-pane {{ $key===0 ? 'active' : ''}}" id="seasonal_tab_{{ $key }}" role="tabpanel">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>@lang('wizard.step3.season_period')</label>
                            <div class="input-group">
                                <input data-num="{{ $key }}" type="text" title="date range"
                                       class="form-control tab-name date-range"
                                       readonly=""
                                       placeholder="{{ __('wizard.step3.select_date_range') }}"
                                       name="step3[times][seasonal][{{ $key }}][period]"
                                       value="{{ $season['period'] ?? '' }}" required>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>

                            <span class="form-text text-muted">
                                @lang('wizard.step3.select_season_period')
                            </span>
                        </div>
                    </div>
                </div>

                <div class="week-times">
                    @include('admin.location-wizard.step3._includes.hours-table',[
                        'rows' => localisedDaysOfWeekArray(),
                        'namePrefix' => 'step3[times][seasonal]['.$key.'][days]',
                        'type' => 'regular',
                        'sundaySpecial' => true,
                        'valueArray' => $times['seasonal'][$key]['days'] ?? [],
                        'exceptions' => $times['seasonal'][$key]['exceptions'] ?? [],
                        'holidays' => $times['seasonal'][$key]['holidays'] ?? [],
                    ])
                </div>

                @include('admin.location-wizard.step3._includes.holidays', [
                    'namePrefix' => 'step3[times][seasonal]['.$key.'][holidays]',
                    'valueArray' => $times['seasonal'][$key]['holidays'] ?? [],
                    'exceptions' => $times['seasonal'][$key]['exceptions'] ?? [],
                    'holidays' => $times['seasonal'][$key]['holidays'] ?? [],
                ])

                @if(auth()->user()->canEditExceptionDates())
                    @include('admin.location-wizard.step3._includes.exceptions', [
                        'namePrefix' => 'step3[times][seasonal]['.$key.'][exceptions]',
                        'valueArray' => $times['seasonal'][$key]['exceptions'] ?? [],
                        'exceptions' => $times['seasonal'][$key]['exceptions'] ?? [],
                        'holidays' => $times['seasonal'][$key]['holidays'] ?? [],
                    ])
                @endif
            </div>

        @endforeach
    </div>

</div>
