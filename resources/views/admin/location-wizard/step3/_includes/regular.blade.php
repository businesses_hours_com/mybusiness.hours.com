<div class="times regular-times" style="{{ ($times['type'] ?? 'regular') === 'regular' ? '' : 'display:none;' }}">
    <h2 class="kt-heading kt-heading--md mt-2"></h2>

    <div class="tab-content regular-tab-content">
        <div class="tab-pane active" id="department_tab_0" role="tabpanel">
            <div class="row week-times">
                <div class="col-12">
                    @include('admin.location-wizard.step3._includes.hours-table', [
                        'rows' => localisedDaysOfWeekArray(),
                        'namePrefix' => 'step3[times][regular][0][days]',
                        'type' => 'regular',
                        'sundaySpecial' => true,
                        'valueArray' => $times['regular'][0]['days'] ?? [],
                        'exceptions' => $times['regular'][0]['exceptions'] ?? [],
                        'holidays' => $times['regular'][0]['holidays'] ?? [],
                    ])
                </div>
            </div>

            @include('admin.location-wizard.step3._includes.holidays', [
                'valueArray' => $times['regular'][0]['holidays'] ?? [],
                'exceptions' => $times['regular'][0]['exceptions'] ?? [],
                'holidays' => $times['regular'][0]['holidays'] ?? [],
                'namePrefix' => 'step3[times][regular][0][holidays]',
            ])
            @if(auth()->user()->canEditExceptionDates())
                @include('admin.location-wizard.step3._includes.exceptions', [
                    'valueArray' => $times['regular'][0]['exceptions'] ?? [],
                    'namePrefix' => 'step3[times][regular][0][exceptions]',
                    'exceptions' => $times['regular'][0]['exceptions'] ?? [],
                    'holidays' => $times['regular'][0]['holidays'] ?? [],
                ])
            @endif
        </div>
    </div>
</div>

@push('scripts')
    <script src="{{ asset('js/plugins/jquery.inputmask.min.js') }}" defer></script>
    <script src="{{ asset('js/plugins/inputmask.binding.js') }}" defer></script>
@endpush
