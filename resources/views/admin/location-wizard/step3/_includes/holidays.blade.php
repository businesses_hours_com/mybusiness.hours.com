<div class="holiday-times mt-4 w-100">
    <h2 class="kt-heading kt-heading--md">Holidays</h2>
    <div class="row">
        <div class="col-12">
            @include('admin.location-wizard.step3._includes.hours-table', [
                'rows' => [
                    1 => 'holiday'
                ],
                'namePrefix' => $namePrefix,
                'type' => 'holidays',
                'sundaySpecial' => false,
            ])
        </div>
    </div>
</div>