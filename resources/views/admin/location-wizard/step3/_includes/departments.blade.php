<div class="times department-times" style="{{  ($times['type'] ?? 'regular') === 'departments' ? '' : 'display:none;' }}">
    <h2 class="kt-heading kt-heading--md">@lang('wizard.step3.departments')</h2>

    <ul class="nav nav-tabs department-nav-tabs" role="tablist" data-new-department="{{ __('wizard.step3.new_department') }}">
        @foreach($times['departments'] ?? [] as $key => $department)
            <li class="nav-item">
                <a class="nav-link {{$key===0 ? 'active' : ''}}" href="#department_tab_{{$key}}" data-toggle="tab">
                    {{ isset($department['name']) && strlen($department['name']) ? $department['name'] : __('wizard.step3.new_department') }}
                </a>
                <span class="remove-tab">&#10006;</span>
            </li>
        @endforeach
        <li class="nav-item">
            <a href="#" class="add-tab nav-link">+ @lang('wizard.step3.add_department')</a>
        </li>
    </ul>

    <div class="tab-content department-tab-content">
        @foreach($times['departments'] ?? ['default'] as $key => $department)
            <div class="tab-pane {{ $key === 0 ? 'active' : '' }}" id="department_tab_{{ $key }}" role="tabpanel">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>@lang('wizard.step3.department_name')</label>
                            <input data-num="{{ $key }}" type="text" class="form-control tab-name" name="step3[times][departments][{{ $key }}][name]" placeholder=""
                                   value="{{ $department['name'] ?? '' }}">
                            <span class="form-text text-muted">@lang('wizard.step3.enter_your_department_name')</span>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group text-center">
                            <label>@lang('wizard.step3.main_department')</label>
                            <div class="d-block mt-2">
                            <span class="kt-switch kt-switch--icon">
                                <label>
                                    <input class="isMainDepartmentCheckbox" type="checkbox"
                                           name="step3[times][departments][{{ $key }}][is_main]" {{ ($department['is_main'] ?? false) === 'on' ? 'checked' : '' }}>
                                    <span></span>
                                </label>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row week-times">
                    @include('admin.location-wizard.step3._includes.hours-table',[
                        'rows' => localisedDaysOfWeekArray(),
                        'namePrefix' => 'step3[times][departments]['.$key.'][days]',
                        'type' => 'regular',
                        'sundaySpecial' => true,
                        'valueArray' => $times['departments'][$key]['days'] ?? [],
                        'exceptions' => $times['departments'][$key]['exceptions'] ?? [],
                        'holidays' => $times['departments'][$key]['holidays'] ?? [],
                    ])
                </div>

                @include('admin.location-wizard.step3._includes.holidays', [
                    'namePrefix' => 'step3[times][departments]['.$key.'][holidays]',
                    'valueArray' => $times['departments'][$key]['holidays'] ?? [],
                    'exceptions' => $times['departments'][$key]['exceptions'] ?? [],
                    'holidays' => $times['departments'][$key]['holidays'] ?? [],
                ])

                @if(auth()->user()->canEditExceptionDates())
                    @include('admin.location-wizard.step3._includes.exceptions', [
                        'namePrefix' => 'step3[times][departments]['.$key.'][exceptions]',
                        'valueArray' => $times['departments'][$key]['exceptions'] ?? [],
                        'exceptions' => $times['departments'][$key]['exceptions'] ?? [],
                        'holidays' => $times['departments'][$key]['holidays'] ?? [],
                    ])
                @endif
            </div>

        @endforeach
    </div>
</div>

