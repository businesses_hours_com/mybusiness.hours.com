<div class="exception-times mt-4 w-100">
    <h2 class="kt-heading kt-heading--md">@lang('wizard.step3.exceptions')</h2>
    <p>@lang('wizard.step3.exceptions_help_text_line1') <br>
        @lang('wizard.step3.exceptions_help_text_line2')
    </p>

    <button type="button" class="btn btn-primary btn-wide add-exception-btn">
        @lang('wizard.step3.add_exception_date')
    </button>

    <div class="row">
        <div class="col-12">
            @include('admin.location-wizard.step3._includes.hours-table', [
                'rows' => array_filter($valueArray, fn($row) => !empty($row['period'])) ?: ['date range'],
                'namePrefix' => $namePrefix,
                'type' => 'exceptions',
                'sundaySpecial' => false,
            ])
        </div>
    </div>
</div>
