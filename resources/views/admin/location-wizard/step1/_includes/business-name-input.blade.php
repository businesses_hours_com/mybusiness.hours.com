<div class="row">
    <div class="col-xl-12">
        <div class="form-group">
            <label for="business-name">@lang('wizard.step1.select_existing_business')</label>
            <div class="row">
                <div class="col-md-6 select-business-wrapper">
                    <input type="hidden" class="business-name-input-id" name="step1[business][id]"
                           value="{{ $step1['business']['id'] ?? null }}">
                    <input title="@lang('wizard.step1.business_name')" type="text"
                           class="form-control business-name-input" name="step1[business][name]"
                           value="{{ $step1['business']['name'] ?? null }}"
                           placeholder="@lang('wizard.step1.business_name')"
                           data-id="{{ $step1['business']['id'] ?? null }}"
                           data-selected-locations="{{ json_encode($step1['locations'] ?? []) }}"
                           data-businesses-route="{{ route('admin.location-wizard.load-businesses') }}"
                           data-locations-route="{{ route('admin.location-wizard.load-locations') }}">
                </div>
            </div>
            <span class="form-text text-muted select-business-description-text">
                @lang('wizard.step1.select_existing_business_help_text')
            </span>
        </div>
    </div>
</div>