<h2 class="kt-heading kt-heading--md" data-text="{{ __('wizard.step1.we_found_the_following_locations_for') }} "></h2>

<div class="form-group">
    <table id="locations-table" class="locations-table w-100 mt-3 table table-striped table-bordered table-hover table-checkable no-footer"
           data-owned-location-text="{{ __('wizard.step1.this_location_already_has_owner', ['request_access_link' => '<a href="#">'.__('wizard.step1.request_access_link_text').'</a>']) }}"
           data-owned-location-modal-title="{{ __('wizard.step1.request_access_link_text') }}"
           data-owned-location-modal-text="{{ __('wizard.step1.request_access_modal_text', [
                'reset_password_link' => '<a href="'.route('password.request').'" target="_blank">'.lcfirst(__('auth.reset_password.title')).'</a>',
                'contact_form_link' => '<a href="'.route('admin.contact.index').'" target="_blank">'.lcfirst(__('wizard.step1.request_access_modal_text_contact_form_anchor_text')).'</a>'
            ]) }}"
    >
        <thead>
        <tr class="header-tr mb-2">
            <th class="pb-3">@lang('wizard.step1.add_to_my_account')</th>
            <th class="pb-3">@lang('wizard.step1.postcode')</th>
            <th class="pb-3">@lang('wizard.step1.city')</th>
            <th class="pb-3">@lang('wizard.step1.address')</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>


