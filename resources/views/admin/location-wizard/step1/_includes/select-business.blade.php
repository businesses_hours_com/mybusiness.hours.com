<div class="form-group mb-0">
    <table id="business-table" class="business-table w-100 mt-3 table table-striped table-bordered table-hover table-checkable no-footer" data-found-no-businesses="@lang('wizard.step1.found_no_businesses')">
        <thead>
        <tr class="header-tr mb-2">
            <th class="pb-3">@lang('wizard.step1.business_name')</th>
            <th class="pb-3">@lang('wizard.step1.locations')</th>
            <th class="pb-3 add_manage_location">@lang('wizard.step1.add_manage_location')</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>


