@if($showTitle ?? false)
    <h2 class="kt-heading kt-heading--md">@lang('wizard.step1.add_new_business')</h2>
@endif

<div class="row">
    <div class="col-xl-6">
        <div class="form-group">
            <label>@lang('wizard.step1.business_name')</label>
            <input type="text" class="form-control new-business-name"
                   name="step1[new_business][name]"
                   placeholder="Business name"
                   value="{{ $step1['new_business']['name'] ?? optional($item)->name ?? null }}">
            <span class="form-text text-muted">@lang('wizard.step1.business_name_help_text')</span>
        </div>
    </div>
    <div class="col-xl-6">
        <div class="form-group">
            <label>@lang('wizard.step1.website')</label>
            <input type="url" class="form-control website"
                   name="step1[new_business][website]"
                   placeholder="https://..."
                   value="{{ $step1['new_business']['website'] ?? optional($item)->website ?? null }}">
            <span class="form-text text-muted">@lang('wizard.step1.website_help_text')</span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-6">
        <div class="form-group">
            <label>@lang('wizard.step1.category')</label>
            <input type="hidden" class="category-hidden-name"
                   name="step1[new_business][category][name]"
                   value="{{ $step1['new_business']['category']['name'] ?? optional($item)->category->translation->name ?? null }}">

            <select class="form-control objects-select select2 categories-select"
                    title="Category"
                    name="step1[new_business][category][id]"
                    aria-describedby="business-error"
                    aria-invalid="false"
                    data-objects-route="{{ route('admin.location-wizard.load-categories') }}">
                <option value="">@lang('wizard.step1.choose_a_category')</option>
                @if($category_id = $step1['new_business']['category']['id'] ?? optional($item)->category->translation->id ?? false)
                    <option selected value="{{ $category_id }}">{{ $step1['new_business']['category']['name'] ?? optional($item)->category->translation->name }}</option>
                @endif
            </select>
            <span class="form-text text-muted">@lang('wizard.step1.category_help_text')</span>
        </div>
    </div>
    <div class="col-xl-6">
        <div class="form-group">
            <label>@lang('wizard.step1.part_of_retail_chain')</label>
            <div class="d-block mt-2">
                <span class="kt-switch kt-switch--icon">
                    <label>
                        <input type="checkbox" class="is-retail-chain" name="step1[new_business][chain]"
                                {{ ($step1['new_business']['chain'] ?? optional($item)->is_chain ?? false) ? 'checked' : '' }}>
                        <span></span>
                    </label>
                </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-6">
        <div class="form-group">
            <label for="promoted_description">{{ __('wizard.step1.description') }}</label>
            <textarea maxlength="250" rows="5" id="promoted_description" class="form-control" name="step1[new_business][promoted_description]">{{ $description->content ?? '' }}</textarea>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-6">
        <div class="form-group">
            <label>Logo</label>
            <input type="hidden" class="dropzone-logo-url" name="step1[new_business][logo][url]"
                   value="{{ $step1['new_business']['logo']['url'] ?? optional($item->logo ?? null)->ready_url ?? null }}">
            <input type="hidden" class="dropzone-logo-name" name="step1[new_business][logo][name]" value="{{ $step1['new_business']['logo']['name'] ?? optional($item->logo ?? null)->name ?? null }}">
            <input type="hidden" class="dropzone-logo-type" name="step1[new_business][logo][type]" value="{{ $step1['new_business']['logo']['type'] ?? optional($item->logo ?? null)->type  ?? null }}">
            <input type="hidden" class="dropzone-logo-size" name="step1[new_business][logo][size]" value="{{ $step1['new_business']['logo']['size'] ?? null }}">
            <div class="dropzone dropzone-logo dropzone-default dropzone-success dz-clickable" data-accepted="image/*" data-name="step1[new_business][logo][file]"
                 data-url="{{ $step1['new_business']['logo']['url'] ?? optional($item->logo ?? null)->ready_url ?? ''}}">
                <div class="dropzone-msg dz-message needsclick">
                    <h3 class="dropzone-msg-title">@lang('wizard.step1.drop_your_logo_here')</h3>
                    <span class="dropzone-msg-desc">@lang('wizard.step1.drop_your_logo_here_help_text')</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6">
        <div class="form-group">
            <label>Favicon</label>
            <input type="hidden" class="dropzone-favicon-url" name="step1[new_business][favicon][url]"
                   value="{{ $step1['new_business']['favicon']['url'] ?? optional($item->favicon ?? null)->ready_url ?? null }}">
            <input type="hidden" class="dropzone-favicon-name" name="step1[new_business][favicon][name]"
                   value="{{ $step1['new_business']['favicon']['name'] ?? optional($item->favicon ?? null)->name ?? null }}">
            <input type="hidden" class="dropzone-favicon-type" name="step1[new_business][favicon][type]"
                   value="{{ $step1['new_business']['favicon']['type'] ?? optional($item->favicon ?? null)->type ?? null }}">
            <input type="hidden" class="dropzone-favicon-size" name="step1[new_business][favicon][size]" value="{{ $step1['new_business']['favicon']['size'] ?? null }}">
            <div class="dropzone dropzone-favicon dropzone-default dropzone-success dz-clickable" data-accepted="image/*" data-name="step1[new_business][favicon][file]"
                 data-url="{{ $step1['new_business']['favicon']['url'] ?? optional($item->favicon ?? null)->ready_url ?? '' }}">
                <div class="dropzone-msg dz-message needsclick">
                    <h3 class="dropzone-msg-title">@lang('wizard.step1.drop_your_favicon_here')</h3>
                    <span class="dropzone-msg-desc">@lang('wizard.step1.drop_your_favicon_here_help_text')</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <label>Facebook</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="la la-facebook"></i></span>
                </div>
                <input type="text" class="form-control social-link facebook" name="step1[new_business][social_profiles][facebook]" placeholder="Facebook profile" aria-describedby="basic-addon1"
                       value="{{ $step1['new_business']['social_profiles']['facebook'] ?? optional($item)->social_profiles->facebook ?? ''}}">
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <label>Instagram</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="la la-instagram"></i></span>
                </div>
                <input type="text" class="form-control social-link instagram" placeholder="Instagram profile" name="step1[new_business][social_profiles][instagram]" aria-describedby="basic-addon1"
                       value="{{ $step1['new_business']['social_profiles']['instagram'] ?? optional($item)->social_profiles->instagram ?? ''}}">
            </div>
        </div>
    </div>
</div>

<div class="accordion accordion-light accordion-toggle-arrow" id="more-social-accordion">
    <div class="card">
        <div class="card-header" id="more-social-heading">
            <div class="card-title collapsed" data-toggle="collapse" data-target="#more-social" aria-expanded="false" aria-controls="more-social">
                @lang('wizard.step1.add_additional_social_networks')
            </div>
        </div>
        <div id="more-social" class="collapse" aria-labelledby="more-social-heading" data-parent="#more-social-accordion" style="">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Twitter</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="la la-twitter"></i></span>
                                </div>
                                <input type="text" class="form-control social-link twitter" name="step1[new_business][social_profiles][twitter]" placeholder="Twitter profile url"
                                       aria-describedby="basic-addon1"
                                       value="{{ $step1['new_business']['social_profiles']['twitter'] ?? optional($item)->social_profiles->twitter ?? ''}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Pinterest</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="la la-pinterest"></i></span>
                                </div>
                                <input type="text" class="form-control social-link pinterest" placeholder="Pinterest profile" name="step1[new_business][social_profiles][pinterest]"
                                       aria-describedby="basic-addon1"
                                       value="{{ $step1['new_business']['social_profiles']['pinterest'] ?? optional($item)->social_profiles->pinterest ?? ''}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Linkedin</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="la la-linkedin"></i></span>
                                </div>
                                <input type="text" class="form-control social-link linkedin" name="step1[new_business][social_profiles][linkedin]" placeholder="Linkedin profile"
                                       aria-describedby="basic-addon1"
                                       value="{{ $step1['new_business']['social_profiles']['linkedin'] ?? optional($item)->social_profiles->linkedin ?? ''}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Foursquare</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="la la-foursquare"></i></span>
                                </div>
                                <input type="text" class="form-control social-link foursquare" placeholder="Foursquare profile" name="step1[new_business][social_profiles][foursquare]"
                                       aria-describedby="basic-addon1"
                                       value="{{ $step1['new_business']['social_profiles']['foursquare'] ?? optional($item)->social_profiles->foursquare ?? ''}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
