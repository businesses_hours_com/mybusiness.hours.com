<div class="kt-wizard-v4__content step1"
     data-ktwizard-type="step-content"
     data-step="1"
     data-manage-btn="{{ ($step1['locations'] ?? false) ? 1 : 0 }}"
     data-scrape-icons-route="{{ route('admin.scraper') }}">

    <div class="kt-heading kt-heading--lg">@lang('wizard.step1.title')</div>
    <div class="kt-form__section kt-form__section--first">
        <div class="kt-wizard-v4__form">

            <div class="business-name-input-wrap">
                @include('admin.location-wizard.step1._includes.business-name-input')
            </div>

            <div class="select-business-wrap" style="display:none">
                @include('admin.location-wizard.step1._includes.select-business')
            </div>

            <div class="add-business-wrap" style="display:none">
                @include('admin.location-wizard.step1._includes.add-business')
            </div>

            <div class="select-locations-wrap" style="display:none">
                @include('admin.location-wizard.step1._includes.select-locations')
            </div>

        </div>
    </div>
</div>
