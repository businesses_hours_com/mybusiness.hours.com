@extends('admin._layouts.admin')

@section('title', __('dashboard.businesses'))

@section('breadcrumbs')
    <a href="{{ route('admin.businesses.index') }}" class="kt-subheader__breadcrumbs-link">
        @lang('dashboard.businesses')
    </a>
@endsection

@section('content')
    @component('admin._includes.index-table')
        <thead>
        <tr class="header-tr mb-2">
            <th class="pb-3"></th>
            <th class="pb-3">@lang('dashboard.name')</th>
            <th class="pb-3">@lang('dashboard.owner')</th>
            <th class="pb-3">@lang('wizard.step1.category')</th>
            <th class="pb-3">@lang('dashboard.locations')</th>
            <th class="pb-3">@lang('wizard.step1.website')</th>
            <th class="pb-3">@lang('dashboard.country')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr data-id="{{ $item->id }}">
                <td class="text-center">{{ $loop->index+1 }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ optional($item->owner)->name }}</td>
                <td>{{ optional($item->category->translation ?? null)->name }}</td>
                <td>{{ count($item->published_locations ?? []) }}</td>
                <td>{{ $item->website }}</td>
                <td>{{ $item->country_code }}</td>
                <td class="px-2 text-right">
                    @include('admin._includes.row-actions', [
                        'editRoute' => route('admin.businesses.edit', $item->id),
                        'addLocationRoute' => route('admin.location-wizard.add-location', $item->id)
                    ])
                </td>
            </tr>
        @endforeach
        @if(! $items->count())
            <tr>
                <td colspan="8">
                    @lang('dashboard.no_businesses_found')
                </td>
            </tr>
        @endif
        </tbody>
    @endcomponent
@endsection
