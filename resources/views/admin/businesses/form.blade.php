@extends('admin._layouts.admin')

@section('title', $item->name)

@section('breadcrumbs')
    <a href="{{ route('admin.businesses.index') }}" class="kt-subheader__breadcrumbs-link">@lang('dashboard.business')</a>
    <span class="kt-subheader__breadcrumbs-separator"></span>
    <a href="{{ route('admin.businesses.edit', $item->id) }}" class="kt-subheader__breadcrumbs-link">
        {{ $item->name }}
    </a>
@endsection

@section('content')
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    {{ $item->name }}
                </h3>
            </div>
        </div>
        <form class="kt-form" method="POST" action="{{ route('admin.businesses.update', $item->id) }}">
            @method('PUT')
            @csrf

            <div class="kt-portlet__body business-edit-form step1" data-scrape-icons-route="{{ route('admin.scraper') }}">
                @include('admin.location-wizard.step1._includes.add-business', [
                    'showTitle' => false,
                ])
                <div class="d-flex flex-row mt-4 align-content-between">
                    <a href="{{ route('admin.businesses.index') }}" class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bolder kt-font-transform-u btn-manage mr-auto">@lang('dashboard.back')</a>
                    <button type="submit" class="btn btn-success btn-md btn-tall btn-wide kt-font-bolder kt-font-transform-u btn-manage ml-auto" data-old-name="{{ $item->name }}" data-times-forwarded-this-year="{{ $item->times_forwarded_this_year }}">
                        @lang('dashboard.update')
                    </button>
                </div>
            </div>

        </form>
    </div>
@endsection
