@extends('admin._layouts.admin')

@section('title', __('dashboard.questions'))

@section('breadcrumbs')
    <a href="{{ route('admin.questions.index') }}" class="kt-subheader__breadcrumbs-link">@lang('dashboard.questions')</a>
@endsection

@section('content')
    @foreach($businesses as $businessId => $businessName)
        <section class="mb-5">
            <div class="d-flex flex-row">
                <h4 class="ml-0 mr-auto btn-font-dark">{{ $businessName }}</h4>
                @if(Auth::user()->canAccessBusiness($businessId))
                    <a class="mb-2 btn btn-primary btn-font-sm btn-sm btn-thin kt-font-transform-u" href="{{ route('admin.businesses.edit', $businessId) }}">
                        <i class="fa fa-fw fa-edit"></i>@lang('dashboard.edit')
                    </a>
                @endif
            </div>

            @component('admin._includes.index-table')
                <thead>
                <tr class="header-tr mb-2">
                    <th class="pb-3">@lang('dashboard.location')</th>
                    <th class="pb-3">@lang('dashboard.user_name')</th>
                    <th class="pb-3">@lang('dashboard.email')</th>
                    <th class="pb-3">@lang('dashboard.votes')</th>
                    <th class="pb-3">@lang('dashboard.text')</th>
                    <th class="pb-3">@lang('dashboard.reported')</th>
                    <th class="pb-3">@lang('dashboard.date')</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if($items->has($businessName))
                    @foreach($items->get($businessName) as $location)
                        @foreach($location->location_hash->questions as $question)
                            <tr data-id="{{ $location->id }}">
                                <td>{{ optional($location->address)->full_address }}</td>
                                <td>{{ $question->user_name }}</td>
                                <td>{{ $question->user_email }}</td>
                                <td>{{ $question->total_votes }}</td>
                                <td style="max-width: 300px;">{{ Illuminate\Support\Str::limit($question->text, 75) }}</td>
                                <td>{{ optional($question->report)->text ?? 'no' }}</td>
                                <td>{{ optional(\Carbon\Carbon::parse($question->updated_at))->format('d/m/Y') }}</td>
                                <td class="px-2 text-right">
                                    @include('admin._includes.row-actions', [
                                        'answersNumber' => count($question->answers),
                                        'answersRoute' => route('admin.questions.answers.index', $question->id),
                                        'destroyRoute' => route('admin.questions.destroy', $question->id),
                                        'reportRoute' =>route('admin.questions.report', $question->id),
                                        'isAppropriate' => !is_null($question->appropriate_at)
                                    ])
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                @else
                    <tr>
                        <td colspan="8">
                            @lang('dashboard.no_questions_found')
                        </td>
                    </tr>
                @endif
                </tbody>
            @endcomponent

        </section>
    @endforeach

    <div class="text-right mt-5 pt-3">
        <a class="btn btn-primary btn-thin kt-font-transform-u ajax-verified-faq" href="{{ route('admin.questions.verified-faq') }}">
            <i class="la la-question-circle"></i> @lang('dashboard.add_faq')
        </a>
    </div>
@endsection
