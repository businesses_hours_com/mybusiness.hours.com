@extends('admin._layouts.admin')

@section('title', __('dashboard.title'))

@section('content')

    <div class="row">
        <div class="col-xl-6">
            @include('admin.dashboard._includes.list-widget', [
                'title' => __('dashboard.businesses'),
                'items' => $businesses,
                'routeName' => 'businesses',
           ])
        </div>
        <div class="col-xl-6">
            @include('admin.dashboard._includes.list-widget', [
                'title' => __('dashboard.locations'),
                'items' => $locations,
                'routeName' => 'locations',
                'nameAttribute' => 'full_address'
            ])
        </div>
    </div>

@endsection