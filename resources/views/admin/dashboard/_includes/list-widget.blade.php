<div class="kt-portlet kt-portlet--height-fluid">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label w-100">
            <h3 class="kt-portlet__head-title d-flex align-items-center w-100 justify-content-between">
                <span>{{ $title }}</span>
                <a class="small text-right" href="{{ route('admin.'.$routeName.'.index') }}">
                    @lang('dashboard.view_all') ({{ $items->count() }})
                </a>
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="tab-content">
            <div class="tab-pane active" id="kt_widget5_tab1_content" aria-expanded="true">
                <div class="kt-widget5">
                    @foreach($items as $item)
                        <div class="kt-widget5__item">
                            <div class="kt-widget5__content">
                                <div class="kt-widget5__pic">
                                    <img class="kt-widget7__img" src="{{ asset('/theme/dist/assets/media/products/product27.jpg') }}" alt="{{ $item->name ?? optional($item->address)->full_address }}">
                                </div>
                                <div class="kt-widget5__section  d-flex flex-column">
                                    {{--<a href="{{ route('admin.'.$routeName.'.edit', $item->id) }}" class="kt-widget5__title mb-3">--}}
                                    <span class="kt-widget5__title mb-3">
                                        {{ $item->name ?? optional($item->address)->full_address }}
                                    </span>
                                    {{--</a>--}}
                                    <div class="kt-widget5__info">
                                        <div class="">
                                            <span>@lang('dashboard.owner'):</span>
                                            <span class="kt-font-bold">
                                                {{ $item->owner->name ?? $item->users[0]->name ?? ''}}
                                            </span>
                                        </div>
                                        <div class="">
                                            <span>@lang('dashboard.added'):</span>
                                            <span class="kt-font-bold">
                                                {{ optional(\Carbon\Carbon::parse($item->created_at))->format('d/m/Y') }}
                                            </span>
                                        </div>
                                        @if($routeName === 'businesses')
                                            <div class="kt-widget5__mobile-locations">
                                                @lang('dashboard.locations'):
                                                <span class="kt-font-bold">
                                               {{ count($item->published_locations ?? []) }}
                                            </span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="kt-widget5__content">
                                <div class="kt-widget5__stats">
                                    @if($routeName === 'businesses')
                                        <span class="kt-widget5__number">
                                            {{ count($item->published_locations ?? []) }}
                                        </span>
                                        <span class="kt-widget5__sales">{{ strtolower(__('dashboard.locations')) }}</span>
                                    @endif

                                </div>
                            </div>

                        </div>
                    @endforeach

                </div>

            </div>
        </div>
    </div>
</div>
