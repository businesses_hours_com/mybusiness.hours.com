@extends('admin._layouts.admin')

@section('title', __('dashboard.answers'))

@section('breadcrumbs')
    <a href="{{ route('admin.questions.index') }}" class="kt-subheader__breadcrumbs-link">@lang('dashboard.questions')</a>
    <span class="kt-subheader__breadcrumbs-separator"></span>
    <a href="{{ route('admin.questions.answers.index', $question->id) }}" class="kt-subheader__breadcrumbs-link">@lang('dashboard.answers')</a>
@endsection

@section('content')

    <div class="card-header bg-white mb-3">
        <h4>@lang('dashboard.question')</h4>
        <p>{{ $question->text }}</p>
    </div>

    @component('admin._includes.index-table')
        <thead>
        <tr class="header-tr mb-2">
            <th class="pb-3">#</th>
            <th class="pb-3">@lang('dashboard.user_name')</th>
            <th class="pb-3">@lang('dashboard.email')</th>
            <th class="pb-3">@lang('dashboard.votes')</th>
            <th class="pb-3">@lang('dashboard.reported')</th>
            <th class="pb-3">@lang('dashboard.date')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr data-id="{{ $item->id }}">
                <td>{{ $item->id }}</td>
                <td>{{ $item->user_name }}</td>
                <td>{{ $item->user_email }}</td>
                <td>{{ $item->total_votes }}</td>
                <td>{{ optional($item->report)->text ?? 'no' }}</td>
                <td>{{ optional(\Carbon\Carbon::parse($item->updated_at))->format('d/m/Y') }}</td>
                <td class="px-2 text-right">
                    @include('admin._includes.row-actions', [
                        'editRoute' => route('admin.questions.answers.edit', [$question->id, $item->id]),
                        'destroyRoute' => route('admin.questions.answers.destroy', [$question->id, $item->id]),
                        'reportRoute' => route('admin.questions.answers.report', [$question->id, $item->id]),
                        'isAppropriate' => !is_null($item->appropriate_at)
                    ])
                </td>
            </tr>
        @endforeach
        @if(! $items->count())
            <tr>
                <td colspan="8">
                    @lang('dashboard.no_answers_found')
                </td>
            </tr>
        @endif
        </tbody>
    @endcomponent

    <div class="">
        <div class="text-right mb-1">
            <a href="{{ route('admin.questions.answers.create', $question->id) }}"
               class='btn btn-primary'>
                <i class="fa fa-fw fa-plus-circle"></i>@lang('dashboard.add_answer')
            </a>
        </div>
    </div>
@endsection
