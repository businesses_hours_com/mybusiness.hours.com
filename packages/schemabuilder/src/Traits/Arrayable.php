<?php

namespace Locally\SchemaBuilder\Traits;

trait Arrayable
{
    /**
     * Convert the object to its Array representation.
     *
     * @param  int  $options
     * @return array
     */
    public function toArray()
    {
        $data = [];

        foreach ($this->data as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    $this->setArrayableNestedArrayValue($data[$key], $v);
                }
            } else {
                $data[$key] = $this->isArrayableItem($value) ? $value->toArray() : $value;
            }

            // Add checksums for first level arrays/objects.
            if (property_exists($this, 'addChecksum') && $this->addChecksum) {
                $this->addChecksum($key, $data);
            }
        }

        return $data;
    }

    /**
     * Note: Use "name" attribute for indexed arrays is present.
     *
     * @param $data
     * @param $value
     */
    protected function setArrayableNestedArrayValue(&$data, $value)
    {
        $val = $this->isArrayableItem($value) ? $value->toArray() : $value;

        if (is_array($val) && isset($val['name'])) {
            $nameKey = $val['name'];
            unset($val['name']);
            $data[$nameKey] = $val;
        } else {
            $data[] = $val;
        }
    }

    /**
     * @param $item
     *
     * @return bool
     */
    protected function isArrayableItem($item)
    {
        return is_object($item) && method_exists($item, 'toArray');
    }

    /**
     * Add checksum for a given object/array (via key / data).
     *
     * @param $key
     * @param $data
     */
    protected function addChecksum($key, &$data)
    {
        if ($this->addChecksum && (is_array($data[$key]) || is_object($data[$key]))) {
            $data["{$key}_checksum"] = $this->calculateValueChecksum($data[$key]);
        }
    }

    /**
     * Checkum calculator for object|array values.
     * TODO: Check if sha256 is "enough" (do we need some custom parsing/encrypting?) or even already overkill (sha1?).
     *
     * @param null $value
     *
     * @return string
     */
    protected function calculateValueChecksum($value = null)
    {
        return hash('sha256', json_encode($value));
    }
}
