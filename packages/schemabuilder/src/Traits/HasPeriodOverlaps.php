<?php

namespace Locally\SchemaBuilder\Traits;

trait HasPeriodOverlaps
{
    /**
     * @param      $day
     * @param      $dayType
     * @param null $ref
     *
     * @throws \ErrorException
     */
    public function checkDayOverlap($day, $dayType, $ref = null)
    {
        $ref = $ref ?: $this;

        if (is_array($ref->$day)) foreach ($ref->$day as $range) {
            if ($dayType->hasOverlap($range)) {
                throw new \ErrorException("Overlap (inner-, left-, right-, full-overlap) not allowed:
                    {$range->toString()} => {$dayType->toString()}");
            }
        }
    }

    /**
     * @param      $day
     * @param      $dayType
     * @param null $ref
     *
     * @throws \ErrorException
     */
    public function checkPeriodOnlyInnerDateOverlap($periodName, $periodType, $ref = [])
    {
        if(count($ref)) foreach ($ref as $range) {
            if ($periodType->hasOverlap($range->getPeriodType()) &&
                !$periodType->hasInnerOverlap($range->getPeriodType())
            ) {
                throw new \ErrorException("Overlap (left-, right-, full-overlap) not allowed, only inner:
                    {$range->getPeriodType()->toString()} => {$periodType->toString()}");
            }
        }
    }

    /**
     * @param $dayVal
     */
    public function sortDayValues(&$dayVal)
    {
        $ord = array();
        foreach ($dayVal as $key => $value){
            $ord[] = $value->getFrom()->getTimestamp();
        }
        array_multisort($ord, SORT_ASC, $dayVal);
    }
}