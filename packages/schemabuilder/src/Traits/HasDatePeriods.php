<?php

namespace Locally\SchemaBuilder\Traits;

trait HasDatePeriods
{
    /**
     * @var \DateTime|null
     */
    protected $fromDate;

    /**
     * @var \DateTime|null
     */
    protected $toDate;

    /**
     * @return \DateTime|null
     */
    public function getFrom()
    {
        return $this->fromDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getTo()
    {
        return $this->toDate;
    }

    /**
     * @param $date
     *
     * @return bool
     */
    public function hasInnerOverlap($date)
    {
        return
            $this->fromDate >= $date->getFrom() &&
            $this->fromDate <= $date->getTo()   &&
            $this->toDate   <= $date->getTo();
    }

    /**
     * @param $date
     *
     * @return bool
     */
    public function hasLeftOverlap($date)
    {
        return
            $this->fromDate <  $date->getFrom() &&
            $this->toDate   >= $date->getFrom() &&
            $this->toDate   <= $date->getTo();
    }

    /**
     * @param $date
     *
     * @return bool
     */
    public function hasRightOverlap($date)
    {
        return
            $this->fromDate >= $date->getFrom() &&
            $this->fromDate <= $date->getTo()   &&
            $this->toDate   >  $date->getTo();
    }

    /**
     * @param $date
     *
     * @return bool
     */
    public function hasFullOverlap($date)
    {
        return
            $this->fromDate < $date->getFrom() &&
            $this->toDate   > $date->getTo();
    }

    /**
     * @param $date
     *
     * @return bool
     */
    public function hasOverlap($date)
    {
        return
            $this->hasInnerOverlap($date) ||
            $this->hasLeftOverlap($date)  ||
            $this->hasRightOverlap($date) ||
            $this->hasFullOverlap($date);
    }
}