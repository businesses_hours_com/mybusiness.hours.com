<?php

namespace Locally\SchemaBuilder\Traits;

trait HasTimezone
{
    /**
     * @var string
     */
    protected $timezone;

    /**
     * @var string
     */
    protected $fallbackTimezone = 'Europe/Amsterdam';

    /**
     * @param null $timezone
     *
     * @return $this
     * @throws \ErrorException
     */
    public function setTimezone($timezone = null)
    {
        // If not $timezone is passed, try to fetch from $_ENV (e.g. from parent object)
        if (empty($timezone)) {
            $timezone = $_ENV['establishment_timezone'] ?? $this->fallbackTimezone;
        }

        if (!$this->isValidTimezone($timezone)) {
            throw new \ErrorException("Invalid timezone ('{$timezone}').");
        }

        $this->timezone = $timezone;

        // are we supposed to add it to data as well (see schema)?
        if (isset($this->schema) && is_array($this->schema) && isset($this->schema['timezone'])) {
            if (!$this->data) {
                $this->data = [];
            }

            $this->data['timezone'] = $timezone;
        }

        return $this;
    }

    /**
     * Check if a string is a valid timezone
     *
     * @param $timezone
     *
     * @return bool
     */
    protected function isValidTimezone($timezone) {
        return in_array($timezone, timezone_identifiers_list());
    }
}