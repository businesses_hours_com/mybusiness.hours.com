<?php

namespace Locally\SchemaBuilder\Traits;

trait Jsonable
{
    use Arrayable;

    /**
     * Convert the object to its JSON representation.
     *
     * @param  int  $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }
}