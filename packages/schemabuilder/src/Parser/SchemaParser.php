<?php

namespace Locally\SchemaBuilder\Parser;

use Exception;
use Locally\SchemaBuilder\Schema\DayType\FullDayClosed;
use Locally\SchemaBuilder\Schema\DayType\FullDayOpen;
use Locally\SchemaBuilder\Schema\PeriodType\DateRange;
use Locally\SchemaBuilder\Schema\PeriodType\MonthRange;
use Locally\SchemaBuilder\Schema\PeriodType\WeekRange;
use Locally\SchemaBuilder\Traits\HasTimezone;
use Locally\SchemaBuilder\Traits\Jsonable;
use Yasumi\Yasumi;

class SchemaParser
{
    use Jsonable, HasTimezone;

    /**
     * @var \stdClass
     */
    protected $data;

    /**
     * @var string
     */
    protected $timezone;

    /**
     * @var string
     */
    protected $date;

    /**
     * @var string
     */
    protected $time;

    /**
     * @var false|int
     */
    protected $monday;

    /**
     * @var false|int
     */
    protected $sunday;

    /**
     * @var string
     */
    protected $dateRegex = '/^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]$/';

    /**
     * @var string
     */
    protected $timeRegex = '/^([0-9]|0[0-9]|1[0-9]|2[0-4]):[0-5][0-9]$/';

    /**
     * @var string
     */
    protected $defaultTimezone = 'Europe/Amsterdam';

    /**
     * SchemaParser constructor.
     *
     * @param      $data
     * @param null $timezone
     * @param null $date
     * @param null $time
     *
     * @throws \ErrorException
     */
    public function __construct($data, $timezone = null, $date = null, $time = null)
    {
        $this->setTimezone($timezone ?: $this->defaultTimezone);

        $this->date = !empty($date)
            ? $this->checkDateFormat($date, $this->dateRegex)
            : (new \DateTime('now', new \DateTimeZone($this->timezone)))->format('Y-m-d');

        $this->time = !empty($time)
            ? $this->checkDateFormat($time, $this->timeRegex)
            : (new \DateTime('now', new \DateTimeZone($this->timezone)))->format('H:i');


        // calculate monday & sunday timestamp;
        $this->setWeekDays();

        $this->data = $this->parseData($data);
    }

    /**
     * Sets monday and sunday, usable throughout this class.
     */
    protected function setWeekDays()
    {
        $currentDateTime = new \DateTime($this->date, new \DateTimeZone($this->timezone));
        $today = strtotime('today', $currentDateTime->getTimestamp());

        $prevMonday = strtotime('previous monday', $currentDateTime->getTimestamp());
        $nextMonday = strtotime('next monday', $currentDateTime->getTimestamp());

        $this->monday = $today === $nextMonday ? $nextMonday : $prevMonday;
        $this->sunday = strtotime('next sunday +1 day -1 second', $currentDateTime->getTimestamp());
    }

    /**
     * @param $input
     *
     * @return \stdClass
     * @throws \ErrorException
     */
    protected function parseData($input)
    {
        $parsed = json_decode($input);

        if (($parsed === null && json_last_error() !== JSON_ERROR_NONE) || !is_object($parsed)) {
            throw new \ErrorException('Supplied json data is incorrect or invalid.');
        } elseif(!isset($parsed->timezone) || !$this->isValidTimezone($parsed->timezone) || !isset($parsed->hours)) {
            throw new \ErrorException('Supplied json data must contain valid timezone and hours.');
        }

        $data = new \stdClass();
        $data->timezone = $parsed->timezone;
        $data->hours = [];

        // add timezone to ENV cache
        $_ENV['establishment_timezone'] = $data->timezone;

        foreach ($parsed->hours as $department) {
            $data->hours[] = $this->parseDepartment($department, $data->timezone);
        }

        return $data;
    }

    /**
     * Multistep process:
     * - First just get the correct hours per department, in the correct output.
     * - Then (per department), check/calculate open_now/untill_open/untill_close.
     *
     * @param $department
     * @param $timezone
     *
     * @return object
     */
    protected function parseDepartment($department, $timezone)
    {
        $hours = $this->parsePeriods($department->periods, $timezone);

        // holiday overrides?
        if (isset($department->holidays)) {
            $this->parseAndAddHolidays(
                $department->holidays,
                $hours,
                $department->country ?? 'Netherlands',
                $department->locale ?? 'nl_NL'
            );
        }

        // exception overrides?
        if (isset($department->exceptions)) {
            $this->parseAndAddExceptions($department->exceptions, $hours);
        }

        return (object) [
            'department'  => $department->department,
            'hours'       => $hours,
            'open_now'    => $this->isOpenNow($hours, $timezone),
            'until_open'  => $this->minutesUntilOpen($hours, $timezone),  // |15m  // TODO.
            'until_close' => $this->minutesUntilClose($hours, $timezone), // |120m // TODO.
        ];
    }

    /**
     * Multistage process:
     * - First check for "default" period.
     * - Then process all/any remaining periods whether they should overrule the "default" day
     *   -> This is when this week falls in the period && the period "is shorter" then "the currently used period"
     *
     * @param $periods
     * @param $timezone
     *
     * @return array|\stdClass
     */
    protected function parsePeriods($periods, $timezone)
    {
        $days = [];

        // first add the 'default' period
        if (isset($periods->default)) {
            $days = $this->parsePeriod('default', $periods->default, $timezone);
            unset($periods->default);
        }

        // next up: check all other periods if they should overrule the days.
        $lengthInDays = 366;

        // loop all remaining periods
        foreach ($periods as $period => $hours) {
            // parse period into correct range
            $periodAttributes = explode('/', $period);

            // week range?
            if (strpos($periodAttributes[0], 'week') === 0) {
                $periodFrom = str_replace('week ', '', $periodAttributes[0]);
                $periodTo = $periodAttributes[1];
                try {
                    $range = new WeekRange($periodFrom, $periodTo);

                    if ($this->isPeriodOverrule($range, $lengthInDays)) {
                        $days = $this->parsePeriod($period, $hours, $timezone);
                        $lengthInDays = $range->getLengthInDays();
                    }
                } catch (Exception $e) { /* no-op */ }
            }

            // date range?
            try {
                $range = new DateRange($periodAttributes[0], $periodAttributes[1]);

                if ($this->isPeriodOverrule($range, $lengthInDays)) {
                    $days = $this->parsePeriod($period, $hours, $timezone);
                    $lengthInDays = $range->getLengthInDays();
                }
            } catch (Exception $e) { /* no-op */ }

            // month range?
            try {
                $range = new MonthRange($periodAttributes[0], $periodAttributes[1]);

                if ($this->isPeriodOverrule($range, $lengthInDays)) {
                    $days = $this->parsePeriod($period, $hours, $timezone);
                    $lengthInDays = $range->getLengthInDays();
                }
            } catch (Exception $e) { /* no-op */ }
        }

        return $days;
    }

    /**
     * @param        $holidays
     * @param        $hours
     * @param string $country
     * @param string $locale
     */
    public function parseAndAddHolidays($holidays, &$hours, $country = 'Netherlands', $locale = 'nl_NL')
    {
        $currentDateTime = new \DateTime($this->date, new \DateTimeZone($this->timezone));
        $provider = Yasumi::create($country, $currentDateTime->format('Y'), $locale);

        foreach ($holidays as $holiday => $range) {
            try {
                if (($holiday = $provider->getHoliday($holiday)) &&
                    ($holiday->getTimestamp() >= $this->monday && $holiday->getTimestamp() <= $this->sunday)
                ) {
                    $hours->{$date->format('N')} = $range;
                }
            } catch (Exception $e) { /* no-op */ }
        }
    }

    /**
     * @param $exceptions
     * @param $hours
     */
    public function parseAndAddExceptions($exceptions, &$hours)
    {
        foreach ($exceptions as $exception => $range) {
            $date = new \DateTime("{$exception} 00:00", new \DateTimeZone($this->timezone));

            if ($date->getTimestamp() >= $this->monday && $date->getTimestamp() <= $this->sunday) {
                $hours->{$date->format('N')} = $range;
            }
        }
    }

    /**
     * @param $period
     * @param $hours
     * @param $timezone
     *
     * @return \stdClass
     */
    protected function parsePeriod($period, $hours, $timezone)
    {
        $days = new \stdClass();

        // add mon-sat.
        for ($i = 1; $i < 7; $i++) {
            if (isset($hours->$i)) {
                $days->$i = $hours->$i;
            } else {
                $days->$i = [(new FullDayClosed())->toString()];
            }
        }

        // add sunday.
        $days->{'7'} = $this->getPeriodSunday($hours, $timezone);

        return $days;
    }

    /**
     * @param $range
     * @param $lengthInDays
     *
     * @return bool
     */
    protected function isPeriodOverrule($range, $lengthInDays)
    {
        return
            ($this->monday >= $range->getFrom()->getTimestamp() && $this->monday <= $range->getTo()->getTimestamp()) &&
            ($this->sunday >= $range->getFrom()->getTimestamp() && $this->sunday <= $range->getTo()->getTimestamp()) &&
            $range->getLengthInDays() <= $lengthInDays;
    }

    /**
     * @param $period
     * @param $timezone
     *
     * @return string
     */
    protected function getPeriodSunday($period, $timezone)
    {
        $currentDateTime = new \DateTime($this->date, new \DateTimeZone($this->timezone));

        $nextSunday = strtotime('next sunday', $currentDateTime->getTimestamp());
        $firstSunday = strtotime('first sunday of this month', $currentDateTime->getTimestamp());
        $lastSunday = strtotime('last sunday of this month', $currentDateTime->getTimestamp());

        $weekNoNextSunday = (int)date('W', $nextSunday);
        $weekNoFirstSunday = (int)date('W', $firstSunday);
        $weekNoLastSunday = (int)date('W', $lastSunday);

        // use last sunday?
        if (isset($period->sunday_last) &&
            $weekNoNextSunday === $weekNoLastSunday
        ) {
            return $period->sunday_last;
        }
        // use first sunday?
        elseif (isset($period->sunday_first) &&
            ($weekNoNextSunday === $weekNoFirstSunday || $weekNoNextSunday === ($weekNoFirstSunday + 1))
        ) {
            return $period->sunday_first;
        }
        // use regular sunday?
        elseif (isset($period->sunday) ) {
            return $period->sunday;
        }

        return [(new FullDayClosed())->toString()];
    }

    /**
     * @param $hours
     * @param $timezone
     *
     * @return int
     */
    protected function isOpenNow($hours, $timezone)
    {
        $dayNo = (int)(new \DateTime($this->date, new \DateTimeZone($this->timezone)))->format('N');

        // just in case: 24h/closed check.
        if ($this->isFullDayClosed($hours->$dayNo)) {
            return 0;
        }
        if ($this->isFullDayOpen($hours->$dayNo)) {
            return 1;
        }

        // timerange
        if (is_array($hours->$dayNo)) {
            $date = \DateTime::createFromFormat('Y-m-d H:i', "{$this->date} {$this->time}", new \DateTimeZone($this->timezone));

            foreach ($hours->$dayNo as $range) {
                $parts = explode('/', $range);

                // 24h/closed check.
                if (count($parts) === 1) {
                    if ($this->isFullDayClosed($parts[0])) {
                        return 0;
                    }
                    if ($this->isFullDayOpen($parts[0])) {
                        return 1;
                    }
                } elseif(count($parts) === 1) {
                    // TODO: $parts[1] can contain "[appointment]"
                    $from = \DateTime::createFromFormat('Y-m-d Hi', "{$this->date} {$parts[0]}", new \DateTimeZone($timezone));
                    $to = \DateTime::createFromFormat('Y-m-d Hi', "{$this->date} {$parts[1]}", new \DateTimeZone($timezone));

                    // only have a return statement on true, otherwise keep looing and/or just continue.
                    if ($date >= $from && $date <= $to) {
                        return 1;
                    }
                }
            }
        }

        return 0;
    }

    /**
     * @param $hours
     * @param $timezone
     *
     * @return int
     */
    protected function minutesUntilOpen($hours, $timezone)
    {
        if ($this->isOpenNow($hours, $timezone)) {
            return 0;
        }

        // TODO

        return 0;
    }

    /**
     * @param $hours
     * @param $timezone
     *
     * @return int
     */
    protected function minutesUntilClose($hours, $timezone)
    {
        if (!$this->isOpenNow($hours, $timezone)) {
            return 0;
        }

        // TODO

        return 0;
    }

    /**
     * @param $time
     *
     * @return string
     * @throws \ErrorException
     */
    protected function checkDateFormat($date, $regex)
    {
        if(!preg_match($regex, $date)) {
            $trace = debug_backtrace();
            $args = json_encode($trace[1]['args']);
            throw new \ErrorException("Used arguments for {$trace[1]['class']} not in correct format.
                Used: {$args}.
                Must match: {$regex}");
        }

        return $date;
    }

    /**
     * @param $item
     *
     * @return bool
     */
    protected function isFullDayClosed($item)
    {
        return $item  === (new FullDayClosed())->toString();
    }

    /**
     * @param $item
     *
     * @return bool
     */
    protected function isFullDayOpen($item)
    {
        return $item === (new FullDayOpen())->toString();
    }
}
