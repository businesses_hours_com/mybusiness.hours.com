<?php

namespace Locally\SchemaBuilder\Schema;

class Address extends SchemaBuilder
{
    /**
     * Address constructor.
     *
     * @param null $addressId
     */
    public function __construct($addressId = null)
    {
        if ($addressId) {
            $this->address_id = $addressId;
        }
    }

    /**
     * @var array
     */
    protected $schema = [
        'address_id'            => 'integer',
        'postal_code'           => 'string',
        'house_number'          => 'integer',
        'house_number_addition' => 'string',
    ];
}