<?php

namespace Locally\SchemaBuilder\Schema;

use Locally\SchemaBuilder\Schema\PeriodType\DefaultPeriod;
use Locally\SchemaBuilder\Traits\HasPeriodOverlaps;
use Locally\SchemaBuilder\Traits\HasTimezone;

class Period extends SchemaBuilder
{
    use HasPeriodOverlaps, HasTimezone;

    /**
     * @var PeriodType instance
     */
    protected $periodType;

    /**
     * @var array
     */
    protected $schema = [
        'name'               => 'string',
        '1'                  => 'array',
        '2'                  => 'array',
        '3'                  => 'array',
        '4'                  => 'array',
        '5'                  => 'array',
        '6'                  => 'array',
        'hint'               => 'string',
        'sunday'             => 'array',
        'sunday_first'       => 'array',
        'sunday_second'      => 'array',
        'sunday_third'       => 'array',
        'sunday_fourth'      => 'array',
        'sunday_second_last' => 'array',
        'sunday_last'        => 'array',
        'sunday_all'         => 'array'
    ];

    /**
     * Period constructor.
     *
     * Allowed input formats:
     * - $type string/null: 'default' / empty
     * - $type object: 'xx', 'xx' (week format)
     * - $type object: 'jan', 'march' (en short month format)
     * - $type object: '2018-01-01', '2018-01-31 (date format)
     *
     * @param null $type
     * @param null $timezone
     *
     * @throws \ErrorException
     */
    public function __construct($type = null, $timezone = null)
    {
        if (is_object($type)) {
            $this->periodType = $type;
        }
        else {
            if (! ($type === 'default' || ! $type)) {
                throw new \ErrorException('No named periods allowed, only default period or Date/Week/Month range');
            }
            // always fall back to default type.
            $this->periodType = new DefaultPeriod();
        }

        // set intance name
        $this->name = $this->periodType->getName();

        // set the timezone
        $this->setTimezone($timezone);
    }

    /**
     * @return PeriodType|DefaultPeriod
     */
    public function getPeriodType()
    {
        return $this->periodType;
    }

    /**
     * @param       $day
     * @param mixed ...$dayTypes
     *
     * @return $this
     * @throws \ErrorException
     */
    public function addDay($day, ...$dayTypes)
    {
        foreach ($dayTypes as $dayType) {
            // check if allowed (overlap AND inner NOT allowed)
            $this->checkDayOverlap($day, $dayType);

            $dayVal   = $this->$day ?: [];
            $dayVal[] = $dayType;

            // sort the new array!
            $this->sortDayValues($dayVal);

            $this->$day = $dayVal;

            // sort days ($data);
            $this->sortDays($this->data);
        }

        return $this;
    }

    public function setSunday(...$data)
    {
        $sunday = "sunday";
        $i      = 0;

        foreach ($data as $item) {
            if ($i === 0 && is_string($item)) {
                $sunday .= "_{$item}";
                $i++;
                continue;
            }

            // check if allowed (overlap AND inner NOT allowed)
            $this->checkDayOverlap($sunday, $item);

            $dayVal   = $this->$sunday ?: [];
            $dayVal[] = $item;

            // sort the new array!
            $this->sortDayValues($dayVal);

            $this->$sunday = $dayVal;

            $i++;
        }

        // sort days ($data);
        $this->sortDays($this->data);

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setHint($value)
    {
        $this->hint = $value;

        return $this;
    }

    /**
     * @param $data
     */
    protected function sortDays(&$data)
    {
        $order = [
            'name'               => 1,
            '1'                  => 2,
            '2'                  => 3,
            '3'                  => 4,
            '4'                  => 5,
            '5'                  => 6,
            '6'                  => 7,
            'sunday'             => 8,
            'sunday_first'       => 9,
            'sunday_second'      => 10,
            'sunday_third'       => 11,
            'sunday_fourth'      => 12,
            'sunday_second_last' => 13,
            'sunday_last'        => 14,
            'sunday_all'         => 15,
        ];

        uksort($data, function($a, $b) use ($order) {
            return $order[$a] < $order[$b] ? -1 : 1;
        });
    }
}
