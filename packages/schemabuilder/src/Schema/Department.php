<?php

namespace Locally\SchemaBuilder\Schema;

use Illuminate\Support\Str;
use Locally\SchemaBuilder\Traits\HasPeriodOverlaps;
use Locally\SchemaBuilder\Traits\HasTimezone;

class Department extends SchemaBuilder
{
    use HasPeriodOverlaps, HasTimezone;

    /**
     * @var array
     */
    protected $schema = [
        'department' => 'string',
        'period'     => 'string',
        'is_main'    => 'boolean',
        'periods'    => 'array',
        'holidays'   => 'object',
        'exceptions' => 'object',
    ];

    /**
     * Department constructor.
     *
     * @param null $name
     * @param null $timezone
     */
    public function __construct($name = null, $timezone = null, $isMain = false)
    {
        if ($name) {
            $this->department = $name;
        }

        $isSeasonal = Str::containsAll($name, ['/', '-']);

        if ($name !== 'default' && ! $isSeasonal) {
            $this->is_main = $isMain;
        }

        if ($isSeasonal) {
            $this->period = $name;
        }

        // set the timezone
        $this->setTimezone($timezone);
    }

    /**
     * @param $key
     *
     * @return Period
     */
    public function period($key)
    {
        // return/find by int-key?
        if ((is_int($key) || $key === null) && $this->periods && isset($this->periods[$key])) {
            return $this->periods[$key];
        }

        // return/find department name
        if (is_string($key) && $this->periods) {
            foreach ($this->periods as $period) {
                if ($period->name === $key) {
                    return $period;
                }
            }
        }
    }

    /**
     * @param Period $period
     *
     * @return $this
     * @throws \ErrorException
     */
    public function addPeriod(Period $period)
    {
        $this->periods = $this->periods ?: [];

        $periodType = $period->getPeriodType();
        $periodName = $periodType->getName();

        // check if allowed (only inner overlap allowed)
        $this->checkPeriodOnlyInnerDateOverlap($periodType->getName(), $periodType, $this->periods);

        $periods              = $this->periods ?: [];
        $periods[$periodName] = $period;

        // sort periods ($periods);
        $this->sortPeriods($periods);

        $this->periods = $periods;

        return $this;
    }

    /**
     * @param $key
     *
     * @return $this
     */
    public function removePeriod($key)
    {
        $periods = $this->periods;

        // return/find by int-key?
        if ((is_int($key) || $key === null) && $periods && isset($periods[$key])) {
            unset($periods[$key]);
            $this->periods = $periods;

            return $this;
        }

        // return/find department name
        if (is_string($key) && $periods) {
            foreach ($periods as $periodKey => $period) {
                unset($periods[$periodKey]);
                $this->periods = $periods;

                return $this;
            }
        }
    }

    /**
     * @param string $holiday
     * @param string $country
     * @param mixed ...$dayTypes
     *
     * @return $this
     * @throws \ErrorException
     */
    public function addHoliday(string $holiday, string $country, ...$dayTypes)
    {
        $holidays = $this->holidays ?: new Holidays();

        $holidays->add($holiday, $country, ...$dayTypes);

        $this->holidays = $holidays;

        return $this;
    }

    /**
     * @param       $key
     * @param mixed ...$dayTypes
     *
     * @return $this
     */
    public function addException($key, ...$dayTypes)
    {
        $exceptions = $this->exceptions ?: new Exceptions();

        $exceptions->add($key, ...$dayTypes);

        $this->exceptions = $exceptions;

        return $this;
    }

    /**
     * @param $periods
     */
    protected function sortPeriods(&$periods)
    {
        uksort($periods, function($a, $b) use ($periods) {
            $aFrom = $periods[$a]->getPeriodType()->getFrom();
            $bFrom = $periods[$b]->getPeriodType()->getFrom();

            if ($aFrom === null) {
                return -1;
            }

            if ($bFrom === null) {
                return 1;
            }

            return $aFrom < $bFrom ? -1 : 1;
        });
    }
}
