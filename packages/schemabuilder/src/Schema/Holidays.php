<?php

namespace Locally\SchemaBuilder\Schema;

use Locally\SchemaBuilder\Traits\HasPeriodOverlaps;
use Locally\SchemaBuilder\Traits\HasTimezone;
use Yasumi\Yasumi;

class Holidays extends SchemaBuilder
{
    use HasPeriodOverlaps, HasTimezone;

    /**
     * Exceptions constructor.
     */
    public function __construct()
    {
        // set the timezone (either from parent $_ENV or from fallback.
        $this->setTimezone();
    }

    /**
     * Override parent __set method; Holidays is allowed to set any attribute
     *
     * @param $name
     * @param $value
     *
     * @return $this
     * @throws \Exception
     */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;

        return $this;
    }

    /**
     * @param string $holiday
     * @param string $country
     * @param mixed ...$dayTypes
     *
     * @return $this
     * @throws \ErrorException
     */
    public function add(string $holiday, string $country, ...$dayTypes)
    {
        // get all holidays for $country
        $holidays = Yasumi::create($country, date('Y'));

        // start parsing only when we have any $dayTypes *and* the holiday is valid for $country.
        if (count($dayTypes) && $this->validateHoliday($holiday, $holidays)) {
            foreach ($dayTypes as $dayType) {
                // check if allowed (overlap AND inner NOT allowed)
                $this->checkDayOverlap($holiday, $dayType);

                $dayVal = $this->$holiday ?: [];
                $dayVal[] = $dayType;

                // sort the new array!
                $this->sortDayValues($dayVal);

                $this->$holiday = $dayVal;

                // sort exceptions
                $this->sortHolidays($this->data);
            }
        }

        return $this;
    }

    /**
     * @param $holiday
     * @param $holidays
     *
     * @return bool
     * @throws \ErrorException
     */
    protected function validateHoliday($holiday, $holidays)
    {
        $valid = $holiday === 'newYearsEve' || in_array($holiday, $holidays->getHolidayNames());

        if (!$valid) {
            $class = (new \ReflectionClass($holidays))->getShortName();
            throw new \ErrorException("'{$holiday}' is not a valid holiday for '$class'.");
        }

        // optionally we can also return the holiday itself using getHoliday($holiday)
        return (bool)$valid;
    }

    /**
     * @param $data
     */
    protected function sortHolidays(&$data)
    {
        ksort($data);
    }
}
