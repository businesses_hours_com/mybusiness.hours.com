<?php

namespace Locally\SchemaBuilder\Schema;

class Contact extends SchemaBuilder
{
    /**
     * @var array
     */
    protected $schema = [
        'email'   => 'string',
        'phone'   => 'string',
        'fax'     => 'string',
        'website' => 'string',
    ];
}