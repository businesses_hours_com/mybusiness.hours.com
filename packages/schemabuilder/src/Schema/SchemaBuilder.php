<?php

namespace Locally\SchemaBuilder\Schema;

use Locally\SchemaBuilder\Traits\Htmlable;
use Locally\SchemaBuilder\Traits\Arrayable;
use Locally\SchemaBuilder\Traits\Jsonable;

class SchemaBuilder
{
    use Jsonable, Htmlable;

    /**
     * To add or not to add checksum for object/array attributes
     *
     * @var bool
     */
    protected $addChecksum = false;

    /**
     * Allowed schema castings.
     *
     * @var array
     */
    private $schemaCastings = [
        'boolean',
        'bool',
        'integer',
        'int',
        'float',
        'double',
        'string',
        'array',
        'object',
        'null',
    ];

    /**
     * Schema options, variableName => casting
     * Casting options see $schemaCastings.
     *
     * @var array
     */
    protected $schema = [];

    /**
     * Data store.
     *
     * @var array
     */
    protected $data = [];

    /**
     * @param $name
     *
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        $function = $this->toCamelCase("get_{$name}_attribute");
        if (method_exists($this, $function)) {
            return $this->$function();
        }

        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
    }

    /**
     * @param $name
     * @param $value
     *
     * @return $this
     * @throws \Exception
     */
    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->schema) && in_array($this->schema[$name], $this->schemaCastings)) {
            $this->validateSetType($name, $value);

            $this->data[$name] = $value;
        } else {
            $trace = debug_backtrace();
            throw new \Exception("Not allowed to set property '{$name}' in {$trace[0]['file']} on line {$trace[0]['line']}");
        }

        return $this;
    }

    /**
     * @param $name
     * @param $value
     *
     * @throws \Exception
     */
    protected function validateSetType($name, $value)
    {
        $type = gettype($value);

        if ($type !== $this->schema[$name] && $type !== 'NULL') {
            $trace = debug_backtrace();
            throw new \Exception("Cannot set '{$name}' as type '{$type}', must be of type '{$this->schema[$name]}' in 
                {$trace[0]['file']} on line {$trace[0]['line']}.");
        }
    }

    /**
     * @param string $string
     * @param string $delimiter
     * @return string
     */
    public function toCamelCase(string $string, $delimiter = '_'): string
    {
        $str = str_replace($delimiter, '', ucwords($string, $delimiter));

        return lcfirst($str);
    }
}