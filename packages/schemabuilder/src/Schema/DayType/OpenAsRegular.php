<?php

namespace Locally\SchemaBuilder\Schema\DayType;

class OpenAsRegular extends DayType
{
    public function __construct()
    {
        parent::__construct();

        $this->fromDate = new \DateTime('1970-01-01 00:00', new \DateTimeZone($this->timezone));
        $this->toDate = new \DateTime('1970-01-01 23:59:59', new \DateTimeZone($this->timezone));
    }

    public function toString()
    {
        $output = 'open_regular';

        return $output;
    }
}
