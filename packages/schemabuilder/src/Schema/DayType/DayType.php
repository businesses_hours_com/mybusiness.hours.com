<?php

namespace Locally\SchemaBuilder\Schema\DayType;

use Locally\SchemaBuilder\Traits\HasDatePeriods;
use Locally\SchemaBuilder\Traits\HasTimezone;

class DayType
{
    use HasDatePeriods, HasTimezone;

    /**
     * @var bool
     */
    protected $byAppointmentOnly = false;

    /**
     * DayType constructor.
     */
    public function __construct()
    {
        // set the timezone (either from parent $_ENV or from fallback.
        $this->setTimezone();
    }

    /**
     * Default toArray output
     *
     * @return string
     */
    public function toArray()
    {
        return $this->toString();
    }

    /**
     * Default toString output
     *
     * @return string
     */
    public function toString()
    {
        return '';
    }

    /**
     * @param bool $flag
     *
     * @return $this
     */
    public function setByAppointmentOnly(bool $flag = true)
    {
        $this->byAppointmentOnly = $flag;

        return $this;
    }
}