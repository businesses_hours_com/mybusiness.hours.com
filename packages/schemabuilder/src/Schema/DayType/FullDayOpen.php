<?php

namespace Locally\SchemaBuilder\Schema\DayType;

class FullDayOpen extends DayType
{
    /**
     * FullDayOpen constructor.
     */
    public function __construct($byAppointmentOnly = false)
    {
        parent::__construct();

        $this->fromDate = new \DateTime('1970-01-01 00:00', new \DateTimeZone($this->timezone));
        $this->toDate = new \DateTime('1970-01-01 23:59:59', new \DateTimeZone($this->timezone));

        $this->byAppointmentOnly = $byAppointmentOnly;
    }

    /**
     * @return string
     */
    public function toString()
    {
        $output = '24h';

        if ($this->byAppointmentOnly) {
            $output .= '[appointment]';
        }

        return $output;
    }
}