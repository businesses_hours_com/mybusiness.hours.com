<?php

namespace Locally\SchemaBuilder\Schema\DayType;

class FullDayClosed extends DayType
{
    protected ?string $reason;

    public function __construct(?string $reason = null)
    {
        parent::__construct();

        $this->reason   = $reason;
        $this->fromDate = new \DateTime('1970-01-01 00:00', new \DateTimeZone($this->timezone));
        $this->toDate   = new \DateTime('1970-01-01 23:59:59', new \DateTimeZone($this->timezone));
    }

    public function toString(): string
    {
        $output = 'closed';

        return $this->reason ? ($output."[".$this->reason."]") : $output;
    }
}
