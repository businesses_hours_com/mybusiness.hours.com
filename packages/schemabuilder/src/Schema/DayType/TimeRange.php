<?php

namespace Locally\SchemaBuilder\Schema\DayType;

class TimeRange extends DayType
{
    /**
     * TimeRange constructor.
     *
     * @param $from
     * @param $to
     *
     * @throws \ErrorException
     */
    public function __construct($from, $to, $byAppointmentOnly = false)
    {
        parent::__construct();

        // should 24:30 be allowed for example? --> 24:30 allowed for now (after discussion)
        if ($from = $this->checkDateFormat($from)) {
            $this->fromDate = new \DateTime("1970-01-01 $from", new \DateTimeZone($this->timezone));
        }

        if ($to = $this->checkDateFormat($to)) {
            $this->toDate = new \DateTime("1970-01-01 $to", new \DateTimeZone($this->timezone));

            if ($this->fromDate && $this->toDate <= $this->fromDate) {
                $this->toDate->modify('+1 day');
            }
        }

        $this->byAppointmentOnly = $byAppointmentOnly;
    }

    /**
     * @return string
     */
    public function toString()
    {
        $output = 'closed';

        if ($this->fromDate) {
            $output = $this->fromDate->format('Hi');

            if ($this->toDate) {
                $output .= '/' . $this->toDate->format('Hi');
            }

            if ($this->byAppointmentOnly) {
                $output .= '[appointment]';
            }
        }

        return $output;
    }

    /**
     * @param $time
     *
     * @return mixed
     * @throws \ErrorException
     */
    protected function checkDateFormat($time)
    {
        if(!preg_match('/^([0-9]|0[0-9]|1[0-9]|2[0-4]):[0-5][0-9]$/', $time)) {
            $trace = debug_backtrace();
            $args = json_encode($trace[1]['args']);
            throw new \ErrorException("Used arguments for {$trace[1]['class']} not in correct format.
                Used: $args.
                Must match: ([0-9]|0[0-9]|1[0-9]|2[0-4]):[0-5][0-9]");
        }

        return $time;
    }
}