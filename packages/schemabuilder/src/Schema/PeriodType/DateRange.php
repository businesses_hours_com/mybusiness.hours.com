<?php

namespace Locally\SchemaBuilder\Schema\PeriodType;

class DateRange extends PeriodType
{
    /**
     * @var string
     */
    protected $dateRegex = '/^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]$/';

    /**
     * DateRange constructor.
     *
     * @param $from
     * @param $to
     *
     * @throws \ErrorException
     */
    public function __construct($from, $to)
    {
        parent::__construct();

        if ($from = $this->checkDateFormat($from)) {
            $this->fromDate = new \DateTime("{$from} 00:00", new \DateTimeZone($this->timezone));
        }

        if ($to = $this->checkDateFormat($to)) {
            $this->toDate = new \DateTime("{$to} 00:00", new \DateTimeZone($this->timezone));
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return "{$this->fromDate->format('Y-m-d')}/{$this->toDate->format('Y-m-d')}";
    }
}