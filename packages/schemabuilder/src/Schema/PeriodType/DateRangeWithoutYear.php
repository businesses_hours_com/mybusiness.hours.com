<?php

namespace Locally\SchemaBuilder\Schema\PeriodType;

use DateTime;

class DateRangeWithoutYear extends PeriodType
{
    protected $dateRegex = '/^[0-9][0-9]-[0-9][0-9]$/';

    protected $fromWithoutYear;
    protected $toWithoutYear;

    public function __construct($from, $to)
    {
        parent::__construct();

        $this->fromWithoutYear = $from;
        $this->toWithoutYear   = $to;

        if ($from = $this->checkDateFormat($from)) {

            $dateString     = now()->year . '-' . $from . ' 00:00';
            $this->fromDate = new DateTime($dateString, new \DateTimeZone($this->timezone));
        }

        if ($to = $this->checkDateFormat($to)) {
            $fromMonth    = explode('-', $from)[0];
            $toMonth      = explode('-', $to)[0];
            $year         = (int)$toMonth < (int)$fromMonth ? now()->addYear()->year : now()->year;
            $dateString   = $year . '-' . $to . ' 00:00';
            $this->toDate = new DateTime($dateString, new \DateTimeZone($this->timezone));
        }
    }

    public function getName(): string
    {
        return "{$this->fromWithoutYear}/{$this->toWithoutYear}";
    }
}