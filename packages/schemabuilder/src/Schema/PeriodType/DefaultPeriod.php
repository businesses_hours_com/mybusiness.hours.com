<?php

namespace Locally\SchemaBuilder\Schema\PeriodType;

class DefaultPeriod extends PeriodType
{
    /**
     * @return string
     */
    public function getName()
    {
        return 'default';
    }
}