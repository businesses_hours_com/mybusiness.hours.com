<?php

namespace Locally\SchemaBuilder\Schema\PeriodType;

class MonthRange extends PeriodType
{
    /**
     * @var string
     */
    protected $dateRegex = '/^Jan|Feb|Mar|Apr|May|June|Jun|July|Jul|Aug|Sept|Sep|Oct|Nov|Dec$/i';

    /**
     * MonthRange constructor.
     *
     * @param $from
     * @param $to
     *
     * @throws \ErrorException
     */
    public function __construct($from, $to)
    {
        parent::__construct();

        $year = date('Y');
        if ($from = $this->checkDateFormat($from)) {
            $normalizedFrom = ucfirst(strtolower($from));
            $this->fromDate = \DateTime::createFromFormat(
                'j-M-Y H:i',
                "01-{$normalizedFrom}-{$year} 00:00",
                new \DateTimeZone($this->timezone)
            );
        }

        if ($to = $this->checkDateFormat($to)) {
            $normalizedTo = ucfirst(strtolower($to));
            $this->toDate = \DateTime::createFromFormat(
                'j-M-Y H:i',
                "01-{$normalizedTo}-{$year} 00:00",
                new \DateTimeZone($this->timezone)
            );
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        $from = strtolower($this->fromDate->format('M'));
        $to = strtolower($this->toDate->format('M'));

        return "{$from}/{$to}";
    }
}