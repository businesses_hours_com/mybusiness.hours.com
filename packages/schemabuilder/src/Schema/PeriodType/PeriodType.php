<?php

namespace Locally\SchemaBuilder\Schema\PeriodType;

use Locally\SchemaBuilder\Traits\HasDatePeriods;
use Locally\SchemaBuilder\Traits\HasTimezone;

class PeriodType
{
    use HasDatePeriods, HasTimezone;

    /**
     * @var string
     */
    protected $dateRegex;

    /**
     * PeriodType constructor.
     */
    public function __construct()
    {
        // set the timezone (either from parent $_ENV or from fallback.
        $this->setTimezone();
    }

    /**
     * @return string
     */
    public function getRegex()
    {
        return $this->dateRegex;
    }

    /**
     * Default toArray output
     *
     * @return string
     */
    public function toArray()
    {
        return $this->getName();
    }

    /**
     * @return string
     */
    public function toString()
    {
        return $this->getName();
    }

    /**
     * Default toString output
     *
     * @return string
     */
    public function getName()
    {
        return '';
    }

    /**
     * @return int
     */
    public function getLengthInDays()
    {
        if ($this->fromDate && $this->toDate) {
            return (int)$this->fromDate->diff($this->toDate, true)->days;
        }

        return 366;
    }

    /**
     * @param $time
     *
     * @return mixed
     * @throws \ErrorException
     */
    protected function checkDateFormat($date)
    {
        if($this->dateRegex && !preg_match($this->dateRegex, $date)) {
            $trace = debug_backtrace();
            $args = json_encode($trace[1]['args']);
            throw new \ErrorException("Used arguments for {$trace[1]['class']} not in correct format.
                Used: {$args}.
                Must match: {$this->dateRegex}");
        }

        return $date;
    }
}