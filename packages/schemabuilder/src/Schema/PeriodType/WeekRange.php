<?php

namespace Locally\SchemaBuilder\Schema\PeriodType;

class WeekRange extends PeriodType
{
    /**
     * @var string
     */
    protected $dateRegex = '/^(5[0-3]|[1-4][0-9]|0[1-9])$/';

    /**
     * WeekRange constructor.
     *
     * @param $from
     * @param $to
     *
     * @throws \ErrorException
     */
    public function __construct($from, $to)
    {
        parent::__construct();

        if ($from = $this->checkDateFormat($from)) {
            $this->fromDate = new \DateTime('now 00:00', new \DateTimeZone($this->timezone));
            $this->fromDate->setISODate(date('Y'), $from);
        }

        if ($to = $this->checkDateFormat($to)) {
            $this->toDate = new \DateTime('now 00:00', new \DateTimeZone($this->timezone));
            $this->toDate->setISODate(date('Y'), $to);
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return "week {$this->fromDate->format('W')}/{$this->toDate->format('W')}";
    }
}