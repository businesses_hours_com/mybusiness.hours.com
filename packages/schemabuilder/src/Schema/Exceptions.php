<?php

namespace Locally\SchemaBuilder\Schema;

use Locally\SchemaBuilder\Traits\HasPeriodOverlaps;
use Locally\SchemaBuilder\Traits\HasTimezone;

class Exceptions extends SchemaBuilder
{
    use HasPeriodOverlaps, HasTimezone;

    /**
     * Exceptions constructor.
     */
    public function __construct()
    {
        // set the timezone (either from parent $_ENV or from fallback.
        $this->setTimezone();
    }

    /**
     * Override parent __set method; Exceptions is allowed to set any attribute
     *
     * @param $name
     * @param $value
     *
     * @return $this
     * @throws \Exception
     */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;

        return $this;
    }

    /**
     * @param       $key
     * @param mixed ...$dayTypes
     *
     * @return $this
     * @throws \ErrorException
     */
    public function add($key, ...$dayTypes)
    {
        if($day = $this->getFormatedDateFromDateString($key)) {
            foreach ($dayTypes as $dayType) {
                // check if allowed (overlap AND inner NOT allowed)
                $this->checkDayOverlap($day, $dayType);

                $dayVal = $this->$day ?: [];
                $dayVal[] = $dayType;

                // sort the new array!
                $this->sortDayValues($dayVal);

                $this->$day = $dayVal;

                // sort exceptions
                $this->sortExceptions($this->data);
            }
        }

        return $this;
    }

    /**
     * @param $date
     *
     * @return string
     * @throws \ErrorException
     */
    protected function getFormatedDateFromDateString($date)
    {
        $date = $this->validateDateString($date);

        return (new \DateTime($date, new \DateTimeZone($this->timezone)))->format('Y-m-d');
    }

    /**
     * @param $date
     *
     * @return mixed
     * @throws \ErrorException
     */
    protected function validateDateString($date)
    {
        if(!preg_match('/^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]$/', $date)) {
            $trace = debug_backtrace();
            $args = json_encode($trace[1]['args']);
            throw new \ErrorException("Used arguments for {$trace[1]['class']} not in correct format.
                Used: $args.
                Must match: [0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]");
        }

        return $date;
    }

    /**
     * @param $data
     */
    protected function sortExceptions(&$data)
    {
        uksort($data, function($a, $b) {
            $aDate = new \DateTime("{$a} 00:00");
            $bDate = new \DateTime("{$b} 00:00");

            return $aDate < $bDate ? -1 : 1;
        });
    }
}