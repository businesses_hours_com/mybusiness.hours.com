<?php

namespace Locally\SchemaBuilder\Schema;

class Meta extends SchemaBuilder
{
    /**
     * Meta constructor.
     *
     * @param string|null $type
     * @param null        $data
     */
    public function __construct(string $type = null, $data = null)
    {
        if ($type) {
            $this->data[$type] = $data;
        }
    }

    /**
     * @param string|null $type
     * @param null        $data
     */
    public function add(string $type, $data = null)
    {
        $this->data[$type] = $data;

        return $this;
    }

    /**
     * Override parent __set method; meta is allowed to set any attribute
     *
     * @param $name
     * @param $value
     *
     * @return $this
     * @throws \Exception
     */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;

        return $this;
    }
}