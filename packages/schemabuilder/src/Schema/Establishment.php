<?php

namespace Locally\SchemaBuilder\Schema;

use Locally\SchemaBuilder\Traits\HasTimezone;

class Establishment extends SchemaBuilder
{
    use HasTimezone {
        setTimezone as traitSetTimezone;
    }

    /**
     * To add or not to add checksum for object/array attributes
     *
     * @var bool
     */
    protected $addChecksum = true;

    /**
     * Default timezone.
     *
     * @var string
     */
    protected $defaultTimezone = 'Europe/Amsterdam';

    /**
     * Schema options, attribute => casting.
     *
     * @var array
     */
    protected $schema = [
        'id'                    => 'integer',
        'hash'                  => 'string',
        'business_id'           => 'integer',
        'user_id'               => 'integer',
        'category_id'           => 'integer',
        'address_id'            => 'integer',
        'name'                  => 'string',
        'description'           => 'string',
        'timezone'              => 'string',
        'address'               => 'object',
        'contact'               => 'object',
        'meta'                  => 'object',
        'hours'                 => 'array',
        'opening_date'          => 'string',
        'source_url'            => 'string',
        'slug'                  => 'string',
        'slug_legacy'           => 'string',
    ];

    /**
     * Establishment constructor.
     *
     * @param null $hash
     * @param null $addressId
     * @param null $businessId
     * @param null $userId
     * @param null $timezone
     */
    public function __construct($hash = null, $addressId = null, $businessId = null, $userId = null, $timezone = null)
    {
        if ($hash) {
            $this->hash = $hash;
        }
        if ($addressId) {
            $this->address_id = $addressId;
        }
        if ($businessId) {
            $this->business_id = $businessId;
        }
        if ($userId) {
            $this->user_id = $userId;
        }

        try {
            $this->setTimezone($timezone ?: $this->defaultTimezone);
        } catch (\ErrorException $e) {
        }

        // initialize slug (if business_id is set, we allready have somthing at least).
        $this->setSlug();
    }

    /**
     * @param $key
     *
     * @return Department
     */
    public function department($key)
    {
        // return/find by int-key?
        if ((is_int($key) || $key === null) && $this->hours && isset($this->hours[$key])) {
            return $this->hours[$key];
        }

        // return/find department name
        if (is_string($key) && $this->hours) {
            foreach ($this->hours as $department) {
                if ($department->department === $key) {
                    return $department;
                }
            }
        }
    }

    /**
     * @return string|null
     */
    public function getPostalCodeAttribute()
    {
        return $this->address ? $this->address->postal_code : null;
    }

    /**
     * @return int|null
     */
    public function getHouseNumberAttribute()
    {
        return $this->address ? $this->address->house_number : null;
    }

    /**
     * @return string|null
     */
    public function getHouseNumberAdditionAttribute()
    {
        return $this->address ? $this->address->house_number_addition : null;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setName($value)
    {
        $this->name = $value;

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setDescription($value)
    {
        $this->description = $value;

        return $this;
    }

    /**
     * @param $id
     *
     * @return $this
     */
    public function setBusinessId($id)
    {
        $this->business_id = $id;

        // update slug.
        $this->setSlug();

        return $this;
    }

    /**
     * @param $id
     *
     * @return $this
     */
    public function setCategoryId($id)
    {
        $this->category_id = $id;

        return $this;
    }

    /**
     * @param $value
     * @return $this
     * @deprecated
     */
    public function setPostcalCode($value)
    {
        return $this->setPostalCode($value);
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setPostalCode($value)
    {
        $this->setAddressAttribute('postal_code', $value);

        // update slug.
        $this->setSlug();

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setHouseNumber($value)
    {
        $this->setAddressAttribute('house_number', $value);

        // update slug.
        $this->setSlug();

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setHouseNumberAddition($value)
    {
        $this->setAddressAttribute('house_number_addition', $value);

        // update slug.
        $this->setSlug();

        return $this;
    }

    /**
     * @param $timezone
     *
     * @throws \ErrorException
     */
    public function setTimezone($timezone)
    {
        $this->traitSetTimezone($timezone);

        // Store timezone in $_ENV for use in child nodes.
        $_ENV['establishment_timezone'] = $timezone;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setEmail($value)
    {
        $this->setContactAttribute('email', $value);

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setPhoneNumber($value)
    {
        $this->setContactAttribute('phone', $value);

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setFaxNumber($value)
    {
        $this->setContactAttribute('fax', $value);

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setWebsite($value)
    {
        $this->setContactAttribute('website', $value);

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     * @throws \ErrorException
     */
    public function setOpeningDate($value)
    {
        if (!$this->timezone) {
            throw new \ErrorException("Please set a timezone before adding an opening date.");
        }
        if ($date = $this->checkDateFormat($value)) {
            $this->opening_date = (new \DateTime("{$date} 00:00", new \DateTimeZone($this->timezone)))->format('Y-m-d');
        }

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     * @throws \ErrorException
     */
    public function setSourceUrl($value)
    {
        if ($url = $this->checkUrlFormat($value)) {
            $this->source_url = $url;
        }

        return $this;
    }

    /**
     * @param null $value
     *
     * @return $this
     */
    public function setSlug($value = null)
    {
        if ($value) {
            $this->slug = $value;
            return $this;
        } else {
            $slug = [];

            // gather all slug parts, where possible.
            foreach (['business_id', 'postal_code', 'house_number', 'house_number_addition'] as $attribute) {
                if ($part = $this->$attribute) {
                    $slug[] = $part;
                }
            }

            // compose the slug from all parts.
            $this->slug = implode('-', $slug);
        }

        return $this;
    }

    /**
     * @param Department $department
     *
     * @return $this
     */
    public function addDepartment(Department $department)
    {
        $hours = $this->hours ?: [];
        $hours[] = $department;

        $this->hours = $hours;

        return $this;
    }

    /**
     * Can be a Meta object or key/value combination
     *
     * @param      $meta
     * @param null $value
     *
     * @return $this
     */
    public function addMeta($meta, $value = null)
    {
        if (!$meta instanceof Meta && $value) {
            $meta = new Meta($meta, $value);
        }

        if ($meta && $this->meta) {
            $this->meta->data = array_merge($this->meta->data, $meta->data);
        } elseif($meta) {
            $this->meta = $meta;
        }

        return $this;
    }

    /**
     * @param $key
     *
     * @return $this
     */
    public function removeDepartment($key)
    {
        $hours = $this->hours;

        // return/find by int-key?
        if ((is_int($key) || $key === null) && $hours && isset($hours[$key])) {
            unset($hours[$key]);
            $this->hours = $hours;

            return $this;
        }

        // return/find department name
        if (is_string($key) && $hours) {
            foreach ($hours as $departmentKey => $department) {
                unset($hours[$departmentKey]);
                $this->hours = $hours;

                return $this;
            }
        }
    }

    /**
     * @param $attribute
     * @param $value
     */
    protected function setContactAttribute($attribute, $value)
    {
        if (!$this->contact) {
            $this->contact = new Contact();
        }

        $this->contact->$attribute = $value;
    }

    /**
     * @param $attribute
     * @param $value
     */
    protected function setAddressAttribute($attribute, $value)
    {
        if (empty($value) || (is_string($value) && empty(trim($value)))) {
            return;
        }

        if (!$this->address) {
            $this->address = new Address($this->address_id ?? null);
        }

        $this->address->$attribute = $value;
    }

    /**
     * @param $date
     *
     * @return mixed
     * @throws \ErrorException
     */
    protected function checkDateFormat($date)
    {
        if(!preg_match('/^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]$/', $date)) {
            $trace = debug_backtrace();
            $args = json_encode($trace[1]['args']);
            throw new \ErrorException("Used arguments for {$trace[1]['class']} not in correct format.
                Used: {$args}.
                Must match: /^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]$/");
        }

        return $date;
    }

    /**
     * @param $url
     *
     * @return mixed
     * @throws \ErrorException
     */
    protected function checkUrlFormat($url)
    {
        if(!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new \ErrorException("Incorrect URL format provided.");
        }

        return $url;
    }

    public function setSlugLegacy(string $string)
    {
        $this->slug_legacy = $string;
    }
}
