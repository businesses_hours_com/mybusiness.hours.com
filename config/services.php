<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain'   => env('MAILGUN_DOMAIN'),
        'secret'   => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key'    => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'backend' => [
        'api_env'   => env('BACKEND_API_ENV'),
        'api_token' => [
            'production' => env('BACKEND_API_TOKEN_PRODUCTION'),
            'staging'    => env('BACKEND_API_TOKEN_STAGING'),
            'local'      => env('BACKEND_API_TOKEN_LOCAL'),
        ],
    ],

    // curl crawler and scraper
    'crawler' => [
        'url' => 'http://docker.datafactory.one:9000',
    ],

    'address_api_url' => 'https://api.datafactory.one/address/v0/address',
    'address_api_proxy_url' => 'https://address.datafactory.one/address_proxy/address/',

    'yext' => [
        'login_url' => [
            'nl' => env('YEXT_LOGIN_URL_NL'),
            'de' => env('YEXT_LOGIN_URL_NL'),
            'uk' => env('YEXT_LOGIN_URL_NL'),
            'us' => env('YEXT_LOGIN_URL_NL'),
            'fr' => env('YEXT_LOGIN_URL_NL'),
        ],
    ]
];
