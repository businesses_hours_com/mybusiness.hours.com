<?php

namespace App\Console\Commands;

use App\Models\LocationWizard;
use Illuminate\Console\Command;

class CleanWizardData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wizard:clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove old wizard data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Save log to db
     */
    public function handle()
    {
        LocationWizard::draft()->whereDate('updated_at', '<', now()->subDay())->delete();
    }
}
