<?php

use App\Models\Country;
use Illuminate\Support\Facades\Cache;
use Money\Exception\FormatterException;
use Illuminate\Support\Facades\File;

if (!function_exists('localisedDaysOfWeekArray')) {
    /**
     * @return array
     */
    function localisedDaysOfWeekArray()
    {
        return [
            __('wizard.step3.monday'),
            __('wizard.step3.tuesday'),
            __('wizard.step3.wednesday'),
            __('wizard.step3.thursday'),
            __('wizard.step3.friday'),
            __('wizard.step3.saturday'),
            __('wizard.step3.sunday'),
        ];
    }
}

if (!function_exists('planPrice')) {
    /**
     * @param $plan
     * @param string $type
     * @return int
     */
    function planPrice($plan, $type = '')
    {
        $value = config('cashier_plans.plans.' . $plan . '.amount.value');

        if (!$value) {
            throw new FormatterException();
        }

        return $type === 'monthly' ? intval($value) / 12 : intval($value);
    }
}
