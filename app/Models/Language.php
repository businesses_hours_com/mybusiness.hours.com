<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Language extends Model
{
    protected $connection = 'mysql_backend';

    public function countries(): BelongsToMany
    {
        return $this->belongsToMany(Country::class, 'country_language', 'language_id', 'country_code', 'country_code', 'id')->withTimestamps();
    }
}
