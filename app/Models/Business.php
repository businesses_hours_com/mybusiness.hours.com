<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $connection = 'mysql_backend';
    protected $table      = 'kc_businesses';
}
