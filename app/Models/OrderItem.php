<?php

namespace App\Models;

use Laravel\Cashier\Order\OrderItem as CashierOrderItem;

class OrderItem extends CashierOrderItem
{
    protected $connection = 'mysql';
}
