<?php

namespace App\Models;

use App\Services\CashierMollie\PercentageDiscountHandler;
use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Coupon\Coupon as CashierCoupon;
use Laravel\Cashier\Coupon\FixedDiscountHandler;

/**
 * @method static active()
 */
class Coupon extends Model
{
    protected $connection = 'mysql_backend';

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    /**
     * Builds a Cashier coupon from the current model.
     */
    public function buildCashierCoupon(): CashierCoupon
    {
        $discountHandler = $this->is_percentage ?
            (new PercentageDiscountHandler) :
            (new FixedDiscountHandler);

        $context = [
            'description'   => $this->description,
            'discount'      => [
                'value'    => $this->amount,
                'currency' => 'EUR',
            ],
            'allow_surplus' => false,
        ];

        return new CashierCoupon($this->code, $discountHandler, $context);
    }
}
