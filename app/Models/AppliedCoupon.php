<?php

namespace App\Models;

use Laravel\Cashier\Coupon\AppliedCoupon as CashierAppliedCoupon;

class AppliedCoupon extends CashierAppliedCoupon
{
    protected $connection = 'mysql';
}
