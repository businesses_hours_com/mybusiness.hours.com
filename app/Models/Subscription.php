<?php
namespace App\Models;

use Laravel\Cashier\Subscription as CashierSubscription;

class Subscription extends CashierSubscription
{
    protected $connection = 'mysql';

    protected $table = 'subscriptions';

    const PLAN_PREMIUM = 'premium';
}
