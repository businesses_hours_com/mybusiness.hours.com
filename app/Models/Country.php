<?php

namespace App\Models;

use App\Services\AdminHoursService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class Country extends Model
{
    const NL = 'nl';
    const UK = 'uk';
    const FR = 'fr';
    const BE = 'be';
    const US = 'us';
    const DE = 'de';

    const ACTIVE_COUNTRIES = [
        self::NL,
        self::UK,
        self::FR,
        self::BE,
        self::DE,
        self::US,
    ];

    const DOMAIN_US = 'mybusiness.hours.com';
    const DOMAIN_UK = 'mybusiness.opening-times.co.uk';
    const DOMAIN_FR = 'monentreprise.horaire.com';
    const DOMAIN_NL = 'mijnbedrijf.openingstijden.nl';
    const DOMAIN_DE = 'meingeschaft.oeffnungszeiten.com';
    const DOMAIN_BE = 'mijnbedrijf.openuren.be';

    const DOMAIN_COUNTRY_MAP = [
        self::DOMAIN_NL => [self::NL],
        self::DOMAIN_UK => [self::UK],
        self::DOMAIN_FR => [self::FR],
        self::DOMAIN_DE => [self::DE],
        self::DOMAIN_BE => [self::BE],
        self::DOMAIN_US => [self::US],
    ];

    const COUNTRY_TIMEZONE_MAP = [
        self::NL => 'Europe/Amsterdam',
        self::UK => 'Europe/London',
        self::FR => 'Europe/Paris',
        self::DE => 'Europe/Berlin',
        self::BE => 'Europe/Brussels',
        self::US => 'US/Eastern',
    ];

    protected $connection = 'mysql_backend';
    protected $table = 'country_codes';

    public function languages(): BelongsToMany
    {
        return $this->belongsToMany(
            Language::class,
            'country_language',
            'country_code',
            'language_id',
            'country_code',
            'id'
        )->withTimestamps();
    }

    public function defaultLanguage(): BelongsToMany
    {
        return $this->languages()->where('default', 1);
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public static function settings(): Collection
    {
        $adminHoursApi = new AdminHoursService();

        try {
            return collect($adminHoursApi->getSettings());
        } catch (\Exception $e) {
            Log::error($e);
            return collect([]);
        }
    }

    public static function getCountryDefaultLocale(string $countryCode): ?string
    {
        $locales = static::settings()->get("locales")[$countryCode] ?? [];

        $localeCodes = array_map(
            fn($localeCode) => explode('_', $localeCode)[0],
            array_keys($locales)
        );

        $locales = array_combine($localeCodes, array_values($locales));

        return array_keys($locales)[0] ?? null;
    }

    public static function getCountryTimezone(string $countryCode = null): string
    {
        return static::COUNTRY_TIMEZONE_MAP[$countryCode ?? config('app.country')] ?? config(
            'app.default_frontend_timezone'
        );
    }

    public static function getDomainSpecificConfig(string $domain): array
    {
        if (!$countryCodes = static::DOMAIN_COUNTRY_MAP[$domain] ?? null) {
            return [];
        }

        $countryCode = $countryCodes[0];

        return Cache::rememberForever(
            "domain_specific_config_{$domain}_{$countryCode}",
            function () use ($domain, $countryCode) {
                return [
                    'app.name' => ucfirst(explode('.', $domain)[0]),
                    'app.url' => $domain,
                    'app.country' => $countryCode,
                    'app.locale' => static::getCountryDefaultLocale($countryCode),
                    'app.timezone' => static::getCountryTimezone($countryCode),
                ];
            }
        );
    }

    public static function getCountryLogo(): ?string
    {
        $locale = config('app.country');

        if (!File::exists(public_path("images/logo/logo-{$locale}.png"))) {
            return asset("images/logo/logo-nl.png");
        }

        return asset("images/logo/logo-{$locale}.png");
    }
}
