<?php

namespace App\Models;

use Carbon\Carbon;
use Laravel\Cashier\Order\Order as CashierOrder;

class Order extends CashierOrder
{
    protected $connection = 'mysql';

    public function getProcessedAtFormattedAttribute(): ?string
    {
        return $this->processed_at ? Carbon::parse($this->processed_at)
            ->setTimezone(Country::getCountryTimezone())
            ->format('d/m/Y - H:i:s') : null;
    }
}
