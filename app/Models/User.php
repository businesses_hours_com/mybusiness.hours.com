<?php

namespace App\Models;

use App\Services\LocationImporter;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Laravel\Cashier\Billable;
use App\Services\AdminHoursService;
use Illuminate\Support\Facades\Cache;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Order\Contracts\ProvidesInvoiceInformation;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Spatie\Permission\Traits\HasRoles;
use stdClass;

/**
 * @property mixed name
 * @property mixed last_name
 * @property mixed email
 * @property mixed company
 * @property mixed account_type
 * @property ?string retailer_type
 */
class User extends Authenticatable implements MustVerifyEmail, CanResetPasswordContract, ProvidesInvoiceInformation
{
    use SoftDeletes, Notifiable, CanResetPassword, Billable, HasRoles;

    const TYPE              = 'retailer';
    const BUSINESS_OWNER    = 'business_owner';
    const BUSINESS_EMPLOYEE = 'business_employee';
    const MARKETING_COMPANY = 'marketing_company';

    const CACHE_LOCATIONS  = 'locations';
    const CACHE_BUSINESSES = 'businesses';
    const CACHE_SETTINGS   = 'setting';

    protected $connection = 'mysql_backend';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'company',
        'retailer_type',
        'email',
        'password',
        'country_codes',
        'type',
        'email_verified_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected AdminHoursService $adminHoursApi;
    protected LocationImporter  $locationImportService;

    public function __construct(array $attributes = [])
    {
        $this->adminHoursApi         = new AdminHoursService();
        $this->locationImportService = new LocationImporter();
        parent::__construct($attributes);
    }

    protected static function boot()
    {
        parent::boot();

//        static::addGlobalScope('countryCode', function(Builder $builder) {
//            $builder
//                ->where('country_codes', 'like', '%' . config('app.country') . '%')
//                ->orWhere('country_codes', '[]')
//                ->orWhereNull('country_codes');
//        });
    }

    /** Override from Billable trait with custom Subscription class */
    public function subscriptions(): MorphMany
    {
        return $this->morphMany(Subscription::class, 'owner');
    }

    /** Override from Billable trait with custom Order class */
    public function orders(): MorphMany
    {
        return $this->morphMany(Order::class, 'owner');
    }

    public function wizards(): HasMany
    {
        return $this->hasMany(LocationWizard::class, 'user_id')->latest();
    }

    public function locations(): Collection
    {
        return Cache::remember($this->getCacheKey(static::CACHE_LOCATIONS), now()->addHour(), function() {
            $data = $this->adminHoursApi->initData($this);
            $data = ($data['locations']['error'] ?? false) ? [] : $data['locations'];

            return collect($data);
        });
    }

    public function businesses(): Collection
    {
        return Cache::remember($this->getCacheKey(static::CACHE_BUSINESSES), now()->addHour(), function() {
            $data = $this->adminHoursApi->initData($this);
            $data = ($data['businesses']['error'] ?? false) ? [] : $data['businesses'];

            return collect($data);
        });
    }

    public function reports(): Collection
    {
        return Cache::remember($this->getCacheKey(__FUNCTION__), now()->addHour(), function() {
            $data = $this->adminHoursApi->baseApi->get('reports');
            $data = ($data['error'] ?? false) ? [] : $data;

            return collect($data);
        });
    }

    public function settings(): Collection
    {
        return Cache::remember($this->getCacheKey(static::CACHE_SETTINGS), now()->addHour(), function() {
            $data = $this->adminHoursApi->initData($this);

            return collect($data['settings']);
        });
    }

    public function getCountryCodesAttribute($value): array
    {
        $array = $value ? json_decode($value, true) : [];

        $array = array_values(Arr::wrap($array ?: Country::active()->pluck('country_code')->unique()->toArray()));

        // NL always first
        usort($array, function($a, $b) {
            if ($a == Country::NL) {
                return -1;
            }
            if ($b == Country::NL) {
                return 1;
            }

            return strcmp($a, $b);
        });

        return $array;
    }

    public function getCountryCodeAttribute(): string
    {
        $countryCode = $this->country_codes[0] ?? 'not_set';

        return $countryCode === 'not_set' ? config('app.country') : $countryCode;
    }

    public function getLocation(string $hash): ?stdClass
    {
        $location = $this->locations()->firstWhere('hash', $hash);

        // admin can edit any location, doesn't have to own it.
        if (! $location && $this->isAdmin()) {
            $locationArray = $this->locationImportService->getSingleLocationFromApi($hash);
            $location      = json_decode(json_encode($locationArray)) ?: null;

            if (! $location) {
                abort(404);
            }
        }

        return $location;
    }

    public function getLocalesPerCountry($countryCode): array
    {
        $locales = $this->settings()->get("locales")[$countryCode] ?? [];

        $localeCodes = array_map(
            fn($localeCode) => explode('_', $localeCode)[0],
            array_keys($locales)
        );

        return array_combine($localeCodes, array_values($locales));
    }

    public function getCurrentCountryLocales(): array
    {
        return $this->getLocalesPerCountry(config('app.country'));
    }

    public function getCurrentLocale(): ?string
    {
        return $this->getDefaultLocaleCodePerCountry(config('app.country'));
    }

    public function getDefaultLocaleCodePerCountry($countryCode): ?string
    {
        $locales = $this->getLocalesPerCountry($countryCode);

        return array_keys($locales)[0] ?? null;
    }

    public function getFullNameAttribute(): ?string
    {
        return $this->last_name ? ($this->name . ' ' . $this->last_name) : $this->name;
    }

    public function shouldShowQaAndReviews(): bool
    {
        $qaReviewsSettings = $this->settings()->get('qa_reviews_settings');

        $shouldShow = ($qaReviewsSettings[config('app.country')] ?? false) === true;

        return $shouldShow && $this->locations()->count();
    }

    public function canAccessBusiness($id): bool
    {
        return $this->businesses()->firstWhere('id', $id) ? true : false;
    }

    public function isAdmin(): bool
    {
        return $this->hasRole('admin');
    }

    public function isOwner(): bool
    {
        return $this->retailer_type === self::BUSINESS_OWNER;
    }

    public function isEmployee(): bool
    {
        return $this->retailer_type === self::BUSINESS_EMPLOYEE;
    }

    public function isMarketingCompany(): bool
    {
        return $this->retailer_type === self::MARKETING_COMPANY;
    }

    public function canEditExceptionDates(): bool
    {
        return $this->isOwner() || ! $this->retailer_type;
    }

    public function getCacheKey(string $type): string
    {
        if ($type === 'reports') {
            $type = 'possible_reports';
        }

        return $type . '&user=' . $this->id . '&country=' . config('app.country');
    }

    public function clearCache(string $type = null): void
    {
        if (! $type) {
            \Log::info(__FUNCTION__ . ": forget ALL");
            Cache::flush();
            return;
        }

        // if we clear Businesses, clear Locations as well
        $typeArray = $type === User::CACHE_BUSINESSES ? [$type, User::CACHE_LOCATIONS] : Arr::wrap($type);

        foreach ($typeArray as $cacheType) {
            \Log::info(__FUNCTION__ . ": Forget $cacheType");
            Cache::forget($this->getCacheKey($cacheType));
        }
    }

    /**
     * @return bool|string
     */
    public function getGravatarAttribute()
    {
        return false;
        $hash         = md5(strtolower(trim($this->email)));
        $gravatar_url = "https://www.gravatar.com/avatar/$hash?d=http%3A%2F%2Fexample.com%2Fimages%2Favatar.jpg";

        try {
            $headers = get_headers($gravatar_url);

            if (! preg_match("|200|", $headers[0])) {
                return false;
            }

            return $gravatar_url;
        }
        catch (Exception $e) {
            app('honeybadger')->notify($e, request());
            return false;
        }
    }

    public function getCountryName(string $code, bool $forDisplay = false)
    {
        $code = explode('_', $code)[0];
        $code = $code === 'uk' ? 'gb' : $code;

        if ($code === 'en') {
            $code = config('app.country') === 'us' ? 'us' : 'gb';
        }

        $names = Cache::remember('country_names', 86400, function() {
            return json_decode(file_get_contents("http://country.io/names.json"), true);
        });

        $name = $names[strtoupper($code)] ?? 'not-set';

        if ($forDisplay) {
            return ucwords(preg_replace("/[\s-]+/", " ", strtolower($name)));
        }

        return preg_replace("/[\s-]+/", "-", strtolower($name));
    }

    public function getSubscriptionName(array $hashes, string $planName = Subscription::PLAN_PREMIUM): string
    {
        $name = $planName;

        foreach ($hashes as $hash) {
            $name .= ";$hash";
        }

        return $name;
    }

    public function getSubscriptionForLocation(string $hash)
    {
        return $this->subscription($this->getSubscriptionName([$hash]));
    }

    /**
     * Overridden method from Cashier-Mollie Billable trait to allow searching for group subscription of multiple hashes
     */
    public function subscription($subscription = 'default')
    {
        [, $hash] = explode(';', $subscription);

        return $this->subscriptions->sortByDesc(function($value) {
            return $value->created_at->getTimestamp();
        })->first(function($value) use ($subscription, $hash) {
            return ($value->name === $subscription) || Str::contains($value->name, $hash);
        });
    }

    /**
     * Get the receiver information for the invoice.
     * Typically includes the name and some sort of (E-mail/physical) address.
     *
     * @return array An array of strings
     */
    public function getInvoiceInformation()
    {
        return [$this->name, $this->email];
    }

    /**
     * Get additional information to be displayed on the invoice.
     * Typically a note provided by the customer.
     *
     * @return string|null
     */
    public function getExtraBillingInformation()
    {
        return null;
    }

    public static function obfuscateEmail(string $email, int $minFill = 3): ?string
    {
        return preg_replace_callback(
            '/^(.)(.*?)([^@]?)(?=@[^@]+$)/u',
            function($m) use ($minFill) {
                return $m[1]
                    . str_repeat("*", max($minFill, mb_strlen($m[2], 'UTF-8')))
                    . ($m[3] ?: $m[1]);
            },
            $email
        );
    }
}
