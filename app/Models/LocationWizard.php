<?php

namespace App\Models;

use App\Services\LocationImporter;
use App\Services\WizardService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property mixed name
 * @property mixed last_name
 * @property mixed email
 * @property mixed company
 * @property mixed account_type
 * @property mixed|null kc_location_hash
 * @property false|string data
 */
class LocationWizard extends Model
{
    protected $connection = 'mysql';
    protected $table      = 'location_wizard';

    protected $fillable = [
        'user_id',
        'data',
        'kc_location_hash',
    ];

    protected $casts = [
        'data' => 'array',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function scopePublished($query)
    {
        return $query->whereNotNull('kc_location_hash');
    }

    public function scopeDraft($query)
    {
        return $query->whereNull('kc_location_hash');
    }

    public function scopeByHash($query, $hash)
    {
        return $query->where('kc_location_hash', $hash)->latest();
    }

    public static function fromHash(string $hash): ?LocationWizard
    {
        /** @var LocationWizard $wizard */
        $wizard = LocationWizard::query()
            ->where('kc_location_hash', $hash)
            ->latest()
            ->first();

        if (! $wizard) {
            $wizard = (new LocationImporter)->import($hash);
        }

        return $wizard;
    }

    public function mergeNewData(array $requestData): LocationWizard
    {
        $oldData = $this->data ?? [];
        $newData = WizardService::array_filter_recursive($requestData);
        $data    = array_merge($oldData, $newData);

        unset($data['_token'], $data['_method'], $data['locations-table_length']);

        $this->data = $data;

        return $this;
    }
}
