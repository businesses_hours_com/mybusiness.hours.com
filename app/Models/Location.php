<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $connection = 'mysql_backend';
    protected $table      = 'published_locations';

    const SOURCE_YEXT = 'yext';
}
