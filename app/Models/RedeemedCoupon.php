<?php

namespace App\Models;

use Laravel\Cashier\Coupon\RedeemedCoupon as CashierRedeemedCoupon;

class RedeemedCoupon extends CashierRedeemedCoupon
{
    protected $connection = 'mysql';
}
