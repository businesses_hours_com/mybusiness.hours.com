<?php

namespace App\Models;

use Laravel\Cashier\Credit\Credit as CashierCredit;

class Credit extends CashierCredit
{
    protected $connection = 'mysql';
}
