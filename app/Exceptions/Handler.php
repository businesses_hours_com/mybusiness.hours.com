<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Routing\Exceptions\InvalidSignatureException;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param Exception $e
     * @return void
     */
    public function report(Throwable $e)
    {
        if (! app()->environment('local') && app()->bound('honeybadger') && $this->shouldReport($e)) {
            app('honeybadger')->notify($e, app('request'));
        }
        parent::report($e);
    }

    public function render($request, Throwable $e)
    {
        $needsJson = $request->is('api/*') || $request->wantsJson() || $request->ajax();

        if($e instanceof InvalidSignatureException && $request->user() && !$request->user()->hasVerifiedEmail()){
            return redirect(route('verification.notice'));
        }

        if (! $needsJson || $e instanceof ValidationException) {
            return parent::render($request, $e);
        }

        return $this->prepareJsonResponse($request, $e);
    }
}
