<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WebScraperRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'website' => 'required|url',
        ];
    }
}
