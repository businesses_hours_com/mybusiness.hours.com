<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'name'             => ['required', 'string'],
            'last_name'        => ['required', 'string'],
            'email'            => ['required', 'string', 'email',
                                   Rule::unique('mysql_backend.users')->ignore(auth()->id())
            ],
            'retailer_type'    => ['string'],
            'current_password' => ['nullable', 'string', 'min:4',],
            'new_password'     => ['nullable', 'string', 'min:4', 'required_with_all:password,verify_password', 'same:verify_password'],
            'verify_password'  => ['nullable', 'string', 'min:4', 'required_with_all:password,new_password', 'same:new_password'],
        ];
    }
}
