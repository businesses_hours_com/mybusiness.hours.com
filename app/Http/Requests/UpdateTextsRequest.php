<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTextsRequest extends FormRequest
{
    public function rules()
    {
        return [
            'promoted_description' => ['required', 'string', 'max:250'],
        ];
    }
}
