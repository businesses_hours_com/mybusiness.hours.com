<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'email'        => 'required|string|email|exists:mysql_backend.users|max:255',
            'password'     => 'required|string|min:4',
        ];
    }
}
