<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'    => 'required|string|max:255',
            'last_name'     => 'required|string|max:255',
            'email'         => 'required|string|email|unique:mysql_backend.users|max:255',
            'company'       => 'required|string|max:255',
            'retailer_type' => 'required|string|in:business_owner,business_employee,marketing_company',
            'password'      => 'required|string|min:6',
            'terms'         => 'required',
        ];
    }
}
