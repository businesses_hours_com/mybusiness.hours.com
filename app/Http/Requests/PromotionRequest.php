<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PromotionRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'hash'    => ['sometimes', Rule::in(auth()->user()->locations()->pluck('hash'))],
        ];
    }
}
