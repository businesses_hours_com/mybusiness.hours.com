<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Services\AdminHoursService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AnswerController extends Controller
{
    protected $adminHoursService;

    public function __construct()
    {
        $this->adminHoursService = new AdminHoursService();
    }

    public function index(Request $request, int $question_id)
    {
        $question = $this->getQuestion($request, $question_id);
        $reports  = $request->user()->reports()->pluck('text', 'id');

        return view('admin.answers.index', [
            'question' => $question,
            'items'    => collect($question->answers),
            'reports'  => $reports
        ]);
    }

    public function create(Request $request, int $question_id)
    {
        $question = $this->getQuestion($request, $question_id);

        return view('admin.answers.form', [
            'question' => $question,
            'create'   => true,
            'item'     => null,
        ]);
    }

    public function edit(Request $request, int $question_id, int $answer_id)
    {
        $question = $this->getQuestion($request, $question_id);
        $answer   = collect($question->answers)->where('id', $answer_id)->first();

        return view('admin.answers.form', [
            'question' => $question,
            'item'     => $answer,
        ]);
    }

    public function store(Request $request, int $question_id)
    {
        $request->merge(['visible' => $request->has('visible') ? 1 : 0]);

        $answer = $this->adminHoursService->baseApi->post('questions/' . $question_id . '/answers', $request->all());

        Auth::user()->clearCache();

        return redirect(route('admin.questions.answers.edit', [$question_id, $answer['id']]));
    }

    public function update(Request $request, int $question_id, int $answer_id)
    {
        $request->merge(['visible' => $request->has('visible') ? 1 : 0]);

        $this->adminHoursService->baseApi->put('questions/' . $question_id . '/answers/' . $answer_id, $request->all());

        Auth::user()->clearCache();

        return redirect(route('admin.questions.answers.edit', [$question_id, $answer_id]));
    }

    public function destroy(int $question_id, int $answer_id)
    {
        $this->adminHoursService->baseApi->delete('questions/' . $question_id . '/answers/' . $answer_id);

        Auth::user()->clearCache();

        return redirect(route('admin.questions.answers.index', $question_id));
    }

    public function report(Request $request, int $question_id, int $answer_id)
    {
        $report_id = intval($request->input('report_id',0));

        if (! $report_id) {
            return null;
        }

        $this->adminHoursService->baseApi->post(
            'questions/' . $question_id . '/answers/' . $answer_id . '/report/' . $report_id,
            $request->only('reported_by')
        );

        Auth::user()->clearCache();

        return redirect(route('admin.questions.answers.index', $question_id));
    }

    private function getQuestion(Request $request, $question_id)
    {
        $location = $request->user()->locations()->filter(function($location) use ($question_id) {
            $questions = collect($location->location_hash->questions);
            return $questions->count() && $questions->where('id', $question_id)->first();
        })->first();

        $questions = optional($location)->location_hash->questions;

        if (! $location || ! $questions) {
            abort(404);
        }

        return collect($questions)->where('id', $question_id)->first();
    }
}
