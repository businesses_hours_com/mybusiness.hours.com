<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use App\Models\Subscription;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Laravel\Cashier\Exceptions\CouponException;
use Laravel\Cashier\Exceptions\PlanNotFoundException;
use Laravel\Cashier\SubscriptionBuilder\RedirectToCheckoutResponse;
use Throwable;

class SubscriptionController extends Controller
{
    /**
     * @throws Throwable
     * @throws CouponException
     * @throws PlanNotFoundException
     */
    public function subscribe(Request $request, ?string $hash = null)
    {
        /** @var User $user */
        $user = $request->user();
        $hashes = Arr::wrap($hash ?? $request->session()->get('hashes'));
        $total = count($hashes) * planPrice('premium');
        Config::set('cashier_plans.plans.premium.amount.value', $total);
        $subscriptionName = $user->getSubscriptionName($hashes);

        if ($user->subscribed($subscriptionName, Subscription::PLAN_PREMIUM)) {
            flash(__('dashboard.already_promoted'))->error();
            return back();
        }

        $subscriptionBuilder = $user->newSubscription($subscriptionName, Subscription::PLAN_PREMIUM);

        if ($coupon = $request->input('coupon')) {
            try {
                $subscriptionBuilder = $subscriptionBuilder->withCoupon($coupon);
            } catch (Exception $e) {
                Log::error($e);
                flash(__('dashboard.invalid_coupon'))->error();
                return back()->with('error', __('dashboard.invalid_coupon'));
            }
        }

        $result = $subscriptionBuilder->create();

        if (is_a($result, RedirectToCheckoutResponse::class)) {
            return $result; // Redirect to Mollie checkout
        }

        flash(__('dashboard.successful_promotion'))->success();

        return back()->with('success', __('dashboard.successful_promotion'));
    }

    public function validateCoupon(Request $request): JsonResponse
    {
        return response()->json([
            'valid' => Coupon::active()->where('code', $request->input('coupon'))->exists()
        ]);
    }
}
