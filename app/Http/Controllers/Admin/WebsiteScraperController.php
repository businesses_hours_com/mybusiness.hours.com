<?php

namespace App\Http\Controllers\Admin;

use App\Services\IconScraper;
use App\Http\Controllers\Controller;
use App\Http\Requests\WebScraperRequest;
use App\Services\LogoScraper;
use App\Services\RequestSource;
use App\Services\SocialScraper;
use Illuminate\Http\JsonResponse;

class WebsiteScraperController extends Controller
{
    public function __invoke(WebScraperRequest $request): JsonResponse
    {
        $htmlStartTime = microtime(true);

        $website        = $request->input('website');
        $html           = (new RequestSource($website))->source();

        $htmlTime = round(microtime(true) - $htmlStartTime, 1);

        $logoStartTime = microtime(true);
        $logoScraper   = new LogoScraper($html);
        $logo          = $logoScraper->scrape();
        $logoTime      = round(microtime(true) - $logoStartTime, 1);

        $socialStartTime = microtime(true);
        $socialScraper   = new SocialScraper($html);
        $social          = $socialScraper->scrape();
        $socialTime      = round(microtime(true) - $socialStartTime, 1);

        $iconStartTime = microtime(true);
        $iconScraper   = new IconScraper($website);
        $icons         = $iconScraper->scrape();
        $iconTime      = round(microtime(true) - $iconStartTime, 1);

        return response()->json(
            [
                'logo'     => $logo,
                'icons'    => $icons,
                'social'   => $social,
                'htmlTime' => $htmlTime,
                'logoTime' => $logoTime,
                'socialTime' => $socialTime,
                'iconTime'   => $iconTime
            ]
        );
    }
}
