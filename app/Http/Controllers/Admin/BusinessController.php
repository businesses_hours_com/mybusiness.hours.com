<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Services\AdminHoursService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class BusinessController extends Controller
{
    protected AdminHoursService $adminHoursService;

    public function __construct(AdminHoursService $adminHoursService)
    {
        $this->adminHoursService = $adminHoursService;
    }

    public function index(Request $request)
    {
        return view('admin.businesses.index', [
            'items' => $request->user()->businesses(),
        ]);
    }

    public function edit(Request $request, int $id)
    {
        $business = $request->user()->businesses()->firstWhere('id', $id);

        if (! $business) {
            abort(404);
        }

        $texts       = $business->texts ?? [];
        $description = array_filter($texts, fn($text) => $text->slug->slug === 'promoted-description');

        return view('admin.businesses.form', [
            'item'        => $business,
            'description' => $description[0] ?? null,
        ]);
    }

    public function update(Request $request, $id)
    {
        $oldBusiness = $request->user()->businesses()->firstWhere('id', $id);
        $business    = $request->all()['step1']['new_business'];

        if ($oldBusiness && $business['name'] !== $oldBusiness->name && $oldBusiness->times_forwarded_this_year >= 2) {
            return back()->withErrors(['msg', 'Too many name changes in a year']);
        }

        $requestData = [
            'id'                   => $id,
            'object'               => 'business',
            'owner_user_id'        => Auth::id(),
            'name'                 => $business['name'],
            'active'               => 1,
            'category_id'          => $business['category']['id'] ?? null,
            'source'               => User::TYPE,
            'is_chain'             => ($business['chain'] ?? false) === 'on' ? 1 : 0,
            'status'               => 'published',
            'website'              => $business['website'] ?? '',
            'logo'                 => $business['logo'],
            'favicon'              => $business['favicon'],
            'promoted_description' => $business['promoted_description'] ?? null,
            'social_profiles'      => $business['social_profiles'] ?? [],
            'country_code'         => config('app.country'),
        ];

        if ($oldBusiness && $oldBusiness->name !== $business['name']) {
            $requestData = array_merge($requestData, ['should_forward' => true]);
        }

        $newBusiness = $this->adminHoursService->addNewBusiness($requestData);

        if ($id = $newBusiness['data'][0]['id'] ?? null) {
            return redirect(route('admin.businesses.edit', $id));
        }

        return back();
    }
}
