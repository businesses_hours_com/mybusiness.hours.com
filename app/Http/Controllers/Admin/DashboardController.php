<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use App\Models\LocationWizard;
use App\Models\User;
use App\Services\WizardService;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Locally\SchemaBuilder\Schema\Exceptions;
use Locally\SchemaBuilder\Schema\PeriodType\DateRange;

class DashboardController extends Controller
{
    public function index(Request $request, string $paymentId = null)
    {
        return redirect(route('admin.locations.index'), 301);

//        $hours = '[{"days": [{"open_status": "open_24"}, {"open_status": "open_24"}, {"open_status": "open_24"}, {"appointment": ["on"], "open_status": "open", "closing_time": ["16:00"], "opening_time": ["07:00"]}, {"appointment": ["on"], "open_status": "open", "closing_time": ["16:00"], "opening_time": ["07:00"]}, {"open_status": "open", "closing_time": ["14:00"], "opening_time": ["09:00"]}, {"appointment": ["on"], "open_status": ["sunday_first", "sunday_second", "sunday_third"], "closing_time": ["10:00"], "opening_time": ["07:00"]}], "period": "11/02/2020 - 25/03/2020", "holidays": {"1": {"name": "newYearsDay", "open_status": "open_regular"}, "2": {"name": "epiphany", "open_status": "closed"}, "3": {"name": "valentinesDay", "open_status": "closed"}, "4": {"name": "carnivalDay", "open_status": "closed"}, "5": {"name": "secondCarnivalDay", "open_status": "closed"}, "6": {"name": "thirdCarnivalDay", "open_status": "closed"}, "7": {"name": "ashWednesday", "open_status": "closed"}, "8": {"name": "summerTime", "open_status": "closed"}, "9": {"name": "goodFriday", "open_status": "closed"}, "10": {"name": "easter", "open_status": "closed"}, "11": {"name": "easterMonday", "open_status": "closed"}, "12": {"name": "kingsDay", "open_status": "closed"}, "13": {"name": "internationalWorkersDay", "open_status": "closed"}, "14": {"name": "commemorationDay", "open_status": "closed"}, "15": {"name": "liberationDay", "open_status": "closed"}, "16": {"name": "mothersDay", "open_status": "closed"}, "17": {"name": "ascensionDay", "open_status": "closed"}, "18": {"name": "pentecost", "open_status": "closed"}, "19": {"name": "pentecostMonday", "open_status": "closed"}, "20": {"name": "fathersDay", "open_status": "closed"}, "21": {"name": "princesDay", "open_status": "closed"}, "22": {"name": "worldAnimalDay", "open_status": "closed"}, "23": {"name": "winterTime", "open_status": "closed"}, "24": {"name": "halloween", "open_status": "closed"}, "25": {"name": "stMartinsDay", "open_status": "closed"}, "26": {"name": "stNicholasDay", "open_status": "closed"}, "27": {"name": "christmasDay", "open_status": "closed"}, "28": {"name": "secondChristmasDay", "open_status": "closed"}}}]';
//
//        $hours = json_decode($hours, true);
//
//        $ws = new WizardService(new LocationWizard());

//        return $ws->formatDepartmentHours( $hours);
    }

    public function changeLocale(string $locale): RedirectResponse
    {
        $locale = strtolower($locale);

        if ($locale !== config('app.locale')) {
            session(['app_locale' => $locale]);
            return back()->withCookie('app_locale', $locale, 24 * 60);
        }

        return back();
    }

    public function changeCountry(string $code): RedirectResponse
    {
        $code = strtolower($code);

        if (Country::active()->where('country_code', $code)->exists() && $code !== config('app.country')) {
            $locale = auth()->user()->getDefaultLocaleCodePerCountry($code);
            session(['app_country' => $code]);
            session(['app_locale' => $locale]);

            return back()
                ->withCookie('app_country', $code, 24 * 60)
                ->withCookie('app_locale', $locale, 24 * 60);
        }

        return back();
    }
}
