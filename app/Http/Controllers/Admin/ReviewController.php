<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Services\AdminHoursService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    protected $adminHoursService;

    public function __construct()
    {
        $this->adminHoursService = new AdminHoursService();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param int|null $location_id
     */
    public function index(Request $request, int $location_id = null)
    {
        $locations = $location_id ? $request->user()->locations()->where('id', $location_id) : $request->user(
        )->locations();
        $businesses = $locations->pluck('business.name', 'business.id');
        $reports = $request->user()->reports()->pluck('text', 'id');

        $items = $locations->filter(function ($location) {
            return count($location->location_hash->reviews ?? []) > 0;
        })->groupBy('business.name');

        return view('admin.reviews.index', [
            'items' => $items,
            'businesses' => $businesses,
            'reports' => $reports,
        ]);
    }

    /**
     * @param int $review_id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function destroy(int $review_id)
    {
        $this->adminHoursService->baseApi->delete('reviews/' . $review_id);

        Auth::user()->clearCache();

        return redirect(route('admin.reviews.index'));
    }

    public function report(Request $request, int $review_id)
    {
        $report_id = intval($request->input('report_id', 0));

        if (!$report_id) {
            return null;
        }

        $this->adminHoursService->baseApi->post(
            'reviews/' . $review_id . '/report/' . $report_id,
            $request->only('reported_by')
        );

        Auth::user()->clearCache();

        return redirect(route('admin.reviews.index'));
    }

    /**
     * Show the form for creating the response.
     *
     * @param Request $request
     * @param int $review_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createResponse(Request $request, int $review_id)
    {
        $review = $this->getReview($request, $review_id);

        return view('admin.reviews.response', [
            'review' => $review,
            'item' => null,
        ]);
    }

    /**
     * Show the form for editing the response
     *
     * @param Request $request
     * @param int $review_id
     * @param int $response_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editResponse(Request $request, int $review_id, int $response_id)
    {
        $review = $this->getReview($request, $review_id);
        $response = collect($review->responses)->where('id', $response_id)->first();

        return view('admin.reviews.response', [
            'review' => $review,
            'item' => $response,
        ]);
    }

    /**
     * @param Request $request
     * @param int $review_id
     * @param int $response_id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateResponse(Request $request, int $review_id, int $response_id)
    {
        $this->adminHoursService->baseApi->put('reviews/' . $review_id . '/responses/' . $response_id, $request->all());

        Auth::user()->clearCache();

        return redirect(route('admin.reviews.responses.edit', [$review_id, $response_id]));
    }

    /**
     * @param Request $request
     * @param int $review_id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function storeResponse(Request $request, int $review_id)
    {
        $response = $this->adminHoursService->baseApi->post('reviews/' . $review_id . '/responses', $request->all());

        Auth::user()->clearCache();

        return redirect(route('admin.reviews.responses.edit', [$review_id, $response['id']]));
    }

    /**
     * @param Request $request
     * @param int $review_id
     * @return mixed
     */
    private function getReview(Request $request, int $review_id)
    {
        $location = $request->user()->locations()->filter(function ($location) use ($review_id) {
            $reviews = collect($location->location_hash->reviews);
            return $reviews->count() && $reviews->where('id', $review_id)->first();
        })->first();

        $reviews = optional($location)->location_hash->reviews;

        if (!$location || !$reviews) {
            abort(404);
        }

        return collect($reviews)->where('id', $review_id)->first();
    }
}
