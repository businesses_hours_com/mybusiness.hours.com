<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use App\Services\AdminHoursService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    protected AdminHoursService $adminHoursService;

    public function __construct(AdminHoursService $adminHoursService)
    {
        $this->adminHoursService = $adminHoursService;
    }

    public function edit(User $user)
    {
        $this->authorize('update', $user);

        return view('admin.users.form', [
            'item' => $user,
        ]);
    }

    public function update(UserUpdateRequest $request, User $user): RedirectResponse
    {
        $this->authorize('update', $user);

        if (
            $request->filled('current_password') &&
            ! Hash::check($request->input('current_password'), auth()->user()->password)
        ) {
            throw ValidationException::withMessages([
                'current_password' => 'Incorrect password'
            ]);
        }

        $user->update($request->validated());

        return back()->with('success','Successfully saved.');
    }
}
