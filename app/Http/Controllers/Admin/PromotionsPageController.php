<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PromotionRequest;
use Illuminate\Support\Arr;

class PromotionsPageController extends Controller
{
    public function index(PromotionRequest $request)
    {
        $hashes = Arr::wrap($request->input('hashes', $request->input('hash', [])));

        if ($hashes) {
            $request->session()->put('hashes', $hashes);
        }

        // if multiple hashes sent via POST, store in session and redirect again to promotions
        if ($request->isMethod('POST')) {
            return redirect(route('admin.promotions'));
        }

        $sessionHashes = $request->session()->get('hashes', []);

        return view('admin.promotions.index', [
            'hashes'         => $sessionHashes,
            'numOfLocations' => count($sessionHashes),
            'monthlyPrice'   => planPrice('premium', 'monthly'),
            'yearlyPrice'    => planPrice('premium', 'yearly'),
        ]);
    }
}
