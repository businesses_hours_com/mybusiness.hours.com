<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AppliedCoupon;
use App\Models\Coupon;
use App\Models\LocationWizard;
use App\Models\Subscription;
use App\Models\User;
use App\Services\AdminHoursService;
use App\Services\LocationImporter;
use App\Services\WizardService;
use Exception;
use Honeybadger\HoneybadgerLaravel\Facades\Honeybadger;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Laravel\Cashier\Order\Order;
use Laravel\Cashier\Payment;
use Symfony\Component\HttpKernel\Exception\HttpException;

class LocationController extends Controller
{
    protected AdminHoursService $adminHoursService;
    protected LocationImporter $locationImportService;

    public function __construct(AdminHoursService $adminHoursService, LocationImporter $locationImportService)
    {
        $this->adminHoursService = $adminHoursService;
        $this->locationImportService = $locationImportService;
    }

    public function index(Request $request, string $paymentId = null)
    {
        /** @var User $user */
        $user = $request->user();

        Cookie::queue(
            Cookie::forget('wizard_id')
        );

        $locations = $user->locations()->groupBy('business.name');

        if ($locations->isEmpty()) {
            return redirect(route('admin.location-wizard.index'));
        }

        if ($paymentId && ($payment = Payment::findByPaymentId($paymentId))) {
            $order = $payment->order;

            $orderable = $order?->items()?->where('orderable_type', Subscription::class)?->first()?->orderable ?? null;

            /** @var ?Subscription $subscription */
            $subscription = match (true) {
                $orderable instanceof Subscription => $orderable,
                $orderable instanceof AppliedCoupon => $orderable->model,
                default => null,
            };

            [, $hash] = explode(';', $subscription->name ?? ';');

            if ($hash) {
                $this->adminHoursService->promoteLocation(['hash' => $hash]);
            }

            if ($subscription) {
                flash(__('dashboard.promotion_success_message'))->success();
                $messageArray = ['success' => __('dashboard.promotion_success_message')];

                if ($subscription instanceof Subscription) {
                    foreach ($subscription->redeemedCoupons()->pluck('name')->toArray() as $couponCode) {
                        Coupon::query()->where('code', $couponCode)->increment('usage_counter');
                    }
                }
            } else {
                flash(__('dashboard.promotion_error_message'))->error();
                $messageArray = ['error' => __('dashboard.promotion_error_message')];
                Log::error('Promotion error', [
                    'payment_id' => $paymentId,
                    'orderable' => $orderable,
                ]);
            }

            $request->session()->remove('hashes');
        }

        return view('admin.locations.index', [
            'items' => $locations
        ])->with($messageArray ?? []);
    }

    public function edit(Request $request, string $hash)
    {
        $location = $request->user()->locations()->firstWhere('hash', $hash);

        if (!$location) {
            abort(404);
        }

        $wizard = LocationWizard::fromHash($location->hash);

        return view('admin.locations.edit-all', [
            'item' => $location,
            'wizard' => $wizard,
            'isPartOfChain' => $location->business->is_chain ?? 0,
            'step3' => optional($wizard)->data['step3'] ?? [],
            'times' => optional($wizard)->data['step3']['times'],
            'address' => (array)$location->address ?? [],
            'contact' => (array)$location->meta_schema ?? []
        ]);
    }

    public function editAddress(Request $request, string $hash)
    {
        $location = $request->user()->getLocation($hash);

        if (!$location) {
            Auth::logout();
            return redirect(route('login'));
        }

        return view('admin.locations.address', [
            'item' => $location,
            'address' => (array)$location->address ?? [],
            'contact' => (array)$location->meta_schema ?? [],
            'showLocalFields' => request()->user()->isAdmin() || ($location->business->is_chain ?? 0),
        ]);
    }

    public function editHours(Request $request, string $hash)
    {
        $location = $request->user()->getLocation($hash);

        if (!$location) {
            Auth::logout();
            return redirect(route('login'));
        }

        $locationArray = json_decode(json_encode($location), true) ?: [];

        $wizard = $request->user()->locations()->firstWhere('hash', $hash) ?
            LocationWizard::fromHash($location->hash) :
            $this->locationImportService->instantiateWizardFromLocation($locationArray);

        $step3 = $wizard->data['step3'] ?? [];

        return view('admin.locations.hours', [
            'item' => $location,
            'step3' => $step3,
            'times' => $step3['times'] ?? [],
        ]);
    }

    public function editImages(Request $request, string $hash)
    {
        $location = $request->user()->getLocation($hash);

        if (!$location) {
            Auth::logout();
            return redirect(route('login'));
        }

        return view('admin.locations.images', [
            'item' => $location,
            'images' => (array)$location->location_hash->files ?? [],
        ]);
    }

    public function editTexts(Request $request, string $hash)
    {
        $location = $request->user()->getLocation($hash);

        if (!$location) {
            Auth::logout();
            return redirect(route('login'));
        }

        $texts = $location->location_hash->texts ?? [];
        $description = array_filter($texts, fn($text) => $text->slug->slug === 'promoted-description');

        return view('admin.locations.texts', [
            'item' => $location,
            'description' => $description[0] ?? null,
        ]);
    }

    public function editReviews(Request $request, string $hash)
    {
        return $this->editAddress($request, $hash);
    }

    public function editQa(Request $request, string $hash)
    {
        return $this->editAddress($request, $hash);
    }

    public function update(Request $request, string $hash)
    {
        $location = $request->user()->getLocation($hash);

        if (!$location) {
            Auth::logout();
            return redirect(route('login'));
        }

        $locationArray = json_decode(json_encode($location), true) ?: [];
        $newAddressId = $request->input('step2.new_location.address.address_id');
        $hasDifferentAddress = $newAddressId && (intval($location->address->id) !== intval($newAddressId));
        $redirectFromHash = $hasDifferentAddress ? $location->hash : null;
        $ownLocation = $request->user()->locations()->firstWhere('hash', $hash);

        $validator = Validator::make(
            $request->all(),
            [
                'step2.new_location.email' => ['sometimes', 'nullable', 'email'],
                'step2.new_location.phone' => ['sometimes', 'nullable', 'digits_between:6,15'],
                'step2.new_location.fax' => ['sometimes', 'nullable', 'digits_between:6,15'],
            ], [],
            [
                'step2.new_location.email' => 'email',
                'step2.new_location.phone' => 'phone',
                'step2.new_location.fax' => 'fax',
            ]
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        try {
            if ($ownLocation) {
                $wizard = $this->updateWizard($location->hash, $request->all());
            } else {
                $wizard = $this->locationImportService
                    ->instantiateWizardFromLocation($locationArray)
                    ->mergeNewData($request->all());
            }

            $wizardService = new WizardService($wizard);
            $requestData = $wizardService->getFormattedRequestDataArray(
                $location->business_id,
                $location->hash,
                $redirectFromHash
            );

            // admin can edit any location
            if (!$ownLocation) {
                $requestData['external_provider_id'] = $location->location_version->external_provider_id ?? null;
                $requestData['source'] = $location->source;
                $requestData['country_code'] = $location->country_code;
                $requestData['publish_now'] = true;
                unset($requestData['user_id']);
            }

            $newLocation = $wizardService->addNewLocation($requestData, (bool)$ownLocation);
        } catch (Exception $e) {
            Honeybadger::notify($e, $request);
            Log::error($e);
            $genericError = 'There was an error during the saving process, please try again or contact us for support.';

            if ($e->getCode() === Response::HTTP_UNPROCESSABLE_ENTITY) {
                $error = json_decode($e->getMessage(), true);
                flash($error['message'][0] ?? $error['message'] ?? $e->getMessage() ?? $genericError)->error();
                return back();
            }

            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, $genericError);
        }

        if ($newLocation ?? false) {
            $isEditAllRoute = back()->getTargetUrl() === route('admin.locations.edit', $location->hash);
            $routeNamePart = $isEditAllRoute ? 'edit' : ($request->has('step2') ? 'address' : 'hours');

            return redirect(route('admin.locations.' . $routeNamePart, $newLocation['hash']));
        }

        return back();
    }


    public function updateImages(Request $request, string $hash)
    {
        $files = [];

        foreach ($request->allFiles()['files'] ?? [] as $file) {
            $blob = file_get_contents($file);
            $files[] = [
                'data' => 'data:image/' . $file->extension() . ';base64,' . base64_encode($blob),
                'name' => $request->input('name'),
                'slug' => $request->input('name'),
                'country_code' => config('app.country'),
                'type' => User::TYPE,
                'visible' => 1,
            ];
        }

        Auth::user()->clearCache(USER::CACHE_LOCATIONS);

        $result = $this->adminHoursService->baseApi->post('kc/files', [
            'hash' => $hash,
            'files' => $files
        ]);

        if ($request->ajax()) {
            return response()->json($result);
        }

        return back();
    }

    public function updateTexts(Request $request, string $hash)
    {
        $result = $this->adminHoursService->baseApi->post('kc/texts', [
            'hash' => $hash,
            'texts' => $request->input('texts'),
        ]);

        Auth::user()->clearCache(USER::CACHE_LOCATIONS);

        if ($request->ajax()) {
            return response()->json($result);
        }

        return back();
    }

    public function updateWizard(string $hash, array $requestData): LocationWizard
    {
        $wizard = LocationWizard::fromHash($hash);
        $wizard->mergeNewData($requestData);
        $wizard->user_id = auth()->id();
        $wizard->save();

        return $wizard;
    }

    public function destroy(string $hash): RedirectResponse
    {
        $this->adminHoursService->deleteLocation($hash);

        Auth::user()->clearCache(USER::CACHE_LOCATIONS);

        return redirect(route('admin.locations.index'));
    }

    public function destroyImage(Request $request, string $hash, int $fileId): RedirectResponse
    {
        $this->adminHoursService->deleteFile(['file_ids' => [$fileId]]);

        Auth::user()->clearCache(USER::CACHE_LOCATIONS);

        return back();
    }

}
