<?php

namespace App\Http\Controllers\Admin;

use App\Mail\SupportEmail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    const LOCALE_TO_ADDRESS_MAP = [
        'nl' => 'contact@openingstijden.nl',
    ];

    public function index()
    {
        return view('admin.contact.index');
    }

    public function send(Request $request): RedirectResponse
    {
        $request->validate(['email' => 'required|email']);

        $toAddress = static::LOCALE_TO_ADDRESS_MAP[config('app.locale')] ?? config('mail.from.address');

        Mail::to($toAddress)->send(
            new SupportEmail($request->only('name', 'email', 'about', 'text'))
        );

        flash('Successfully sent email! We\'ll get back to you soon.')->success();

        return back();
    }
}
