<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Services\AdminHoursService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class QuestionController extends Controller
{
    protected $adminHoursService;

    public function __construct()
    {
        $this->adminHoursService = new AdminHoursService();
    }

    public function index(Request $request, int $location_id = null)
    {
        $locations  = $location_id ? $request->user()->locations()->where('id', $location_id) : $request->user()->locations();
        $businesses = $locations->pluck('business.name', 'business.id');
        $reports    = $request->user()->reports()->pluck('text', 'id');

        $items = $locations->filter(function($location) {
            return count($location->location_hash->questions ?? []) > 0;
        })->groupBy('business.name');

        return view('admin.questions.index', [
            'items'      => $items,
            'businesses' => $businesses,
            'reports'    => $reports,
        ]);
    }

    public function destroy(int $question_id)
    {
        $this->adminHoursService->baseApi->delete('questions/' . $question_id);

        Auth::user()->clearCache();

        return redirect(route('admin.questions.index'));
    }

    public function report(Request $request, int $question_id)
    {
        $report_id = intval($request->input('report_id', 0));

        if (! $report_id) {
            return null;
        }

        $this->adminHoursService->baseApi->post(
            'questions/' . $question_id . '/report/' . $report_id,
            $request->only('reported_by')
        );

        Auth::user()->clearCache();

        return redirect(route('admin.questions.index'));
    }

    public function verifiedFAQ(Request $request)
    {
        dd($request->only('question','answer'));
    }

}
