<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use App\Models\User;
use App\Services\LocationImporter;
use Exception;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Models\LocationWizard;
use App\Services\WizardService;
use App\Services\AddressService;
use Illuminate\Http\JsonResponse;
use App\Services\AdminHoursService;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;

class LocationWizardController extends Controller
{
    protected AdminHoursService $adminHoursService;
    protected WizardService     $wizardService;
    protected AddressService    $addressService;
    protected LocationImporter  $locationImporter;

    public function __construct(AdminHoursService $adminHoursService, AddressService $addressService, LocationImporter $locationImporter)
    {
        $this->adminHoursService = $adminHoursService;
        $this->addressService    = $addressService;
        $this->locationImporter  = $locationImporter;

        // workaround to accessing user session in constructor
        $this->middleware(function($request, $next) {
            $wizard              = $request->user()->wizards()->draft()->first();
            $this->wizardService = new WizardService($wizard);
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        if ($hash = $request->input('hash')) {
            $wizard = $this->locationImporter->import($hash);
            return redirect(route('admin.locations.index'));
        }
        else {
            $wizard = LocationWizard::query()->find($request->cookie('wizard_id'));
        }

        $selectedLocations = array_fill_keys($request->user()->locations()->pluck('id')->toArray(), 'on');

        $step1              = $wizard->data['step1'] ?? [];
        $step1['locations'] = ($step1['locations'] ?? []) + $selectedLocations;

        return view('admin.location-wizard.index', [
            'wizard'      => $wizard,
            'currentStep' => $wizard->data['current_step'] ?? 1,
            'item'        => null,
            'step1'       => $step1,
            'step2'       => $wizard->data['step2'] ?? [],
            'step3'       => $wizard->data['step3'] ?? [],
            'times'       => [
                'type'        => $wizard->data['step3']['times']['type'] ?? 'regular',
                'regular'     => $wizard->data['step3']['times']['regular'] ?? [],
                'seasonal'    => $wizard->data['step3']['times']['seasonal'] ?? ['new'],
                'departments' => $wizard->data['step3']['times']['departments'] ?? ['new'],
            ],
        ]);
    }

    public function addLocation(Request $request)
    {
        $wizard = LocationWizard::query()->create([
            'user_id' => auth()->id(),
            'data'    => [
                'current_step' => 2,
                'step1'        => [
                    'business' => [
                        'id'   => $request->input('business_id'),
                        'name' => $request->input('business_name'),
                    ]
                ]
            ],
        ]);

        return redirect(route('admin.location-wizard.index'))->cookie('wizard_id', $wizard->id, 60);
    }

    public function loadBusinesses(Request $request): array
    {
        $params = $request->merge([
            'country_code' => config('app.country'),
        ])->all();

        return $this->adminHoursService->getBusinesses($params);
    }

    public function loadLocations(Request $request): array
    {
        $params = $request->merge([
            'country_code' => config('app.country'),
            'limit'        => 10000,
        ])->all();

        $shouldObfuscateEmail = $request->input('obfuscate_emails') === 'true';
        $locations            = $this->adminHoursService->getLocations($params);
        $locations            = $locations['data'] ?? [];

        if (empty($locations) || ! $shouldObfuscateEmail) {
            return $locations;
        }

        return array_map(function($location) {
            $users = $location['users'] ?? [];

            if (empty($users)) {
                return $location;
            }

            foreach ($users as &$user) {
                $user['email'] = User::obfuscateEmail($user['email']);
            }

            $location['users'] = $users;

            return $location;
        }, $locations);
    }

    public function loadHolidays(): Collection
    {
        return $this->adminHoursService->getHolidaysAsCollection([
            'country_code' => config('app.country'),
            'show_retail'  => 1,
            'current_year' => 1,
        ]);
    }

    public function loadCategories(Request $request): array
    {
        $data = $request->merge([
            'active'       => 1,
            'country_code' => config('app.country'),
        ])->all();

        return $this->adminHoursService->getCategories($data);
    }

    /**
     * @throws RequestException
     */
    public function resolveAddress(Request $request): ?array
    {
        $data = [
            'country_code'         => config('app.country'),
            'street'               => $request->input('street'),
            'number'               => $request->input('house_number'),
            'addition'             => $request->input('addition'),
            'zipcode'              => $request->input('postal_code'),
            'city'                 => $request->input('city'),
            'province'             => $request->input('province'),
            'geo_lat'              => $request->input('lat'),
            'geo_lng'              => $request->input('lng'),
            'google_place_details' => $request->input('google_place_details'),
        ];

        return $this->addressService->getAddressWithID($data);
    }

    public function saveProgress(Request $request): JsonResponse
    {
        $data = $this->validateData($request);

        $wizard_id = intval($request->cookie('wizard_id'));
        $wizard    = $this->wizardService->saveProgress($data, $wizard_id);

        return response()->json([
            'id' => $wizard->id,
        ])->cookie('wizard_id', $wizard->id, 60);
    }

    public function destroyProgress(Request $request): JsonResponse
    {
        $wizard_id = $request->cookie('wizard_id');
        optional(LocationWizard::query()->find($wizard_id))->delete();

        return response()->json([
            'id' => $wizard_id
        ])->cookie('wizard_id', null, 60);
    }

    public function store(Request $request): JsonResponse
    {
        $submitType = $request->input('submit_type');

        if ($submitType === 'manage') {
            $this->wizardService->manageSelectedLocations();

            return response()->json([
                'href' => route('admin.locations.index')
            ])->cookie('wizard_id', null, 60);
        }

        try {
            $this->wizardService->storeWizardData();
        }
        catch (Exception $e) {
            Log::error($e);
            if ($e->getCode() === Response::HTTP_UNPROCESSABLE_ENTITY) {
                return response()->json([
                    'error' => $e->getMessage(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, 'There was an error during the saving process, please try again or contact us for support.');
        }

        if ($submitType === 'new-location') {
            $newWizard = $this->wizardService->createNewWizard(2);

            return response()->json([
                'id'   => $newWizard->id,
                'href' => route('admin.location-wizard.index'),
            ])->cookie('wizard_id', $newWizard->id, 60);
        }

        return response()->json([
            'href' => route('admin.promotions', ['hash' => $this->wizardService->wizard->kc_location_hash])
        ])->cookie('wizard_id', null, 60);
    }

    private function validateData(Request $request): array
    {
        $data = [];
        parse_str($request->input('data'), $data);
        $data = WizardService::array_filter_recursive($data);

        $currentStep    = intval($request->input('current_step'));
        $rules          = $this->getValidationRules($currentStep);
        $attributeNames = $this->getValidationAttributeNames($rules);

        $validator = Validator::make($data, $rules, [], $attributeNames);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        if ($errors = $this->validateFromApi($currentStep, $data)) {
            throw ValidationException::withMessages($errors);
        }

        $data['current_step'] = $currentStep;
        $data['user_id']      = $request->input('user_id', auth::id());
        unset($data['_token'], $data['locations-table_length']);
        $data['data'] = $data;

        return $data;
    }

    private function getValidationRules(int $step): array
    {
        switch ($step) {
            case 2:
                $rules = [
                    'step2.new_location.address.postal_code'  => ['required'],
                    'step2.new_location.address.house_number' => ['max:16'],
                    'step2.new_location.email'                => ['sometimes', 'email'],
                    'step2.new_location.phone'                => ['sometimes', 'digits_between:6,15'],
                    'step2.new_location.fax'                  => ['sometimes', 'digits_between:6,15'],
                ];

                if(config('app.country') !== Country::UK){
                    $rules['step2.new_location.address.house_number'][] = 'required';
                }

                return $rules;
            case 1:
            case 3:
            default:
                return [];
        }
    }

    // Transform the names into readable names, e.g. email instead of step2.new_location.email
    private function getValidationAttributeNames(array $rules): array
    {
        $keys = array_keys($rules);

        $values = array_map(function($key) {
            $keyArray = explode('.', $key);
            $lastKey  = end($keyArray);
            return str_replace('_', ' ', $lastKey);
        }, $keys);

        return array_combine($keys, $values);
    }

    private function validateFromApi(int $step, array $data): ?array
    {
        $newBusinessName = $data['step1']['new_business']['name'] ?? null;

        if ($step === 1 && $newBusinessName) {
            $errors = $this->adminHoursService->validateBusiness([
                'name'        => $newBusinessName,
                'category_id' => $data['step1']['new_business']['category']['id'] ?? null,
            ]);

            if (! $errors) {
                return $errors;
            }

            $errors['category_id'] = str_replace('id', '', $errors['category_id'] ?? null);

            return [
                'step1.new_business.name'        => $errors['name'] ?? null,
                'step1.new_business.category.id' => $errors['category_id'] ?? null,
            ];
        }

        return [];
    }
}
