<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/locations';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'country'       => 'required|string|exists:mysql_backend.country_codes,country_code',
            'first_name'    => 'required|string|max:50',
            'last_name'     => 'required|string|max:50',
            'email'         => 'required|string|email|unique:mysql_backend.users|max:255',
            'company'       => 'required|string|max:50',
            'retailer_type' => 'required|string|in:business_owner,business_employee,marketing_company',
            'password'      => 'required|string|min:6',
            'terms'         => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     */
    protected function create(array $data): User
    {
        /** @var User $user */
        $user = User::query()->create([
            'country_codes'     => json_encode([$data['country']]),
            'name'              => $data['first_name'],
            'last_name'         => $data['last_name'],
            'company'           => $data['company'],
            'type'              => User::TYPE,
            'retailer_type'     => $data['retailer_type'],
            'email'             => $data['email'],
            'password'          => Hash::make($data['password']),
            'email_verified_at' => intval($data['verify'] ?? 1) ? null : now(),
        ]);

        return $user;
    }
}
