<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;

class Authenticate extends Middleware
{
    public function handle($request, Closure $next, ...$guards)
    {
        /** @var User $originalUser */
        $originalUser = User::query()->find(Cookie::get('original_user_id')) ?? $request->user();

        // impersonate another user
        if ($originalUser && $originalUser->isAdmin() && $request->input('impersonate_id')) {
            /** @var User $user */
            $user = User::query()
                ->withoutGlobalScope('countryCode')
                ->find($request->input('impersonate_id'));

            Auth::login($user, true);

            if ($user) {
                Cookie::queue('app_locale', $user->getDefaultLocaleCodePerCountry($user->country_code), 24 * 60);
                Cookie::queue('app_country', $user->country_code, 24 * 60);
                Cookie::queue('original_user_id', $originalUser->id, 24 * 60);

                $request->session()->regenerate();
            }
        }

        return parent::handle($request, $next, $guards);
    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        }
    }
}
