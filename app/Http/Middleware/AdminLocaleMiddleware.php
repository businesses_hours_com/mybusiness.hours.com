<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminLocaleMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if (! auth()->check()) {
            return $next($request);
        }

        $locale = session('app_locale') ?? $request->cookie('app_locale');

        if ($locale && ($locale !== config('app.locale'))) {
            config([
                'app.locale' => $locale,
            ]);
        }

        return $next($request);
    }
}
