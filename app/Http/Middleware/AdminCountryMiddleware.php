<?php

namespace App\Http\Middleware;

use Closure;

class AdminCountryMiddleware
{
    public function handle($request, Closure $next)
    {
        if (! auth()->check()) {
            return $next($request);
        }

        $countryCode = session('app_country') ?? $request->cookie('app_country');

        if ($countryCode && ($countryCode !== config('app.country'))) {
            config([
                'app.country' => $countryCode,
                'app.locale'  => auth()->user()->getDefaultLocaleCodePerCountry($countryCode) ?? config('app.locale'),
            ]);
        }

        return $next($request);
    }
}
