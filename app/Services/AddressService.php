<?php

namespace App\Services;

use Exception;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Throwable;

class AddressService
{
    /**
     * @throws RequestException
     */
    public function resolveAddress(array $query): ?array
    {
        $countryCode = config('app.country');

        try {
            $responseArray = Http::asJson()
                ->withoutVerifying()
                ->acceptJson()
                ->get(config('services.address_api_proxy_url') . $countryCode . '/search', $query)
                ->throw()
                ->json();

            if ($error = $responseArray['error'] ?? null) {
                throw new Exception($error, Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            $data = [
                'street' => $responseArray['street'],
                'number' => $responseArray['houseNumber'],
                'addition' => $responseArray['houseNumberAddition'],
                'zipcode' => $responseArray['postalCode'],
                'city' => $responseArray['city'],
                'province' => $responseArray['province'],
                'geo_lat' => $responseArray['lat'],
                'geo_lng' => $responseArray['lng'],
                'country_code' => $countryCode,
            ];

            return $this->getAddressWithID($data);
        } catch (Throwable $e) {
            Log::error('AddressService@resolveAddress error', [
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
                'countryCode' => $countryCode,
                'query' => json_encode($query),
                'data' => json_encode($data),
            ]);
            throw $e;
        }
    }

    public function getAddressWithID(array $data, string $loggingIdentifier = null): ?array
    {
        try {
            return Http::asJson()
                ->withoutVerifying()
                ->acceptJson()
                ->post(config('services.address_api_proxy_url'), $data)
                ->throw()
                ->json();
        } catch (Exception $e) {
            $msg = $e->getMessage();

            if ($e instanceof RequestException) {
                $msg = json_decode((string)optional($e->response)->getBody(), true)['error'] ?? $msg;
            }

            Log::error(($loggingIdentifier ?? 'error') . ' / ' . $msg . ' / json: ' . json_encode($data));

            throw $e;
        }
    }
}
