<?php

namespace App\Services;

use Illuminate\Support\Carbon;

class RequestSource
{
    private string          $userAgent    = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:51.0) Gecko/20100101 Firefox/51.0';
    private string          $cacheTag     = '';
    private ?string         $source       = null;
    private ?string         $lastModified = null;
    private bool            $noCache      = false;
    private ScrapperRequest $scrapperRequest;
    private ?string         $requestUrl   = null;

    public static function stashCacheTag(string $cacheTag): string
    {
        $url = self::url() . '/api/crawl/source?cache-tag=' . $cacheTag . '&stash=true';
        return file_get_contents($url);
    }

    public function __construct($input, bool $noCache = false)
    {
        if ($input instanceof ScrapperRequest) {
            $this->scrapperRequest = $input;
        }
        else if (is_string($input) && ! empty($input)) {
            $this->scrapperRequest = new ScrapperRequest($input);
        }

        $this->noCache = $noCache;
    }

    public static function url(): string
    {
        return config('services.crawler.url');
    }

    public function getUrl(): string
    {
        if (is_string($this->requestUrl)) {
            return $this->requestUrl;
        }
        return $this->scrapperRequest->url();
    }

    public function setSource(string $source): RequestSource
    {
        $this->source = $source;
        if (empty($this->lastModified)) {
            $this->lastModified = date('Y-m-d');
        }

        return $this;
    }

    private function didRequestSource(): bool
    {
        return $this->source !== null;
    }

    public function source(): ?string
    {
        if (empty($this->scrapperRequest) && ! $this->didRequestSource()) {
            $this->source = '';
        }

        if ($this->didRequestSource()) {
            return $this->source;
        }

        preg_match('/cache-tag=([a-zA-Z0-9=]+)/', urldecode($this->scrapperRequest->url()), $inputMatch);
        if (! empty($inputMatch[1]) && stripos($this->scrapperRequest->url(), RequestSource::url()) === 0) {
            $curlUrl = RequestSource::url() . '/api/crawl/source?cache-tag=' . $inputMatch[1];
        }
        else {
            $curlUrl = RequestSource::url() . '/api/crawl/source' . ($this->noCache ? '?no-cache=true' : '');
        }

        $httpHeaders   = $this->scrapperRequest->getCustomHeaders();
        $httpHeaders[] = 'Url: ' . $this->scrapperRequest->url();
        $httpHeaders[] = 'User-Agent: ' . $this->userAgent;

        $ch      = curl_init();
        $headers = [];
        curl_setopt($ch, CURLOPT_URL, $curlUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeaders);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,  5);
        curl_setopt($ch, CURLOPT_TIMEOUT,  10);
        curl_setopt($ch, CURLOPT_VERBOSE,  true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADERFUNCTION,
            function($curl, $header) use (&$headers) {
                $len    = strlen($header);
                $header = explode(':', $header, 2);
                if (count($header) < 2) // ignore invalid headers
                    return $len;

                $name = trim($header[0]);
                if (! array_key_exists($name, $headers)) {
                    $headers[$name] = [trim($header[1])];
                }
                else {
                    $headers[$name][] = trim($header[1]);
                }

                return $len;
            }
        );
        $this->scrapperRequest->updateCurlRequest($ch);
        $this->source = curl_exec($ch);

        $items = [];
        for ($char = 0; $char < 32; $char++) {
            $items[] = chr($char);
        }

        // Could break strange e-umlaut chars
        //for ($char = 128; $char <= 255; $char++) {
        //$items[] = chr($char);
        //}

        $this->source = str_replace($items, ' ', $this->source);
        $this->source = str_replace('&nbsp;', ' ', $this->source);
        $this->source = preg_replace_callback('/(&#[0-9]+;)/', function($m) {
            return mb_convert_encoding($m[1], 'UTF-8', 'HTML-ENTITIES');
        }, $this->source);

        if (! empty($headers['Last-Modified'])) {
            $this->lastModified = Carbon::parse($headers['Last-Modified'][0])->tz('Europe/Amsterdam')->toDateTimeString();
        }
        else {
            $this->lastModified = date('Y-m-d');
        }

        if (! empty($headers['Cache-Tag'])) {
            $this->cacheTag = $headers['Cache-Tag'][0];

            if (! empty($headers['Request-Url'])) {
                $this->requestUrl = $headers['Request-Url'][0];
            }
        }

        return $this->source;
    }

    /**
     * @return int
     */
    public function getLastModifiedTimestamp(): int
    {
        return strtotime($this->lastModified);
    }

    /**
     * @return string
     */
    public function getCacheTag(): string
    {
        if (! $this->didRequestSource()) {
            $this->source();
        }

        return $this->cacheTag;
    }
}
