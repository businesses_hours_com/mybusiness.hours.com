<?php

namespace App\Services;

use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

abstract class BaseScraper
{
    abstract function scrape(): array;

    protected function formatIconArray(?string $name): array
    {
        if (! $name) {
            return [];
        }

        try {
            $url          = Storage::disk('public')->url($name);
            $size         = @getimagesize($url);
            $size         = $size[0] ?? 32;
            $type         = Storage::disk('public')->mimeType($name);
            $fileContents = @file_get_contents($url);

            if (! $fileContents) {
                throw new Exception('file_get_contents error', Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return [
                'url'  => $url,
                'name' => $name,
                'size' => $size,
                'type' => $type,
                'file' => 'data:image/' . $type . ';base64,' . base64_encode($fileContents),
            ];
        }
        catch (\Exception $e) {
            Log::error($e . "\n details: \n" . json_encode([
                    'url'  => $url ?? '',
                    'name' => $name,
                    'size' => $size ?? '',
                    'type' => $type ?? '',
                ])
            );

            return [];
        }
    }
}
