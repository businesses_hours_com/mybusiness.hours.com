<?php

namespace App\Services\CashierMollie;

use Laravel\Cashier\Coupon\FixedDiscountHandler as CoreFixedDiscountHandler;
use Money\Money;

class PercentageDiscountHandler extends CoreFixedDiscountHandler
{
    protected function unitPrice(Money $base): Money
    {
        $discount = mollie_array_to_money($this->context('discount'));
        $discount = $base->multiply($discount->getAmount() * 0.01 / 100);

        if ($this->context('allow_surplus', false) && $discount->greaterThan($base)) {
            return $base->negative();
        }

        return $discount->negative();
    }
}
