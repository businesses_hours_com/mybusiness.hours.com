<?php

namespace App\Services\CashierMollie;

use App\Models\Coupon;
use Laravel\Cashier\Coupon\Coupon as CashierCoupon;
use Laravel\Cashier\Exceptions\CouponNotFoundException;
use Laravel\Cashier\Coupon\Contracts\CouponRepository;

class DatabaseCouponRepository implements CouponRepository
{
    public function find(string $coupon): ?CashierCoupon
    {
        /** @var Coupon $couponModel */
        $couponModel = Coupon::query()->where('code', $coupon)->first();

        return optional($couponModel)->buildCashierCoupon();
    }

    public function findOrFail(string $coupon): CashierCoupon
    {
        if (! $cashierCoupon = self::find($coupon)) {
            throw new CouponNotFoundException;
        }

        return $cashierCoupon;
    }
}
