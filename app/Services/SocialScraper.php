<?php

namespace App\Services;

use KubAT\PhpSimple\HtmlDomParser;

class SocialScraper extends BaseScraper
{
    protected $dom;
    protected array $supportedSocialNetworks = [
        'facebook',
        'twitter',
        'instagram',
        'pinterest',
        'linkedin',
        'foursquare',
    ];

    public function __construct(string $html)
    {
        $this->dom = HtmlDomParser::str_get_html($html, false, null, 0);
    }

    public function scrape(): array
    {
        $socialLinks = [];

        if (!$this->dom) {
            return $socialLinks;
        }

        foreach ($this->dom->find('a') as $link) {
            foreach ($this->supportedSocialNetworks as $socialNetwork) {
                if (strpos($link->href, $socialNetwork) !== false) {
                    $socialLinks[$socialNetwork] = $link->href;
                }
            }
        }

        return $socialLinks;
    }
}
