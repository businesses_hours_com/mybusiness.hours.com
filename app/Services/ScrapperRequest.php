<?php

namespace App\Services;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;

class ScrapperRequest implements Arrayable, Jsonable
{
    private string $url;
    private string $method        = 'GET';
    private array  $customHeaders = [];

    /**
     * @var mixed
     */
    private $data = null;

    /**
     * @var int
     */
    private $depth = -1;

    private $analyse = null;

    public function __construct(string $url, $depth = -1, $analyse = null)
    {
        $this->url     = $url;
        $this->depth   = $depth;
        $this->analyse = $analyse;

        return $this;
    }

    public static function builder(string $url, $depth = -1, $analyse = null)
    {
        return new self($url, $depth, $analyse);
    }

    /**
     * @param mixed $data
     * @return $this
     */
    public function post($data)
    {
        $this->method = 'POST';
        $this->data   = $data;

        return $this;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function addCustomHeader(string $key, string $value)
    {
        $this->customHeaders[] = $key . ': ' . $value;
    }

    /**
     * @return array
     */
    public function getCustomHeaders(): array
    {
        return $this->customHeaders;
    }

    public function id(): string
    {
        /*
		 * TODO
		 * - Only GET now supported
		 * - Add start time of scrappe round
		 */
        //return sha1($this->url);
        return $this->hash();
    }

    /**
     * @return string
     */
    public function rawHash()
    {
        $hash = [];

        if ($this->method != 'GET') {
            $hash[] = $this->method;
        }

        if (! empty($this->data)) {
            $hash[] = $this->data;
        }

        $hash[] = $this->url;

        return implode('|', $hash);
    }

    /**
     * @return string
     */
    public function hash()
    {
        return base64_encode($this->rawHash());

        // TODO Create unique hash for this request, used for later recreating request
    }

    /**
     * @return string
     */
    public function url()
    {
        return $this->url;
    }

    /**
     * @param int $depth
     * @return ScrapperRequest
     */
    public function setDepth(int $depth)
    {
        if ($this->depth < 0) {
            $this->depth = $depth;
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getDepth(): int
    {
        return $this->depth;
    }

    /**
     * @param bool $analyse
     * @return $this
     */
    public function setAnalyse(bool $analyse)
    {
        $this->analyse = $analyse;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAnalyse()
    {
        return $this->analyse;
    }

    public static function requestByHash($hash)
    {
        if (filter_var($hash, FILTER_VALIDATE_URL) !== false) {
            return new ScrapperRequest($hash);
        }

        return null;
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        // TODO missing timestamp
        return [
            //RequestNavigator::FIELD_ID => $this->hash(),
            //'hash' => $this->hash(),
            //RequestNavigator::FIELD_URL => $this->url(),
            'request' => $this
            //ScrapperNavigator::FIELD_CHILDREN => array(), // No childs indicate not yext scrapped
        ];
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return $this->url();
    }

    /**
     * @param $ch
     */
    public function updateCurlRequest(&$ch)
    {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->method);

        if ($this->method == 'POST') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->data);
        }
    }
}
