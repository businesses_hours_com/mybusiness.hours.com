<?php

namespace App\Services;

use App\Models\LocationWizard;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use ErrorException;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Locally\SchemaBuilder\Schema\DayType\DayType;
use Locally\SchemaBuilder\Schema\DayType\FullDayClosed;
use Locally\SchemaBuilder\Schema\DayType\FullDayOpen;
use Locally\SchemaBuilder\Schema\DayType\OpenAsRegular;
use Locally\SchemaBuilder\Schema\DayType\TimeRange;
use Locally\SchemaBuilder\Schema\Department;
use Locally\SchemaBuilder\Schema\Exceptions;
use Locally\SchemaBuilder\Schema\Holidays;
use Locally\SchemaBuilder\Schema\Period;
use Locally\SchemaBuilder\Schema\PeriodType\DateRange;
use Locally\SchemaBuilder\Schema\PeriodType\DefaultPeriod;

class WizardService
{
    protected AdminHoursService $adminHoursService;
    public ?LocationWizard      $wizard;
    public array                $data;
    public array                $errors = [];

    public function __construct(LocationWizard $wizard = null)
    {
        $this->wizard            = $wizard;
        $this->adminHoursService = new AdminHoursService();
        $this->data              = optional($wizard)->data ?? [];
    }

    public function saveProgress(array $data, ?int $wizard_id): LocationWizard
    {
        /** @var LocationWizard $wizard */
        $wizard = $wizard_id ? LocationWizard::query()->find($wizard_id) : null;

        if ($wizard) {
            $wizard->update($data);
        }
        else {
            $wizard = LocationWizard::query()->create($data);
        }

        return $wizard;
    }

    public function storeWizardData(): void
    {
        // test department hours format and validation
        $hours = $this->getFormattedHoursArray();

        $businessId = $this->data['step1']['business']['id'] ?? false;

        if ($businessId) {
            $this->manageSelectedLocations();
        }

        if ($this->data['step1']['new_business'] ?? false) {
            $businessId = $this->addNewBusiness();
        }

        if (($this->data['step2']['new_location'] ?? false) && $businessId) {
            $requestData = $this->getFormattedRequestDataArray($businessId);
            $this->addNewLocation($requestData);
        }
    }

    public function manageSelectedLocations()
    {
        $locations = $this->data['step1']['locations'] ?? [];

        if (! $locations) {
            return;
        }

        $requestData = [
            'user_id'            => Auth::id(),
            'business_id'        => $this->data['step1']['business']['id'],
            'locations_id_array' => array_keys($locations),
            'country_code'       => config('app.country'),
        ];

        $this->adminHoursService->associateLocations($requestData);
    }

    public function addNewBusiness(): ?int
    {
        $newBusiness = $this->data['step1']['new_business'];

        $requestData = [
            'object'               => 'business',
            'owner_user_id'        => Auth::id(),
            'name'                 => $newBusiness['name'],
            'active'               => 1,
            'category_id'          => $newBusiness['category']['id'] ?? null,
            'source'               => User::TYPE,
            'is_chain'             => ($newBusiness['chain'] ?? false) === 'on' ? 1 : 0,
            'status'               => 'published',
            'website'              => $newBusiness['website'] ?? '',
            'logo'                 => $newBusiness['logo'] ?? [],
            'favicon'              => $newBusiness['favicon'] ?? [],
            'promoted_description' => $newBusiness['promoted_description'] ?? null,
            'social_profiles'      => $newBusiness['social_profiles'] ?? [],
            'country_code'         => config('app.country'),
        ];


        $response = $this->adminHoursService->addNewBusiness($requestData);

        return $response['data'][0]['id'] ?? null;
    }

    public function addNewLocation(array $requestData, bool $saveWizard = true): array
    {
        $response               = $this->adminHoursService->addNewLocation($requestData);
        $savedPublishedLocation = $response['data'][0] ?? null;

        if (! $savedPublishedLocation) {
            throw new Exception('No published location returned from api', Response::HTTP_BAD_REQUEST);
        }

        if ($saveWizard) {
            $this->wizard->kc_location_hash = $savedPublishedLocation['hash'];
            $this->wizard->save();
        }

        return $savedPublishedLocation;
    }

    public function getFormattedRequestDataArray(int $businessId, string $hash = null, string $redirectFromHash = null): array
    {
        $newLocation = $this->data['step2']['new_location'];

        $requestData = [
            'object'       => 'location',
            'source'       => User::TYPE,
            'user_id'      => Auth::id(),
            'business_id'  => $businessId,
            'address_id'   => $newLocation['address']['address_id'] ?? null,
            'address'      => $newLocation['address'],
            'hours'        => $this->getFormattedHoursArray(),
            'contact'      => [
                'email'    => $newLocation['email'] ?? null,
                'phone'    => $newLocation['phone'] ?? null,
                'fax'      => $newLocation['fax'] ?? null,
                'website'  => $newLocation['website'] ?? null,
                'name'     => $newLocation['name'] ?? null,
                'est_code' => $newLocation['est_code'] ?? null,
            ],
            'country_code' => config('app.country'),
        ];

        $requestData['contact'] = array_filter($requestData['contact']);

        // add checksums
        $requestData['address_checksum'] = hash('sha256', (json_encode($requestData['address'])));
        $requestData['contact_checksum'] = hash('sha256', (json_encode($requestData['contact'])));
        $requestData['hours_checksum']   = hash('sha256', (json_encode($requestData['hours'])));

        // add existing hash if available
        if ($hash) {
            $requestData['hash'] = $hash;
        }

        // if updating existing location with new address
        if ($redirectFromHash) {
            $requestData['redirect_from_hash'] = $redirectFromHash;
        }

        return $requestData;
    }

    public function getFormattedHoursArray(): array
    {
        $hours     = $this->data['step3']['times'] ?? [];
        $hoursType = $hours['type'] ?? 'regular';

        return $this->formatDepartmentHours($hours[$hoursType] ?? []);
    }

    public function formatDepartmentHours(array $hours): array
    {
        $departments = [];

        foreach ($hours as $departmentHours) {

            $departmentName = $departmentHours['name'] ?? $departmentHours['period'] ?? 'default';
            $isMain         = ($departmentHours['is_main'] ?? null) === 'on';

            $department = new Department($departmentName, null, $isMain);
            $period     = $this->addDaysToPeriod($departmentHours['days'], $departmentHours['period'] ?? null);
            $department->addPeriod($period);

            $department->exceptions = $this->formatExceptions($departmentHours['exceptions'] ?? []);
            $department->holidays   = $this->formatHolidays($departmentHours['holidays'] ?? []);

            $departments[] = $department->toArray();
        }

        return $departments;
    }

    private function getPeriodInstance(string $periodDateRangeString = null): Period
    {
        if ($periodDateRangeString) {
            $periodArray = explode('/', $periodDateRangeString);
            $from        = Carbon::createFromFormat('d-m-Y', trim($periodArray[0]))->format('Y-m-d');
            $to          = Carbon::createFromFormat('d-m-Y', trim($periodArray[1]))->format('Y-m-d');
            return new Period(new DateRange($from, $to));
        }
        else {
            return new Period(new DefaultPeriod());
        }
    }

    /**
     * @throws ErrorException
     */
    private function addDaysToPeriod(array $daysArray, string $periodDateRangeString = null): Period
    {
        $this->errors = [];
        $period       = $this->getPeriodInstance($periodDateRangeString);

        foreach ($daysArray as $dayIndex => $day) {
            $openingTime = $day['opening_time'] ?? [];
            $closingTime = $day['closing_time'] ?? [];
            $openStatus  = $day['open_status'] ?? 'closed';

            if ($dayIndex < 6) {
                $dayTypes = count($openingTime) ? [] : [$this->getDateType($openStatus)];
                foreach ($openingTime as $key => $openTime) {
                    if (isset($openTime) && isset($closingTime[$key])) {
                        $appointmentOnly = ($day['appointment'][$key] ?? 'off') === 'on';
                        $dayTypes[]      = $this->getDateType($openStatus, $appointmentOnly, $openTime, $closingTime[$key]);
                    }
                }
                try {
                    $period->addDay(($dayIndex + 1), ...$dayTypes);
                }
                catch (Exception $e) {
                    $this->errors['message'][] = $e->getMessage();
                    $this->errors['overlap'][] = $dayIndex;
                }
            }
            else {
                $openStatus = Arr::wrap($openStatus);
                if (in_array('closed', $openStatus, true)) {
                    $dayTypes = [$this->getDateType('closed')];
                    try {
                        $period->addDay('sunday_all', ...$dayTypes);
                    }
                    catch (Exception $e) {
                        $this->errors['message'][] = $e->getMessage();
                        $this->errors['overlap'][] = $dayIndex;
                    }
                }
                else {
                    foreach ($openStatus as $dayStatus) {
                        $dayTypes = [];
                        foreach ($openingTime as $key => $openTime) {
                            try {
                                $appointmentOnly = ($day['appointment'][$key] ?? 'off') === 'on';
                                $dayTypes[]      = $dayStatus === 'open_24' ?
                                    new FullDayOpen($appointmentOnly) :
                                    new TimeRange($openTime, $closingTime[$key], $appointmentOnly);
                            }
                            catch (Exception $e) {
                                Log::error($e->getMessage() . ' / $openStatus: ' . json_encode($openStatus) . ' / $openingTime:  ' . json_encode($openingTime) . ' / $closingTime: ' . json_encode($closingTime));
                                throw $e;
                            }
                        }
                        try {
                            $period->addDay($dayStatus, ...$dayTypes);
                        }
                        catch (Exception $e) {
                            $this->errors['message'][] = $e->getMessage();
                            $this->errors['overlap'][] = $dayIndex;
                        }
                    }
                }
            }
        }

        if ($this->errors) {
            throw new Exception(json_encode($this->errors), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $period;
    }

    public function formatExceptions(array $hours): Exceptions
    {
        $exceptions = new Exceptions();

        foreach ($hours as $hoursArray) {

            if (empty($hoursArray['period'])) {
                continue;
            }

            $openStatus  = $hoursArray['open_status'] ?? 'closed';
            $periodArray = explode('/', $hoursArray['period'] ?? '');
            $reason      = $hoursArray['reason'] ?? '';
            $openingTime = $hoursArray['opening_time'] ?? [null];
            $closingTime = $hoursArray['closing_time'] ?? [null];
            $dayTypes    = [];

            foreach ($openingTime as $key => $openTime) {
                $appointmentOnly = ($hoursArray['appointment'][$key] ?? 'off') === 'on';
                $dayTypes[]      = $this->getDateType($openStatus, $appointmentOnly, $openTime, $closingTime[$key], $reason);
            }

            try {
                $from         = Carbon::createFromFormat('d-m-Y', trim($periodArray[0]))->format('Y-m-d');
                $to           = Carbon::createFromFormat('d-m-Y', trim($periodArray[1]))->format('Y-m-d');
                $carbonPeriod = CarbonPeriod::create($from, $to);
                foreach ($carbonPeriod as $day) {
                    $exceptions->add($day->format('Y-m-d'), ...$dayTypes);
                }
            }
            catch (Exception $e) {
                throw new Exception(
                    'Invalid date range for your exceptions, please choose correct dates.',
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }
        }

        return $exceptions;
    }

    public function formatHolidays(array $hours): Holidays
    {
        $holidays = new Holidays();

        foreach ($hours as $name => $hoursArray) {
            if ($hoursArray['name'] ?? false) {
                $holiday         = $hoursArray['name'];
                $country         = $hoursArray['country'] ?? ucfirst(auth()->user()->getCountryName(config('app.country')));
                $openStatus      = $hoursArray['open_status'] ?? 'closed';
                $openingTime     = $hoursArray['opening_time'] ?? [null];
                $closingTime     = $hoursArray['closing_time'] ?? [null];
                $dayTypes        = [];

                foreach ($openingTime as $key => $openTime) {
                    $appointmentOnly = ($hoursArray['appointment'][$key] ?? 'off') === 'on';
                    $dayTypes[]      = $this->getDateType($openStatus, $appointmentOnly, $openTime, $closingTime[$key]);
                }

                try {
                    $holidays->add($holiday, $country, ...$dayTypes);
                }
                catch (Exception $e) {
                    if (! Str::contains($e->getMessage(), 'newYearsEve')) {
                        throw $e;
                    }
                }
            }
        }

        return $holidays;
    }

    /**
     * @throws \ErrorException
     */
    public function getDateType(string $openStatus, bool $appointmentOnly = false, string $from = null, string $to = null, string $reason = ''): DayType
    {
        switch ($openStatus) {
            case 'open':
                return ($from && $to) ? new TimeRange($from, $to, $appointmentOnly) : new FullDayClosed();
            case 'closed':
                return new FullDayClosed($reason);
            case "open_24":
                return new FullDayOpen($appointmentOnly);
            case "open_regular":
            default:
                return new OpenAsRegular();
        }
    }

    public function createNewWizard(int $currentStep = 1): LocationWizard
    {
        $userId     = Auth::id();
        $stepToCopy = 'step' . ($currentStep - 1);

        /** @var LocationWizard $currentWizard */
        $currentWizard = LocationWizard::query()
            ->where('user_id', $userId)
            ->latest()
            ->first();

        /** @var LocationWizard $newWizard */
        $newWizard = LocationWizard::query()->create([
            'user_id' => $userId,
            'data'    => [
                'current_step' => $currentStep,
                $stepToCopy    => $currentWizard->data[$stepToCopy] ?? [],
            ]
        ]);

        return $newWizard;
    }

    public static function array_filter_recursive(array $input): array
    {
        foreach ($input as $key => &$value) {
            if (is_array($value)) {
                $value = static::array_filter_recursive($value);
                if ($key === 'appointment') {
                    $value = array_values($value);
                }
            }
            elseif (is_string($value)) {
                $value = trim($value);
            }
        }

        return array_filter($input);
    }
}
