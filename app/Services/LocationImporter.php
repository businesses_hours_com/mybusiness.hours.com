<?php

namespace App\Services;

use App\Models\LocationWizard;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class LocationImporter
{
    const TIMES_TYPES = [
        'regular',
        'departments',
        'seasonal',
    ];

    protected string            $hash;
    protected AdminHoursService $adminHoursService;

    public function __construct()
    {
        $this->adminHoursService = new AdminHoursService();
    }

    public function instantiateWizardFromLocation(array $publishedLocation): ?LocationWizard
    {
        $wizardData = $this->prepareDataForImporting($publishedLocation);

        return new LocationWizard([
            'user_id'          => auth()->id(),
            'data'             => $wizardData,
            'kc_location_hash' => $publishedLocation['hash'],
        ]);
    }

    public function import(string $hash): ?LocationWizard
    {
        $publishedLocation = $this->getSingleLocationFromApi($hash);
        $wizardData        = $this->prepareDataForImporting($publishedLocation);

        /** @var LocationWizard $wizard */
        $wizard = LocationWizard::query()->firstOrCreate([
            'user_id'          => auth()->id(),
            'data'             => $wizardData,
            'kc_location_hash' => $hash,
        ]);

        $wizardService = new WizardService($wizard);
        $wizardService->manageSelectedLocations();

        return $wizard;
    }

    public function prepareDataForImporting(array $publishedLocation): array
    {
        $hoursSchema = $publishedLocation['location_version']['schema']['hours'] ?? [];

        if (!$publishedLocation || !$hoursSchema) {
            Log::alert(__METHOD__ . ': Missing hoursSchema: ' . json_encode($hoursSchema) . ' for publishedLocation ' . ($publishedLocation['hash'] ?? ''));
            abort(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $times = [];

        foreach ($hoursSchema as $index => $department) {

            $departmentName = $department['department'] ?? 'default';
            $isSeasonal     = Str::containsAll($departmentName, ['/', '-']);
            $type           = $isSeasonal ? 'seasonal' : ($departmentName === 'default' ? 'regular' : 'departments');

            $times['type'] = $times['type'] ?? $type;

            $formattedPeriods                  = $this->transformBackendPeriodDays($department['periods'] ?? []);
            $formattedPeriods[0]['holidays']   = $this->transformBackendHolidays($department['holidays'] ?? []);
            $formattedPeriods[0]['exceptions'] = $this->transformBackendExceptions($department['exceptions'] ?? []);

            if ($type === 'departments') {
                $formattedPeriods[0]['name']    = $departmentName;
                $formattedPeriods[0]['is_main'] = ($department['is_main'] ?? false) === true ? 'on' : 'off';
            }
            if ($type === 'seasonal') {
                $formattedPeriods[0]['period'] = $departmentName;
            }

            $times[$type][] = $formattedPeriods[0];
        }

        $initialEmptyPeriod = [
            'days'       => [],
            'holidays'   => [], //$formattedPeriods[0]['holidays'],
            'exceptions' => [], //$formattedPeriods[0]['exceptions']
        ];

        foreach (static::TIMES_TYPES as $timeType) {
            $times[$timeType] ??= [$initialEmptyPeriod];
        }

        return [
            'current_step' => 3,
            'user_id'      => Auth::id(),
            'step1'        => [
                'business'  => [
                    'id'   => $publishedLocation['business']['id'],
                    'name' => $publishedLocation['business']['name'],
                ],
                'locations' => auth()->user()->locations()
                    ->mapWithKeys(fn($loc) => [$loc->id => 'on'])
                    ->union([$publishedLocation['id'] => 'on'])
                    ->toArray()
            ],
            'step2'        => [
                'new_location' => [
                    'address' => [
                        'lat'           => $publishedLocation['address']['lat'] ?? '',
                        'lng'           => $publishedLocation['address']['lng'] ?? '',
                        'street'        => $publishedLocation['address']['street'] ?? '',
                        'country'       => $publishedLocation['address']['country'] ?? '',
                        'locality'      => $publishedLocation['address']['locality'] ?? '',
                        'address_id'    => $publishedLocation['address']['id'] ?? null,
                        'postal_code'   => $publishedLocation['address']['postal_code'] ?? '',
                        'house_number'  => $publishedLocation['address']['street_number'] ?? '',
                        'street_number' => $publishedLocation['address']['street_number'] ?? '',
                    ]
                ]
            ],
            'step3'        => [
                'times' => $times ?? [],
            ]
        ];
    }

    public function getSingleLocationFromApi(string $hash): array
    {
        $data = $this->adminHoursService->getSingleLocation($hash, [
            'relations' => [
                'address', 'city', 'business', 'locationVersion', 'users:id',
            ]
        ]);

        return $data['data'][0] ?? [];
    }

    private function transformBackendPeriodDays(array $periods): array
    {
        $formattedPeriods = [];

        foreach ($periods as $periodName => $hoursArray) {
            $days = [];

            foreach ($hoursArray as $day => $timeRanges) {

                if (in_array($timeRanges[0], ['closed', '24h'], true)) {

                    $openStatus = $timeRanges[0] === 'closed' ? 'closed' : 'open_24';

                    $days[] = [
                        'open_status' => $openStatus,
                    ];

                    continue;
                }

                [$openingTimes, $closingTimes, $appointment] = $this->getTimeArraysFromTimeRanges($timeRanges);

                $dayTimes = [
                    'open_status'  => Str::contains($day, 'sunday') ? [$day] : 'open',
                    'opening_time' => $openingTimes,
                    'closing_time' => $closingTimes,
                ];

                if ($appointment) {
                    $dayTimes['appointment'] = ['on'];
                }

                $days[] = $dayTimes;
            }

            $formattedPeriods[] = [
                'days' => $days
            ];
        }

        return $formattedPeriods;
    }

    private function transformBackendExceptions(array $exceptions): array
    {
        $formattedArray = [];
        $range          = [];
        $index          = 1;

        foreach ($exceptions as $date => $timeRanges) {

            $lastInRange = Arr::last($range);

            if (!$lastInRange) {
                $range[] = $date;
                continue;
            }

            $lastCarbonInRange = Carbon::parse($lastInRange);
            $currentCarbon     = Carbon::parse($date);

            if ($lastCarbonInRange->lt($currentCarbon) && $lastCarbonInRange->diffInDays($currentCarbon) === 1) {
                $range[] = $date;

                if ($date === array_key_last($exceptions)) {
                    $formattedArray[(string)$index] = $this->transformBackendTimeRanges($exceptions, $range);
                }

                continue;
            }

            $formattedArray[(string)$index] = $this->transformBackendTimeRanges($exceptions, $range);

            $range = [$date];
            $index++;

            if ($date === array_key_last($exceptions)) {
                $formattedArray[(string)$index] = $this->transformBackendTimeRanges($exceptions, $range);
            }
        }

        return $formattedArray;
    }

    private function transformBackendTimeRanges($exceptions, $range): array
    {
        $startDate    = Carbon::parse($range[0])->format('d-m-Y');
        $endDate      = Carbon::parse(Arr::last($range))->format('d-m-Y');
        $periodString = $startDate . ' / ' . $endDate;
        $timeRange    = $exceptions[$range[0]];
        $openStatus   = $timeRange[0];
        $reason       = '';

        if (Str::startsWith($openStatus, 'closed[')) {
            $reason     = str_replace(['closed[', ']'], '', $openStatus);
            $openStatus = 'closed';
        }

        if (in_array($openStatus, ['closed', '24h'], true)) {
            return [
                'period'      => $periodString,
                'open_status' => $openStatus === '24h' ? 'open_24' : $openStatus,
                'reason'      => $reason,
            ];
        } else {
            [$opening, $closing] = $this->getTimeArraysFromTimeRanges($timeRange);
            return [
                'period'       => $periodString,
                'open_status'  => 'open',
                'opening_time' => $opening,
                'closing_time' => $closing,
            ];
        }
    }

    private function transformBackendHolidays(array $holidays): array
    {
        $formattedHolidays = [];
        $allHolidays       = $this->adminHoursService->getHolidaysAsCollection([
            'country_code' => config('app.country'),
            'show_retail'  => 1,
            'current_year' => 1,
        ]);

        foreach ($holidays as $holidayName => $timeRanges) {

            $holidayId = $allHolidays->firstWhere('short_name', $holidayName)['id'] ?? null;

            if (!$holidayId) {
                continue;
            }

            if (in_array($timeRanges[0], ['closed', '24h', 'open_regular'], true)) {

                $formattedHolidays[$holidayId] = [
                    'name'        => $holidayName,
                    'open_status' => $timeRanges[0] === '24h' ? 'open_24' : $timeRanges[0],
                ];

                continue;
            }

            [$openingTimes, $closingTimes, $appointment] = $this->getTimeArraysFromTimeRanges($timeRanges);

            $formattedHolidays[$holidayId] = [
                'name'         => $holidayName,
                'open_status'  => 'open',
                'opening_time' => $openingTimes,
                'closing_time' => $closingTimes,
            ];

            if ($appointment) {
                $formattedHolidays[$holidayId]['appointment'] = ['on'];
            }

        }

        return $formattedHolidays;
    }

    private function getTimeArraysFromTimeRanges(array $timeRangeArray): array
    {
        $openingTimes    = [];
        $closingTimes    = [];
        $appointmentOnly = false;

        foreach ($timeRangeArray as $timeRange) {

            if (Str::contains($timeRange, '[appointment]')) {
                $appointmentOnly = true;
                $timeRange       = str_replace('[appointment]', '', $timeRange);
            }


            [$openingTime, $closingTime] = explode('/', $timeRange);

            $openingTime    = substr_replace($openingTime, ':', 2, 0);
            $closingTime    = substr_replace($closingTime, ':', 2, 0);
            $openingTimes[] = $openingTime;
            $closingTimes[] = $closingTime;
        }

        return [$openingTimes, $closingTimes, $appointmentOnly];
    }
}
