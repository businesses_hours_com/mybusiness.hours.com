<?php

namespace App\Services;

use App\Models\User;
use Carbon\Carbon;
use Exception;
use Honeybadger\HoneybadgerLaravel\Facades\Honeybadger;
use HoursAdminSdk\BaseApi;
use HoursAdminSdk\HolidayApi;
use HoursAdminSdk\BusinessApi;
use HoursAdminSdk\PublishedLocationApi;
use Illuminate\Http\Client\Pool;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Throwable;

class AdminHoursService
{
    public string  $env;
    public string  $token;
    public BaseApi $businessApi;
    public BaseApi $publishedLocationApi;
    public BaseApi $holidayApi;
    public BaseApi $baseApi;

    public function __construct()
    {
        $this->env   = config('services.backend.api_env');
        $this->token = config('services.backend.api_token.' . $this->env);

        $this->businessApi          = new BusinessApi($this->token, $this->env);
        $this->publishedLocationApi = new PublishedLocationApi($this->token, $this->env);
        $this->holidayApi           = new HolidayApi($this->token, $this->env);
        $this->baseApi              = new BaseApi($this->token, $this->env);
    }

    /**
     * @throws Exception
     */
    public function initData(User $user = null): array
    {
        /** @var User $user */
        $user = $user ?? auth()->user();

        if (! $user) {
            Honeybadger::notify(new Exception('Missing User in initData()', 500), request());
            return [
                'locations'  => [],
                'businesses' => [],
                'settings'   => [],
            ];
        }

        $timeStart = microtime(true);

        $responses = Http::pool(function(Pool $pool) use ($user) {
            $baseUri = $this->baseApi->getUrl($this->env);

            $tokenQuery = ['api_token' => $this->token];
            $query      = [
                'user_id'      => $user->id,
                'country_code' => config('app.country')
            ];

            $poolArray = [];

            if (Cache::missing($user->getCacheKey(User::CACHE_LOCATIONS))) {
                \Log::info('Pool: ' . User::CACHE_LOCATIONS);
                $poolArray[] = $pool->as(User::CACHE_LOCATIONS)->get($baseUri . 'kc/associate', array_merge($query, $tokenQuery));
            }

            if (Cache::missing($user->getCacheKey(User::CACHE_BUSINESSES))) {
                \Log::info('Pool: ' . User::CACHE_BUSINESSES);
                $poolArray[] = $pool->as(User::CACHE_BUSINESSES)->get($baseUri . 'businesses/associate', array_merge($query, $tokenQuery));
            }

            if (Cache::missing($user->getCacheKey(User::CACHE_SETTINGS))) {
                \Log::info('Pool: ' . User::CACHE_SETTINGS);
                $poolArray[] = $pool->as(User::CACHE_SETTINGS)->get($baseUri . 'settings/retail-admin', $tokenQuery);
            }

            return $poolArray;
        });

        try {
            $locations = isset($responses[User::CACHE_LOCATIONS]) ? $responses[User::CACHE_LOCATIONS]->throw()->object(
            ) : [];
            $businesses = isset($responses[User::CACHE_BUSINESSES]) ? $responses[User::CACHE_BUSINESSES]->throw(
            )->object() : [];
            $settings = isset($responses[User::CACHE_SETTINGS]) ? $responses[User::CACHE_SETTINGS]->throw()->json(
            ) : [];
        }
        catch(Throwable $e) {
            throw new Exception('Error fetching data from API', 500, $e);
        }

        \Log::info(__FUNCTION__ . ' / ' . (round(microtime(true) - $timeStart, 2)));

        if ($locations) {
            Cache::put($user->getCacheKey(User::CACHE_LOCATIONS), collect($locations), now()->addDay());
        }
        if ($businesses) {
            Cache::put($user->getCacheKey(User::CACHE_BUSINESSES), collect($businesses), now()->addDay());
        }
        if ($settings) {
            Cache::put($user->getCacheKey(User::CACHE_SETTINGS), collect($settings), now()->addDay());
        }

        return [
            'locations'  => $locations ?: Cache::get($user->getCacheKey(User::CACHE_LOCATIONS)),
            'businesses' => $businesses ?: Cache::get($user->getCacheKey(User::CACHE_BUSINESSES)),
            'settings'   => $settings ?: Cache::get($user->getCacheKey(User::CACHE_SETTINGS)),
        ];
    }

    public function getBusinesses(array $data)
    {
        return $this->businessApi->index($data);
    }

    public function getLocations(array $data)
    {
        $data['referrer'] = 'business_admin';
        return $this->publishedLocationApi->index($data);
    }

    public function getSingleLocation(string $hashSlugOrId, array $data = [])
    {
        $data['referrer'] = 'business_admin';
        return $this->publishedLocationApi->show($hashSlugOrId, $data);
    }

    public function deleteLocation(string $hashSlugOrId, array $data = [])
    {
        $data['referrer'] = 'business_admin';
        return $this->baseApi->delete('published/locations/' . $hashSlugOrId, $data);
    }

    public function deleteFile(array $data = [])
    {
        return $this->baseApi->delete('kc/files', $data);
    }

    public function getSettings(array $data = [])
    {
        return $this->baseApi->get('settings/retail-admin', $data);
    }

    public function getHolidays(array $data)
    {
        return $this->holidayApi->index($data);
    }

    public function getHolidaysAsCollection(array $data): Collection
    {
        $response = $this->getHolidays($data);

        return collect($response['data'] ?? [])->map(function($holiday) {
            return array_merge($holiday, [
                'short_date' => Carbon::parse($holiday['date'])->formatLocalized('%d %b')
            ]);
        })->keyBy('id');
    }

    public function getCategories(array $data)
    {
        return $this->baseApi->get('categories', $data);
    }

    public function getAssociatedLocations(array $data)
    {
        $data['country_code'] = config('app.country');
        return $this->baseApi->get('kc/associate', $data, false);
    }

    public function associateLocations(array $data)
    {
        try {
            $data['country_code'] = config('app.country');
            $response             = $this->baseApi->post('kc/associate', $data, false);

            auth()->user()->clearCache(User::CACHE_LOCATIONS);

            return $response;
        }
        catch (Exception $e) {
            Log::error($e);
            throw $e;
        }
    }

    public function promoteLocation(array $data)
    {
        try {
            $data['country_code'] = config('app.country');
            $response             = $this->baseApi->post('kc/promote', $data, false);

            auth()->user()->clearCache(User::CACHE_LOCATIONS);

            return $response;
        }
        catch (Exception $e) {
            Log::error($e);
            throw $e;
        }
    }

    public function addNewLocation(array $data)
    {
        return $this->storeStagingObject($data);
    }

    public function addNewBusiness(array $data)
    {
        return $this->storeStagingObject($data);
    }

    private function storeStagingObject(array $data)
    {
        try {
            $data['country_code'] = $data['country_code'] ?? config('app.country');
            $response             = $this->baseApi->post('kc', $data);

            $cacheKey = $data['object'] === 'location' ? User::CACHE_LOCATIONS : User::CACHE_BUSINESSES;
            auth()->user()->clearCache($cacheKey);

            return $response;
        }
        catch (Exception $e) {
            Log::error($e);
            throw $e;
        }
    }

    public function getOwnedBusinesses(array $data)
    {
        $data['country_code'] = config('app.country');
        return $this->baseApi->get('businesses/associate', $data, false);
    }

    public function validateBusiness(array $data)
    {
        $data['country_code'] = config('app.country');
        return $this->baseApi->get('businesses/validate', $data, true);
    }
}
