<?php

namespace App\Services;

use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use KubAT\PhpSimple\HtmlDomParser;

class LogoScraper extends BaseScraper
{
    protected $dom;

    public function __construct(string $html)
    {
        $this->dom = HtmlDomParser::str_get_html($html, false, null, 0);
    }

    public function scrape(): array
    {
        if (! $this->dom) {
            return [];
        }

        $script = $this->dom->find('script[type="application/ld+json"]', 0);
        $json   = @json_decode($script->innertext, true);
        $graph  = $json['@graph'] ?? [];

        foreach ($graph as $array) {
            if (array_key_exists('logo', $array)) {
                $name = $this->saveLogo($array['logo']);

                if ($formatIconArray = $this->formatIconArray($name)) {
                    return $formatIconArray;
                }
            }
        }

        return [];
    }

    private function saveLogo($logoArray): ?string
    {
        if (! is_array($logoArray)) {
            return null;
        }

        if (count($logoArray) === 0 || array_key_exists('url', $logoArray) === false) {
            return null;
        }

        try {
            $url = $logoArray['url'] ?? null;

            if (empty($url)) {
                return null;
            }

            $contents = file_get_contents($url);
            $name     = substr($url, strrpos($url, '/') + 1);
            $name     = 'scrape_data/' . Str::uuid()->toString() . '_' . $name;

            Storage::disk('public')->put($name, $contents);
        }
        catch (Exception $e) {
            $name = null;
            Log::alert($e->getMessage());
        }

        return $name;
    }
}
