<?php

namespace App\Services;

use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Services\Scraper\Icon;
use App\Services\Scraper\Scraper;
use Illuminate\Support\Facades\Storage;
use Throwable;

class IconScraper extends BaseScraper
{
    protected Scraper $scraper;
    protected string $website;

    public function __construct(string $website)
    {
        $this->scraper = new Scraper();
        $this->website = $website;
    }

    public function scrape(): array
    {
        $icons = [];

        foreach ($this->fetch() as $type => $iconArray) {
            $name = $this->saveIcon($iconArray);

            if ($formatIconArray = $this->formatIconArray($name)) {
                $icons[$type] = $formatIconArray;
            }
        }

        return $icons;
    }

    private function fetch(): array
    {
        try {
            $icons = $this->scraper->get($this->website);

            $appleTouchIcons = array_filter($icons, function ($icon) {
                return $icon->getType() === 'apple-touch-icon';
            });

            $favicons = array_filter($icons, function ($icon) {
                return $icon->getType() === 'favicon';
            });

            if (count($favicons) === 0) {
                $favicons = $appleTouchIcons;
            }

            if (count($favicons) > 1) {
                usort($favicons, array(self::class, 'orderIcons'));
            }
            if (count($appleTouchIcons) > 1) {
                usort($appleTouchIcons, array(self::class, 'orderIcons'));
            }
        } catch (Exception $e) {
            $favicons = [];
            $appleTouchIcons = [];
            Log::alert($e->getMessage());
        }

        return [
            'favicon' => $favicons,
            'appleTouchIcon' => $appleTouchIcons,
        ];
    }

    private function orderIcons($first, $second): bool
    {
        return $first->getWidth() < $second->getWidth();
    }

    private function saveIcon(array $iconArray): ?string
    {
        if (!$iconArray) {
            return null;
        }

        try {
            $icon = reset($iconArray);
            $iconHref = $this->getHref($icon);
            $contents = @file_get_contents($iconHref);

            if (!$contents) {
                Log::alert('IconScraper: Could not fetch icon: ' . $iconHref);
                return null;
            }

            $name = substr($iconHref, strrpos($iconHref, '/') + 1);
            $name = 'scrape_data/' . Str::uuid()->toString() . '_' . $name;

            Storage::disk('public')->put($name, $contents);
        } catch (Throwable $e) {
            $name = null;
            Log::alert($e->getMessage());
        }

        return $name;
    }

    private function getHref(Icon $icon): string
    {
        $iconHref = $icon->getHref();
        $parsedUrl = parse_url($iconHref);
        $path = ($parsedUrl['path'] ?? '') . ($parsedUrl['query'] ?? '');

        if ($iconHref && $path && str_contains($path, '//')) {
            $pathArray = explode('//', $path);
            $iconHref = $parsedUrl['scheme'] . "://" . $parsedUrl['host'] . '/' . end($pathArray);
        }

        return $iconHref;
    }
}
