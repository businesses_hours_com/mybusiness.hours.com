<?php

namespace App\Policies;

use App\Models\User;

class UserPolicy
{
    public function view(User $user, User $updateUser)
    {
        return $user->id === $updateUser->id;
    }

    public function update(User $user, User $updateUser)
    {
        return $user->id === $updateUser->id;
    }
}
