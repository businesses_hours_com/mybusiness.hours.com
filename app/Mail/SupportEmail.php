<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SupportEmail extends Mailable
{
    use Queueable, SerializesModels;

    public string $name;
    public string $email;
    public string $about;
    public string $text;

    public function __construct(array $data)
    {
        $data = array_filter($data);

        $this->name  = $data['name'] ?? '';
        $this->email = $data['email'];
        $this->about = $data['about'];
        $this->text  = $data['text'] ?? '';
    }

    public function build()
    {
        return $this->markdown('emails.contact');
    }
}
