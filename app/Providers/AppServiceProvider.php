<?php

namespace App\Providers;

use App\Models\AppliedCoupon;
use App\Models\Country;
use App\Models\Credit;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\RedeemedCoupon;
use App\Models\Subscription;
use App\Services\CashierMollie\DatabaseCouponRepository;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\ServiceProvider;
use Laravel\Cashier\Cashier;
use Laravel\Cashier\Coupon\Contracts\CouponRepository;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(CouponRepository::class, DatabaseCouponRepository::class);
    }

    public function boot()
    {
        Cashier::useSubscriptionModel(Subscription::class);
        Cashier::useOrderModel(Order::class);
        Cashier::useOrderItemModel(OrderItem::class);
        Cashier::useCreditModel(Credit::class);
        Cashier::useAppliedCouponModel(AppliedCoupon::class);
        Cashier::useRedeemedCouponModel(RedeemedCoupon::class);

        // Set domain-specific values
        if ($_SERVER && ($domain = $_SERVER['HTTP_HOST'] ?? null)) {
            if ($configArray = Country::getDomainSpecificConfig($domain)) {
                config($configArray);
            }
        }

        EloquentBuilder::macro('toSqlWithBindings', function () {
            return $this->toBase()->toSqlWithBindings();
        });

        Builder::macro('toSqlWithBindings', function () {
            return vsprintf(
                str_replace('?', '%s', $this->grammar->compileSelect($this)),
                collect($this->getBindings())
                    ->map(function ($binding) {
                        return is_numeric($binding) ? $binding : "'{$binding}'";
                    })
                    ->toArray()
            );
        });
    }
}
