FROM php:8.0-fpm

# Arguments defined in docker-compose.yml
ARG user
ARG uid

# Install system dependencies
RUN apt-get update &&  \
    apt-get install -y git curl libpng-dev libonig-dev libxml2-dev zlib1g-dev libicu-dev g++ libzip-dev zip unzip nano && \
    apt-get clean &&  \
    rm -rf /var/lib/apt/lists/*

# Install PHP extensions
RUN docker-php-ext-configure intl
RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd zip intl soap

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# nvm environment variables
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 14.18.1

# install nvm
# https://github.com/creationix/nvm#install-script
RUN mkdir $NVM_DIR && \
    curl https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

# install node and npm
RUN echo "source $NVM_DIR/nvm.sh && \
    nvm install $NODE_VERSION && \
    nvm alias default $NODE_VERSION && \
    nvm use default" | bash

ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

RUN node -v
RUN npm -v

# Create systempw user to run Composer and Artisan Commands
RUN useradd -G www-data,root -u $uid -d /home/$user $user
RUN mkdir -p /home/$user/.composer && \
    chown -R $user:$user /home/$user

# Set working directory
WORKDIR /var/www

USER $user
