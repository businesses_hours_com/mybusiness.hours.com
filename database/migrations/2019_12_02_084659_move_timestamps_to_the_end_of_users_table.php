<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoveTimestampsToTheEndOfUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // move timestamps to the end
        DB::connection('mysql_backend')->statement("ALTER TABLE users MODIFY COLUMN created_at TIMESTAMP NULL AFTER extra_billing_information");
        DB::connection('mysql_backend')->statement("ALTER TABLE users MODIFY COLUMN updated_at TIMESTAMP NULL AFTER created_at");
        DB::connection('mysql_backend')->statement("ALTER TABLE users MODIFY COLUMN deleted_at TIMESTAMP NULL AFTER updated_at");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
