<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMollieFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_backend')->table('users', function (Blueprint $table) {
            if (!Schema::connection('mysql_backend')->hasColumn('users', 'mollie_customer_id')) {
                $table->string('mollie_customer_id')->nullable();
                $table->string('mollie_mandate_id')->nullable();
                $table->decimal('tax_percentage', 6, 4)->default(0);
                $table->dateTime('trial_ends_at')->nullable();
                $table->text('extra_billing_information')->nullable();
            }

            // drop not needed fields
            if (Schema::connection('mysql_backend')->hasColumn('users', 'loggedin_at')) {
                $table->dropColumn('loggedin_at');
            }
            if (Schema::connection('mysql_backend')->hasColumn('users', 'role')) {
                $table->dropColumn('role');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_backend')->table('users', function (Blueprint $table) {
            $table->dropColumn('mollie_customer_id');
            $table->dropColumn('mollie_mandate_id');
            $table->dropColumn('tax_percentage', 6, 4);
            $table->dropColumn('trial_ends_at');
            $table->dropColumn('extra_billing_information');
        });
    }
}
