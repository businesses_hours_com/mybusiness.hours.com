const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/frontend/app.js', 'public/js')
    .js('resources/js/admin/admin.js', 'public/js')
    .js('resources/js/auth/auth.js', 'public/js')
    .sass('resources/sass/frontend/app.scss', 'public/css')
    .sass('resources/sass/admin/admin.scss', 'public/css')
    .sass('resources/sass/auth/auth.scss', 'public/css')
    .styles([
        'public/theme/dist/assets/plugins/global/plugins.bundle.css',
        'public/theme/dist/assets/css/style.bundle.css',
        'public/theme/dist/assets/css/skins/aside/light.css',
        'public/theme/dist/assets/css/skins/brand/light.css',
        'public/theme/dist/assets/css/skins/header/base/light.css',
        'public/theme/dist/assets/css/skins/header/menu/light.css'
    ], 'public/css/theme.css')
    .scripts([
        'public/theme/dist/assets/plugins/global/plugins.bundle.js',
        'public/theme/dist/assets/js/scripts.bundle.js'
    ], 'public/js/theme.js');

if (mix.inProduction()) {
    mix.version();
}
